﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu của khách hàng
    /// </summary>
    public class KhachHang
    {
        #region Fields
        private string _maKhachHang;
        private string _hoTen;
        private string _gioiTinh;
        private string _diaChi;
        private string _soDienThoai;
        private string _email;
        #endregion

        #region Properties
        /// <summary>
        /// Mã khách hàng
        /// </summary>
        public string MaKhachHang
        {
            get { return _maKhachHang; }
            set { _maKhachHang = value; }
        }

        /// <summary>
        /// Họ tên
        /// </summary>
        public string HoTen
        {
            get { return _hoTen; }
            set { _hoTen = value; }
        }

        /// <summary>
        /// Giới tính
        /// </summary>
        public string GioiTinh
        {
            get { return _gioiTinh; }
            set { _gioiTinh = value; }
        }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        public string DiaChi
        {
            get { return _diaChi; }
            set { _diaChi = value; }
        }

        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string SoDienThoai
        {
            get { return _soDienThoai; }
            set { _soDienThoai = value; }
        }

        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        #endregion

        #region Methods
        public KhachHang(string maKhachHang, string hoTen, string gioiTinh, string diaChi, string soDienThoai, string email)
        {
            MaKhachHang = maKhachHang;
            HoTen = hoTen;
            GioiTinh = gioiTinh;
            DiaChi = diaChi;
            SoDienThoai = soDienThoai;
            Email = email;
        }
        #endregion
    }
}
