﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu Sao luu
    /// </summary>
    public class SaoLuu
    {
        #region Fields
        private string _maSaoLuu;
        private string _duongDan;
        private DateTime _ngayThucHien;
        private string _noiDung;
        #endregion

        #region Properties
        /// <summany>
        /// Mã sao lưu
        /// </summany>
        public string MaSaoLuu
        {
            get { return _maSaoLuu; }
            set { _maSaoLuu = value; }
        }

        /// <summany>
        /// Đường dẫn 
        /// </summany>
        public string DuongDan
        {
            get { return _duongDan; }
            set { _duongDan = value; }
        }

        /// <summany>
        /// Ngày thực hiện
        /// </summany>
        public DateTime NgayThucHien
        {
            get { return _ngayThucHien; }
            set { _ngayThucHien = value; }
        }

        /// <summany>
        /// Nội dung
        /// </summany>
        public string NoiDung
        {
            get { return _noiDung; }
            set { _noiDung = value; }
        }
        #endregion

        #region Methods
        public SaoLuu(string maSaoLuu, string duongDan, DateTime ngayThucHien, string noiDung)
        {
            MaSaoLuu = maSaoLuu;
            DuongDan = duongDan;
            NgayThucHien = ngayThucHien;
            NoiDung = noiDung;
        }
        #endregion
    }
}
