﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu báo giá
    /// </summary>
    public class BaoGia
    {
        #region Fields
        private string _maBaoGia;
        private string _maNhanVien;
        private DateTime _ngayLap;
        private string _ghiChu;
        #endregion

        #region Properties
        /// <summary>
        /// Mã phiếu
        /// </summary>
        public string MaBaoGia
        {
            get { return _maBaoGia; }
            set { _maBaoGia = value; }
        }

        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set { _maNhanVien = value; }
        }

        /// <summary>
        /// Ngày lập
        /// </summary>
        public DateTime NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public BaoGia(string maBaoGia, string maNhanVien, DateTime ngayLap, string ghiChu)
        {
            MaBaoGia = maBaoGia;
            MaNhanVien = maNhanVien;
            NgayLap = ngayLap;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
