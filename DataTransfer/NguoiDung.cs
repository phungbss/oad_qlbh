﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu của người dùng
    /// </summary>
    public class NguoiDung
    {
        #region Fields
        private string _maNguoiDung;
        private string _tenDangNhap;
        private string _matKhau;
        private string _hoTen;
        private string _gioiTinh;
        private string _maDacQuyen;
        private DateTime _ngaySinh;
        private string _soDienThoai;
        private string _email;
        #endregion

        #region Properties
        /// <summary>
        /// Mã người dùng
        /// </summary>
        public string MaNguoiDung
        {
            get { return _maNguoiDung; }
            set { _maNguoiDung = value; }
        }
        
        /// <summary>
        /// Tên đăng nhập
        /// </summary>
        public string TenDangNhap
        {
            get { return _tenDangNhap; }
            set { _tenDangNhap = value; }
        }
       
        /// <summary>
        /// Mật khẩu
        /// </summary>
        public string MatKhau
        {
            get { return _matKhau; }
            set { _matKhau = value; }
        }
        
        /// <summary>
        /// Họ tên
        /// </summary>
        public string HoTen
        {
            get { return _hoTen; }
            set { _hoTen = value; }
        }
        
        /// <summary>
        /// Giới tính
        /// </summary>
        public string GioiTinh
        {
            get { return _gioiTinh; }
            set { _gioiTinh = value; }
        }
        
        /// <summary>
        /// Mã đặc quyền
        /// </summary>
        public string MaDacQuyen
        {
            get { return _maDacQuyen; }
            set { _maDacQuyen = value; }
        }
        
        /// <summary>
        /// Ngày sinh
        /// </summary>
        public DateTime NgaySinh
        {
            get { return _ngaySinh; }
            set { _ngaySinh = value; }
        }
        
        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string SoDienThoai
        {
            get { return _soDienThoai; }
            set { _soDienThoai = value; }
        }
        
        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        #endregion

        #region Methods
        public NguoiDung(string maNguoiDung, string tenDangNhap, string matKhau, string hoTen,
            string gioiTinh,string maDacQuyen, DateTime ngaySinh, string soDienThoai, string email)
        {
            MaNguoiDung = maNguoiDung;
            TenDangNhap = tenDangNhap;
            MatKhau = matKhau;
            HoTen = hoTen;
            GioiTinh = gioiTinh;
            MaDacQuyen = maDacQuyen;
            NgaySinh = ngaySinh;
            SoDienThoai = soDienThoai;
            Email = email;
        }
        #endregion
    }
}
