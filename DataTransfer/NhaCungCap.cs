﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu của nhà cung cấp
    /// </summary>
    public class NhaCungCap
    {
        #region Fields
        private string _maNhaCungCap;
        private string _tenNhaCungCap;
        private string _diaChi;
        private string _soDienThoai;
        private string _email;
        #endregion

        #region Properties
        /// <summary>
        /// Mã nhà cung cấp
        /// </summary>
        public string MaNhaCungCap
        {
            get { return _maNhaCungCap; }
            set { _maNhaCungCap = value; }
        }
        
        /// <summary>
        /// Tên nhà cung cấp
        /// </summary>
        public string TenNhaCungCap
        {
            get { return _tenNhaCungCap; }
            set { _tenNhaCungCap = value; }
        }
        
        /// <summary>
        /// Địa chỉ
        /// </summary>
        public string DiaChi
        {
            get { return _diaChi; }
            set { _diaChi = value; }
        }
       
        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string SoDienThoai
        {
            get { return _soDienThoai; }
            set { _soDienThoai = value; }
        }
       
        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        #endregion

        #region Methods
        public NhaCungCap(string maNhaCungCap, string tenNhaCungCap, string diaChi,
            string soDienThoai,string email)
        {
            MaNhaCungCap = maNhaCungCap;
            TenNhaCungCap = tenNhaCungCap;
            DiaChi = diaChi;
            SoDienThoai = soDienThoai;
            Email = email;
        }
        #endregion

    }
}
