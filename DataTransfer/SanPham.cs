﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu Sản phẩm
    /// </summary>
    public class SanPham
    {
        #region Fields
        private string _maSanPham;
        private string _tenSanPham;
        private double _giaNhap;
        private double _giaBan;
        private string _maLoai;
        private int _tonKho;
        private string _moTa;
        private string _donVi;
        private double _vat;
        private double _chietKhau;
        #endregion

        #region Properties
        /// <summany>
        /// Mã sản phẩm
        /// </summany>
        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }

        /// <summany>
        /// Tên sản phẩm
        /// </summany>
        public string TenSanPham
        {
            get { return _tenSanPham; }
            set { _tenSanPham = value; }
        }

        /// <summany>
        /// Gía nhập
        /// </summany>
        public double GiaNhap
        {
            get { return _giaNhap; }
            set { _giaNhap = value; }
        }

        /// <summany>
        /// Gía bán
        /// </summany>
        public double GiaBan
        {
            get { return _giaBan; }
            set { _giaBan = value; }
        }

        /// <summany>
        /// Mã loại
        /// </summany>
        public string MaLoai
        {
            get { return _maLoai; }
            set { _maLoai = value; }
        }

        /// <summany>
        /// Tồn kho
        /// </summany>
        public int TonKho
        {
            get { return _tonKho; }
            set { _tonKho = value; }
        }

        /// <summany>
        /// Mô tả
        /// </summany>
        public string MoTa
        {
            get { return _moTa; }
            set { _moTa = value; }
        }

        /// <summany>
        /// Đơn vị
        /// </summany>
        public string DonVi
        {
            get { return _donVi; }
            set { _donVi = value; }
        }

        /// <summany>
        /// VAT
        /// </summany>
        public double VAT
        {
            get { return _vat; }
            set { _vat = value; }
        }

        /// <summany>
        /// Chiết khấu
        /// </summany>
        public double ChietKhau
        {
            get { return _chietKhau; }
            set { _chietKhau = value; }
        }
        #endregion

        #region Methods
        public SanPham(string maSanPham, string tenSanPham, double giaNhap,
            double giaBan, string maLoai, int tonKho, string moTa,
            string donVi, double vat, double chietKhau)
        {
            MaSanPham = maSanPham;
            TenSanPham = tenSanPham;
            GiaNhap = giaNhap;
            GiaBan = giaBan;
            MaLoai = maLoai;
            TonKho = tonKho;
            MoTa = moTa;
            DonVi = donVi;
            VAT = vat;
            ChietKhau = chietKhau;
        }
        #endregion
    }
}
