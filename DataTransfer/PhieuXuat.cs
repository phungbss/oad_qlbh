﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu phiếu xuất
    /// </summary>
    public class PhieuXuat
    {
        #region Fields
        private string _maPhieu;
        private string _maNhanVien;
        private DateTime _ngayLap;
        private double _tongTien;
        private string _ghiChu;
        #endregion

        #region Properties
        /// <summary>
        /// Mã phiếu
        /// </summary>
        public string MaPhieu
        {
            get { return _maPhieu; }
            set { _maPhieu = value; }
        }

        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set { _maNhanVien = value; }
        }

        /// <summary>
        /// Ngày lập
        /// </summary>
        public DateTime NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }

        /// <summary>
        /// Tổng tiền các sản phẩm
        /// </summary>
        public double TongTien
        {
            get { return _tongTien; }
            set { _tongTien = value; }
        }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public PhieuXuat(string maPhieu, string maNhanVien, DateTime ngayLap, double tongTien, string ghiChu)
        {
            MaPhieu = maPhieu;
            MaNhanVien = maNhanVien;
            NgayLap = ngayLap;
            TongTien = tongTien;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
