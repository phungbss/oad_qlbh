﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu đặc quyền người dùng
    /// </summary>
    public class DacQuyen
    {
        #region Fields
        private string _maDacQuyen;
        private string _tenDacQuyen;
        #endregion

        #region Properties
        /// <summary>
        /// Mã đặc quyền
        /// </summary>
        public string MaDacQuyen
        {
            get { return _maDacQuyen; }
            set { _maDacQuyen = value; }
        }

        /// <summary>
        /// Tên đặc quyền
        /// </summary>
        public string TenDacQuyen
        {
            get { return _tenDacQuyen; }
            set { _tenDacQuyen = value; }
        }
        #endregion

        #region Methods
        public DacQuyen(string maDacQuyen, string tenDacQuyen)
        {
            MaDacQuyen = maDacQuyen;
            TenDacQuyen = tenDacQuyen;
        }
        #endregion
    }
}
