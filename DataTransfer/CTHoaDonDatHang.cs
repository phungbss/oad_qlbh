﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    /// <summary>
    /// Chi tiết Hóa đơn đặt hàng
    /// </summary>
    public class CTHoaDonDatHang
    {
        #region Fields
        string _maHoaDon;
        string _maSanPham;
        int _soLuong;
        #endregion

        #region Properties
        /// <summany>
        /// Mã hóa đơn
        /// </summany>
        public string MaHoaDon
        {
            get { return _maHoaDon; }
            set { _maHoaDon = value; }
        }

        /// <summany>
        /// Mã sản phẩm
        /// </summany>
        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }

        /// <summany>
        /// Số lượng
        /// </summany>
        public int SoLuong
        {
            get { return _soLuong; }
            set { _soLuong = value; }
        }
        #endregion

        #region Methods
        public CTHoaDonDatHang(string maHoaDon, string maSanPham, int soLuong)
        {
            MaHoaDon = maHoaDon;
            MaSanPham = maSanPham;
            SoLuong = soLuong;
        }
        #endregion
    }
}
