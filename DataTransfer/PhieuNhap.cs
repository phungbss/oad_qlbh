﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu Phiếu Nhập
    /// </summary>
    public class PhieuNhap
    {
        #region Fields
        private string _maPhieu;
        private string _maNhanVien;
        private string _maNhaCungCap;
        private DateTime _ngayLap;
        private string _loaiPhieu;
        private double _tongTien;
        private string _ghiChu;
        #endregion

        #region Properties
        /// <summary>
        /// Mã phiếu
        /// </summary>
        public string MaPhieu
        {
            get { return _maPhieu; }
            set { _maPhieu = value; }
        }

        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set { _maNhanVien = value; }
        }

        /// <summary>
        /// Mã nhà cung cấp
        /// </summary>
        public string MaNhaCungCap
        {
            get { return _maNhaCungCap; }
            set { _maNhaCungCap = value; }
        }

        /// <summary>
        /// Ngày lập
        /// </summary>
        public DateTime NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }

        /// <summary>
        /// Loại phiếu
        /// </summary>
        public string LoaiPhieu
        {
            get { return _loaiPhieu; }
            set { _loaiPhieu = value; }
        }

        /// <summary>
        /// Tổng tiền các sản phẩm
        /// </summary>
        public double TongTien
        {
            get { return _tongTien; }
            set { _tongTien = value; }
        }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public PhieuNhap(string maPhieu, string maNhanVien, string maNhaCungCap, DateTime ngayLap,
            string loaiPhieu, double tongTien, string ghiChu)
        {
            MaPhieu = maPhieu;
            MaNhanVien = maNhanVien;
            MaNhaCungCap = maNhaCungCap;
            NgayLap = ngayLap;
            LoaiPhieu = loaiPhieu;
            TongTien = tongTien;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
