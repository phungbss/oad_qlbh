﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu Hóa đơn đặt hàng
    /// </summary>
    public class HoaDonDatHang
    {
        #region Fields
        private string _maHoaDon;
        private string _maKhachHang;
        private string _maNhanVien;
        private DateTime _ngayDuKienGiao;
        private DateTime _ngayLap;
        private double _tongTien;
        private double _thanhTien;
        private bool _daGiao;
        private string _ghiChu;
        #endregion

        #region Properties
        /// <summany>
        /// Mã hóa đơn
        /// </summany>
        public string MaHoaDon
        {
            get { return _maHoaDon; }
            set { _maHoaDon = value; }
        }

        /// <summany>
        /// Mã khách hàng
        /// </summany>
        public string MaKhachHang
        {
            get { return _maKhachHang; }
            set { _maKhachHang = value; }
        }

        /// <summany>
        /// Mã nhân viên
        /// </summany>
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set { _maNhanVien = value; }
        }

        /// <summany>
        /// Ngày dự kiến giao
        /// </summany>
        public DateTime NgayDuKienGiao
        {
            get { return _ngayDuKienGiao; }
            set { _ngayDuKienGiao = value; }
        }

        /// <summany>
        /// Ngày lập
        /// </summany>
        public DateTime NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }

        /// <summany>
        /// Tổng tiền
        /// </summany>
        public double TongTien
        {
            get { return _tongTien; }
            set { _tongTien = value; }
        }

        /// <summany>
        /// Thành tiền
        /// </summany>
        public double ThanhTien
        {
            get { return _thanhTien; }
            set { _thanhTien = value; }
        }

        /// <summany>
        /// Đã giao
        /// </summany>
        public bool DaGiao
        {
            get { return _daGiao; }
            set { _daGiao = value; }
        }

        /// <summany>
        /// Ghi chú
        /// </summany>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public HoaDonDatHang(string maHoaDon, string maKhachHang, string maNhanVien,
            DateTime ngayDuKienGiao, DateTime ngayLap, double tongTien, double thanhTien,
            bool daGiao, string ghiChu)
        {
            MaHoaDon = maHoaDon;
            MaKhachHang = maKhachHang;
            MaNhanVien = maNhanVien;
            NgayDuKienGiao = ngayDuKienGiao;
            NgayLap = ngayLap;
            TongTien = tongTien;
            ThanhTien = thanhTien;
            DaGiao = daGiao;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
