﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu phiếu thu
    /// </summary>
    public class PhieuThu
    {
        #region Fields
        private string _maPhieu;
        private string _maNhanVien;
        private DateTime _ngayLap;
        private string _tenNguoiNop;
        private double _soTien;
        private string _lyDo;
        #endregion

        #region Properties
        /// <summary>
        /// Mã phiếu
        /// </summary>
        public string MaPhieu
        {
            get { return _maPhieu; }
            set { _maPhieu = value; }
        }

        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set { _maNhanVien = value; }
        }
        /// <summary>
        /// Tên người nộp tiền
        /// </summary>
        public string TenNguoiNop
        {
            get { return _tenNguoiNop; }
            set { _tenNguoiNop = value; }
        }
        /// <summary>
        /// Ngày lập
        /// </summary>
        public DateTime NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }
        /// <summary>
        /// Số tiền
        /// </summary>
        public double SoTien
        {
            get { return _soTien; }
            set { _soTien = value; }
        }
        /// <summary>
        /// Lý do nộp tiền
        /// </summary>
        public string LyDo
        {
            get { return _lyDo; }
            set { _lyDo = value; }
        }
        #endregion

        #region Methods
        public PhieuThu(string maPhieu, string maNhanVien, DateTime ngayLap, string tenNguoiNop, double soTien, string lyDo)
        {
            MaPhieu = maPhieu;
            MaNhanVien = maNhanVien;
            NgayLap = ngayLap;
            TenNguoiNop = tenNguoiNop;
            SoTien = soTien;
            LyDo = lyDo;
        }
        #endregion
    }
}
