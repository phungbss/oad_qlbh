﻿using System;

namespace DataTransfer
{
    public class SaoLuuPhucHoi
    {
        #region FieldNames
        public static string MASAOLUU = "MaSaoLuu";
        public static string DUONGDAN = "DuongDan";
        public static string NGAYTHUCHIEN = "NgayThucHien";
        public static string NOIDUNG = "NoiDung";
        #endregion

        #region Fields
        private string maSaoLuu;
        private string duongDan;
        private DateTime ngayThucHien;
        private string noiDung;
        #endregion

        #region Properties
        public string MaSaoLuu
        {
            get { return maSaoLuu; }
        }

        public string DuongDan
        {
            get { return duongDan; }
        }

        public DateTime NgayThucHien
        {
            get { return ngayThucHien; }
        }

        public string NoiDung
        {
            get { return noiDung; }
        }
        #endregion

        #region Methods
        public SaoLuuPhucHoi(string maSaoLuu, string duongDan, DateTime ngayThucHien, string noiDung)
        {
            this.maSaoLuu = maSaoLuu;
            this.duongDan = duongDan;
            this.ngayThucHien = ngayThucHien;
            this.noiDung = noiDung;
        }
        #endregion
    }
}
