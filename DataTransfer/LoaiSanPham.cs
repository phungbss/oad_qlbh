﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    /// <summary>
    /// Chi tiết Loại sản phẩm
    /// </summary>
    public class LoaiSanPham
    {
        #region Fields
        string _maLoai;
        string _tenLoai;
        string _ghiChu;
        #endregion

        #region Properties
        /// <summany>
        /// Mã loại sản phẩm
        /// </summany>
        public string MaLoai
        {
            get { return _maLoai; }
            set { _maLoai = value; }
        }

        /// <summany>
        /// Tên loại sản phẩm
        /// </summany>
        public string TenLoai
        {
            get { return _tenLoai; }
            set { _tenLoai = value; }
        }

        /// <summany>
        /// Ghi chú
        /// </summany>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public LoaiSanPham(string maLoai, string tenLoai, string ghiChu)
        {
            MaLoai = maLoai;
            TenLoai = tenLoai;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
