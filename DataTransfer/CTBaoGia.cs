﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu Chi tiết báo giá
    /// </summary>
    public class CTBaoGia
    {
        #region Fields
        private string _maBaoGia;
        private string _maSanPham;
        private double _giaBan;
        private string _ghiChu;
        #endregion

        #region Properties
        /// <summary>
        /// Mã báo giá
        /// </summary>
        public string MaBaoGia
        {
            get { return _maBaoGia; }
            set { _maBaoGia = value; }
        }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }
        /// <summary>
        /// Giá bán
        /// </summary>
        public double GiaBan
        {
            get { return _giaBan; }
            set { _giaBan = value; }
        }
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public CTBaoGia(string maBaoGia, string maSanPham, double giaBan, string ghiChu)
        {
            MaBaoGia = maBaoGia;
            MaSanPham = maSanPham;
            GiaBan = giaBan;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
