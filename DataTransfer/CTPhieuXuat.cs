﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu của chi tiết phiếu xuất
    /// </summary>
    public class CTPhieuXuat
    {
        #region Fields
        private string _maPhieu;
        private string _maSanPham;
        private int _soLuong;
        private string _ghiChu;
        #endregion

        #region Properties
        /// <summary>
        /// Mã phiếu
        /// </summary>
        public string MaPhieu
        {
            get { return _maPhieu; }
            set { _maPhieu = value; }
        }

        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }

        /// <summary>
        /// Số lượng
        /// </summary>
        public int SoLuong
        {
            get { return _soLuong; }
            set { _soLuong = value; }
        }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public CTPhieuXuat(string maPhieu, string maSanPham, int soLuong, string ghiChu)
        {
            MaPhieu = maPhieu;
            MaSanPham = maSanPham;
            SoLuong = soLuong;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
