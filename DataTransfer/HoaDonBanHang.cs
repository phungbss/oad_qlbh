﻿using System;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu của Hóa đơn bán hàng
    /// </summary>
    public class HoaDonBanHang
    {
        #region Fields
        private string _maHoaDon;
        private string _maKhachHang;
        private string _maNhanVien;
        private DateTime _ngayLap;
        private double _tongTien;
        private double _thanhTien;
        private bool _daThu;
        private string _ghiChu;
        #endregion

        #region Properties    
        /// <summary>
        /// Mã hóa đơn
        /// </summary>
        public string MaHoaDon
        {
            get { return _maHoaDon; }
            set { _maHoaDon = value; }
        }

        /// <summary>
        /// Mã khách hàng
        /// </summary>
        public string MaKhachHang
        {
            get { return _maKhachHang; }
            set { _maKhachHang = value; }
        }
        
        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set { _maNhanVien = value; }
        }

        /// <summary>
        /// Ngày lập
        /// </summary>
        public DateTime NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }

        /// <summary>
        /// Tổng tiền các sản phẩm (đã bao gồm VAT)
        /// </summary>
        public double TongTien
        {
            get { return _tongTien; }
            set { _tongTien = value; }
        }

        /// <summary>
        /// Thành tiền
        /// </summary>
        public double ThanhTien
        {
            get { return _thanhTien; }
            set { _thanhTien = value; }
        }

        /// <summary>
        /// Đã thu hay chưa
        /// </summary>
        public bool DaThu
        {
            get { return _daThu; }
            set { _daThu = value; }
        }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        #endregion

        #region Methods
        public HoaDonBanHang(string maHoaDon, string maKhachHang, string maNhanVien,
            DateTime ngayLap, double tongTien, double thanhTien, bool daThu, string ghiChu)
        {
            MaHoaDon = maHoaDon;
            MaKhachHang = maKhachHang;
            MaNhanVien = maNhanVien;
            NgayLap = ngayLap;
            TongTien = tongTien;
            ThanhTien = thanhTien;
            DaThu = daThu;
            GhiChu = ghiChu;
        }
        #endregion
    }
}
