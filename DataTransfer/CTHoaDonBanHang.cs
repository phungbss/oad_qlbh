﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTransfer
{
    /// <summary>
    /// Chứa dữ liệu chi tiết của Hóa đơn bán hàng
    /// </summary>
    public class CTHoaDonBanHang
    {
        #region Fields
        private string _maHoaDon;
        private string _maSanPham;
        private int _soLuong;
        #endregion

        #region Properties
        /// <summary>
        /// Mã hóa đơn
        /// </summary>
        public string MaHoaDon
        {
            get { return _maHoaDon; }
            set { _maHoaDon = value; }
        }

        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }

        /// <summary>
        /// Số lượng
        /// </summary>
        public int SoLuong
        {
            get { return _soLuong; }
            set { _soLuong = value; }
        }
        #endregion

        #region Methods
        public CTHoaDonBanHang(string maHoaDon, string maSanPham, int soLuong)
        {
            MaHoaDon = maHoaDon;
            MaSanPham = maSanPham;
            SoLuong = soLuong;
        }
        #endregion
    }
}
