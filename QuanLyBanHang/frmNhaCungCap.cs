﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmNhaCungCap : DevExpress.XtraEditors.XtraForm
    {
        public frmNhaCungCap()
        {
            InitializeComponent();
            if (BUS.NhaCungCap == null)
                BUS.NhaCungCap = new BusinessLogic.NhaCungCap();
        }

        DataTransfer.NhaCungCap ncc;

        /// <summary>
        /// Nạp lên giao diện
        /// </summary>
        public void NapGiaoDien()
        {
            gridDSNCC.DataSource = BUS.NhaCungCap.Table;
            lblTongNCC.Text = string.Format("Tổng cộng: {0} nhà cung cấp", gridViewDSNCC.RowCount);
        }

        private void frmNhaCungCap_Load(object sender, EventArgs e)
        {
            NapGiaoDien();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Form nccEdit = new frmNhaCungCapEdit();
            nccEdit.Text = "Thêm nhà cung cấp";
            nccEdit.ShowDialog();
            NapGiaoDien();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            DataRow r = gridViewDSNCC.GetDataRow(gridViewDSNCC.FocusedRowHandle);
            ncc = new DataTransfer.NhaCungCap(r["MaNhaCungCap"].ToString(), r["TenNhaCungCap"].ToString(),
                r["DiaChi"].ToString(), r["SoDienThoai"].ToString(), r["Email"].ToString());

            Form nccEdit = new frmNhaCungCapEdit(ncc);
            nccEdit.Text = "Sửa thông tin nhà cung cấp";
            nccEdit.ShowDialog();
            NapGiaoDien();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewDSNCC.GetDataRow(gridViewDSNCC.FocusedRowHandle);
                if (XtraMessageBox.Show("Bạn có muốn xóa nhà cung cấp " + r["TenNhaCungCap"].ToString() + "?" + Environment.NewLine +
                    "Thao tác này sẽ xóa tất cả các Phiếu nhập của nhà cung cấp này!",
                    "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (BUS.NhaCungCap.Xoa(r["MaNhaCungCap"].ToString()))
                    {
                        XtraMessageBox.Show("Đã xóa nhà cung cấp thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NapGiaoDien();
                    }
                    else
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch
            {
                XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridDSNCC_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(sender, e);
        }
    }
}