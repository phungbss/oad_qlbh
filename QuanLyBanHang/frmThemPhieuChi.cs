﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmThemPhieuChi : DevExpress.XtraEditors.XtraForm
    {
        public BusinessLogic.PhieuChi pc_bus;
        public frmThemPhieuChi(BusinessLogic.PhieuChi pc = null)
        {
            pc_bus = pc;
            if (pc_bus == null)
                pc_bus = new BusinessLogic.PhieuChi();
            InitializeComponent();
        }
        /// <summary>
        /// nạp giao diện mặc định
        /// </summary>
        public void NapGiaoDien()
        {
            txtMaPhieu.Text = pc_bus.AutoGenerateID();
            dateNgayLap.EditValue = DateTime.Now;
            txtLyDo.Text = txtSoTien.Text = txtNguoiNop.Text = string.Empty;
        }

        private void txtSoTien_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsDigit(e.KeyChar)&&!Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void frmThemPhieuThu_Load(object sender, EventArgs e)
        {           
            NapGiaoDien();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(txtNguoiNop.Text != string.Empty && txtSoTien.Text != string.Empty)
            {
                try
                {
                    DataTransfer.PhieuChi pc = new DataTransfer.PhieuChi(txtMaPhieu.Text,
                        "ND000002", dateNgayLap.DateTime, txtNguoiNop.Text,
                        double.Parse(txtSoTien.Text), txtLyDo.Text);
                    if(pc_bus.Them(pc))
                    {
                        XtraMessageBox.Show("Phiếu chi đã được thêm thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập đầy đủ các thông tin", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}