﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyBanHang.Properties;

namespace QuanLyBanHang
{
    public partial class frmPhucHoi : DevExpress.XtraEditors.XtraForm
    {
        public frmPhucHoi()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }

        private string _Duongdan = Settings.Default.Backup_Path;

        /// <summary>
        /// Hiển thị giao diện phục hồi và dữ liệu lịch sử sao lưu
        /// </summary>
        private void ShowScreenPH()
        {
            GridSaoLuu.DataSource = BUS.SaoLuu.Table;
            gridViewSaoluu.ExpandAllGroups();
        }

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.SaoLuu == null)
                BUS.SaoLuu = new BusinessLogic.SaoLuu();
        }

        /// <summary>
        /// Hiển thị giao diện khi load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPhucHoi_Load(object sender, EventArgs e)
        {
            ShowScreenPH();
        }

        /// <summary>
        /// Lấy đường dẫn file sao lưu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog FileBrowser = new OpenFileDialog();
            FileBrowser.Filter = "Backup files (*.bak)|*.bak|All files (*.*)|*.*";

            if (FileBrowser.ShowDialog() == DialogResult.OK)
            {
                txtDuongdan.Text = FileBrowser.FileName;
            }
        }

        /// <summary>
        /// Thực hiện chức năng phục hồi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPhucHoi_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDuongdan.Text.Length == 0) // Kiểm tra đường dẫn có tồn tại
                    throw new IndexOutOfRangeException();

                if (BUS.SaoLuu.Restore(txtDuongdan.Text))
                {
                    #region Làm Mới BUS

                    BUS.BaoGia = null;
                    BUS.CTBaoGia = null;
                    BUS.CTPhieuNhap = null;
                    BUS.CTPhieuXuat = null;
                    BUS.CTHoaDonBanHang = null;
                    BUS.CTHoaDonDatHang = null;
                    BUS.DacQuyen = null;
                    BUS.HoaDonBanHang = null;
                    BUS.HoaDonDatHang = null;
                    BUS.KhachHang = null;
                    BUS.LoaiSanPham = null;
                    BUS.NhaCungCap = null;
                    BUS.NguoiDung = null;
                    BUS.PhieuChi = null;
                    BUS.PhieuNhap = null;
                    BUS.PhieuThu = null;
                    BUS.PhieuXuat = null;
                    BUS.SanPham = null;

                    #endregion

                    XtraMessageBox.Show("Phục hồi thành công !");

                    if (ckeDeleteAfterRestore.Checked)
                    {
                        frmSaoLuu frmSl = new frmSaoLuu();
                        frmSl.DeleteFile(txtDuongdan.Text);
                    }                  
                }
                else
                    XtraMessageBox.Show("Bản sao lưu không hợp lệ !");
            }
            catch (IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Bạn chưa chọn bản cần phục hồi !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch
            {
                XtraMessageBox.Show("Lỗi hệ thống !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ckeRestoredefault_CheckedChanged(object sender, EventArgs e)
        {
            if (ckeRestoredefault.Checked)
            {
                btnOpen.Enabled = false;
                ckeDeleteAfterRestore.Checked = false;
                ckeDeleteAfterRestore.Enabled = false;
                txtDuongdan.Text = Settings.Default.Backup_Path + @"\Sl_full.bak";
            }
            else
            {
                txtDuongdan.Text = string.Empty;
                btnOpen.Enabled = true;
                ckeDeleteAfterRestore.Enabled = true;
            }
        }
    }
}
