﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmLapPhieuXuat : DevExpress.XtraEditors.XtraForm
    {
        //BusinessLogic.SanPham sp_bus;
        //BusinessLogic.PhieuXuat px_bus;
        //BusinessLogic.CTPhieuXuat ctpx_bus;
        DataTable tbCTPX = new DataTable();

        public frmLapPhieuXuat()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
            KhoiTaoTableCTPX();
        }

        /// <summary>
        /// Nạp giao diện
        /// </summary>
        public void NapGiaoDien()
        {
            txtMaPhieu.Text = BUS.PhieuXuat.AutoGenerateID();
            dateNgayLap.EditValue = DateTime.Now;
            txtGhiChu.Text = string.Empty;
            labTongTien.Text = "đ";
            tbCTPX.Rows.Clear();
            txtChonSanPham.Text = string.Empty;
        }
        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
            if (BUS.PhieuXuat == null)
                BUS.PhieuXuat = new BusinessLogic.PhieuXuat();
            if (BUS.CTPhieuXuat == null)
                BUS.CTPhieuXuat = new BusinessLogic.CTPhieuXuat();
        }
        /// <summary>
        /// Khởi tạo bảng cho tbCTPX
        /// </summary>
        public void KhoiTaoTableCTPX()
        {
            tbCTPX.Columns.Add("MaSanPham");
            tbCTPX.Columns.Add("TenSanPham");
            tbCTPX.Columns.Add("SoLuong");
            tbCTPX.Columns.Add("DonVi");
            tbCTPX.Columns.Add("ThanhTien");
            tbCTPX.Columns.Add("GhiChu");
            tbCTPX.PrimaryKey = new DataColumn[] { tbCTPX.Columns[0] };
        }
        /// <summary>
        /// Cập nhật số tiền
        /// </summary>
        public void CapNhatSoTien()
        {
            long tongTien = 0;
            foreach (DataRow item in tbCTPX.Rows)
            {
                tongTien += Convert.ToInt32(item["ThanhTien"]);
            }
            labTongTien.Text = tongTien.ToString() + "đ";
        }

        private void frmLapPhieuXuat_Load(object sender, EventArgs e)
        {
            gridChonSP.DataSource = BUS.SanPham.Table;
            gridCTSP.DataSource = tbCTPX;
            labStateChonSP.Text = BUS.SanPham.Table.Rows.Count.ToString() + " sản phẩm";
            NapGiaoDien();
        }

        private void gridChonSP_Click(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSP.GetRowCellValue(gridViewChonSP.FocusedRowHandle, gridViewChonSP.Columns[0]).ToString();
                
            }
            catch { }
        }

        private void btnChonSanPham_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = tbCTPX.NewRow();
                DataRow _r = gridViewChonSP.GetDataRow(gridViewChonSP.FocusedRowHandle);
                r["MaSanPham"] = _r["MaSanPham"];
                r["TenSanPham"] = _r["TenSanPham"];
                r["DonVi"] = _r["DonVi"];
                r["SoLuong"] = 1;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                tbCTPX.Rows.Add(r);
                gridViewCTSP.SelectRow(gridViewCTSP.FindRow(r));
                gridViewCTSP.Focus();
                labStateDSSPDaChon.Text = tbCTPX.Rows.Count.ToString() + " sản phẩm trong danh sách";
                CapNhatSoTien();
            }
            catch (ConstraintException)
            {
                XtraMessageBox.Show("Bạn đã chọn sản phẩm " + txtChonSanPham.Text + " rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridChonSP_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSP.GetRowCellValue(gridViewChonSP.FocusedRowHandle, gridViewChonSP.Columns[0]).ToString();
                btnChonSanPham_Click(sender, e);
            }
            catch { }
        }

        private void btnTang_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                r["SoLuong"] = Convert.ToInt32(r["SoLuong"]) + 1;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);
                CapNhatSoTien();
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnGiam_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                int s1 = Convert.ToInt32(r["SoLuong"]);
                r["SoLuong"] = s1 > 1 ? s1 - 1 : s1;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                CapNhatSoTien();
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                tbCTPX.Rows.Remove(gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle));
                CapNhatSoTien();
            }
            catch
            {
                XtraMessageBox.Show("Không thể xóa khi danh sách trống!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoaTatCa_Click(object sender, EventArgs e)
        {
            tbCTPX.Rows.Clear();
            labTongTien.Text = "0đ";
        }

        private void btnTaoPhieu_Click(object sender, EventArgs e)
        {
            if(tbCTPX.Rows.Count > 0)
            {
                try
                {
                    //
                    DataTransfer.PhieuXuat px = new DataTransfer.PhieuXuat(txtMaPhieu.Text,
                        "ND000002", dateNgayLap.DateTime,
                        Convert.ToDouble(labTongTien.Text.Substring(0, labTongTien.Text.Length - 1)), txtGhiChu.Text);
                    if (BUS.PhieuXuat.Them(px))
                    {
                        XtraMessageBox.Show("Phiếu xuất " + txtMaPhieu.Text + " đã được thêm thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    //
                    DataTransfer.CTPhieuXuat ct;
                    foreach (DataRow r in tbCTPX.Rows)
                    {
                        ct = new DataTransfer.CTPhieuXuat(txtMaPhieu.Text, r["MaSanPham"].ToString(), Convert.ToInt32(r["SoLuong"]), "");
                        BUS.CTPhieuXuat.Them(ct);
                    }
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Chưa có sản phẩm nào trong Phiếu Xuất", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            NapGiaoDien();
            labStateDSSPDaChon.Text = "Chưa có sản phẩm nào được chọn";
        }
    }
}