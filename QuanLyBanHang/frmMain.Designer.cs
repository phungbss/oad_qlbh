﻿namespace QuanLyBanHang
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.btnLapHDBH = new DevExpress.XtraBars.BarButtonItem();
            this.btnHDBH = new DevExpress.XtraBars.BarButtonItem();
            this.btnLapHDDH = new DevExpress.XtraBars.BarButtonItem();
            this.btnHDDH = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnXuatKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhoHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnLapPhieuThu = new DevExpress.XtraBars.BarButtonItem();
            this.btnLapPhieuChi = new DevExpress.XtraBars.BarButtonItem();
            this.btnTinhTrangThuChi = new DevExpress.XtraBars.BarButtonItem();
            this.btnLapBaoGia = new DevExpress.XtraBars.BarButtonItem();
            this.btnBaoGia = new DevExpress.XtraBars.BarButtonItem();
            this.btnDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuanLyNguoiDung = new DevExpress.XtraBars.BarButtonItem();
            this.btnCaiDatNguoiDung = new DevExpress.XtraBars.BarButtonItem();
            this.btnSaoLuu = new DevExpress.XtraBars.BarButtonItem();
            this.btnPhucHoi = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongTinCuaHang = new DevExpress.XtraBars.BarButtonItem();
            this.barWorkspaceMenuItem1 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.barHeaderItem1 = new DevExpress.XtraBars.BarHeaderItem();
            this.barWorkspaceMenuItem2 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.barDockingMenuItem1 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barWorkspaceMenuItem3 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.btnTroGiup = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongTinPhanMem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.btnSanPham = new DevExpress.XtraBars.BarButtonItem();
            this.btnLoaiSanPham = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhaCungCap = new DevExpress.XtraBars.BarButtonItem();
            this.lblTime = new DevExpress.XtraBars.BarStaticItem();
            this.lblStoreName = new DevExpress.XtraBars.BarStaticItem();
            this.lblDate = new DevExpress.XtraBars.BarStaticItem();
            this.lblCurrentUser = new DevExpress.XtraBars.BarStaticItem();
            this.btnTrangDau = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.defaultLookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tmTimeNow = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.skinRibbonGalleryBarItem1,
            this.btnLapHDBH,
            this.btnHDBH,
            this.btnLapHDDH,
            this.btnHDDH,
            this.btnNhapKho,
            this.btnXuatKho,
            this.btnKhoHang,
            this.btnLapPhieuThu,
            this.btnLapPhieuChi,
            this.btnTinhTrangThuChi,
            this.btnLapBaoGia,
            this.btnBaoGia,
            this.btnDangXuat,
            this.btnQuanLyNguoiDung,
            this.btnCaiDatNguoiDung,
            this.btnSaoLuu,
            this.btnPhucHoi,
            this.btnThongTinCuaHang,
            this.barWorkspaceMenuItem1,
            this.barHeaderItem1,
            this.barWorkspaceMenuItem2,
            this.barDockingMenuItem1,
            this.barSubItem1,
            this.barButtonGroup1,
            this.barButtonItem19,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barWorkspaceMenuItem3,
            this.barButtonItem22,
            this.barButtonItem23,
            this.btnTroGiup,
            this.btnThongTinPhanMem,
            this.barButtonItem26,
            this.btnSanPham,
            this.btnLoaiSanPham,
            this.btnKhachHang,
            this.btnNhaCungCap,
            this.lblTime,
            this.lblStoreName,
            this.lblDate,
            this.lblCurrentUser,
            this.btnTrangDau});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 164;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageHeaderItemLinks.Add(this.lblCurrentUser);
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage3,
            this.ribbonPage4});
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(950, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.TransparentEditors = true;
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 1;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // btnLapHDBH
            // 
            this.btnLapHDBH.Caption = "Lập hóa đơn mới";
            this.btnLapHDBH.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLapHDBH.Glyph")));
            this.btnLapHDBH.Id = 3;
            this.btnLapHDBH.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnLapHDBH.LargeGlyph")));
            this.btnLapHDBH.Name = "btnLapHDBH";
            this.btnLapHDBH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLapHDBH_ItemClick);
            // 
            // btnHDBH
            // 
            this.btnHDBH.Caption = "Xem các hóa đơn";
            this.btnHDBH.Glyph = ((System.Drawing.Image)(resources.GetObject("btnHDBH.Glyph")));
            this.btnHDBH.Id = 4;
            this.btnHDBH.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnHDBH.LargeGlyph")));
            this.btnHDBH.Name = "btnHDBH";
            this.btnHDBH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHDBH_ItemClick);
            // 
            // btnLapHDDH
            // 
            this.btnLapHDDH.Caption = "Lập hóa đơn mới";
            this.btnLapHDDH.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLapHDDH.Glyph")));
            this.btnLapHDDH.Id = 5;
            this.btnLapHDDH.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnLapHDDH.LargeGlyph")));
            this.btnLapHDDH.Name = "btnLapHDDH";
            this.btnLapHDDH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLapHDDH_ItemClick);
            // 
            // btnHDDH
            // 
            this.btnHDDH.Caption = "Xem các hóa đơn";
            this.btnHDDH.Glyph = ((System.Drawing.Image)(resources.GetObject("btnHDDH.Glyph")));
            this.btnHDDH.Id = 6;
            this.btnHDDH.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnHDDH.LargeGlyph")));
            this.btnHDDH.Name = "btnHDDH";
            this.btnHDDH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHDDH_ItemClick);
            // 
            // btnNhapKho
            // 
            this.btnNhapKho.Caption = "Nhập kho";
            this.btnNhapKho.Glyph = ((System.Drawing.Image)(resources.GetObject("btnNhapKho.Glyph")));
            this.btnNhapKho.Id = 7;
            this.btnNhapKho.Name = "btnNhapKho";
            this.btnNhapKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhapKho_ItemClick);
            // 
            // btnXuatKho
            // 
            this.btnXuatKho.Caption = "Xuất kho";
            this.btnXuatKho.Glyph = ((System.Drawing.Image)(resources.GetObject("btnXuatKho.Glyph")));
            this.btnXuatKho.Id = 8;
            this.btnXuatKho.Name = "btnXuatKho";
            this.btnXuatKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnXuatKho_ItemClick);
            // 
            // btnKhoHang
            // 
            this.btnKhoHang.Caption = "Xem tình trạng";
            this.btnKhoHang.Glyph = global::QuanLyBanHang.Properties.Resources.icon_Kho_32dp;
            this.btnKhoHang.Id = 9;
            this.btnKhoHang.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_Kho_32dp;
            this.btnKhoHang.Name = "btnKhoHang";
            this.btnKhoHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhoHang_ItemClick);
            // 
            // btnLapPhieuThu
            // 
            this.btnLapPhieuThu.Caption = "Lập phiếu thu";
            this.btnLapPhieuThu.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLapPhieuThu.Glyph")));
            this.btnLapPhieuThu.Id = 10;
            this.btnLapPhieuThu.Name = "btnLapPhieuThu";
            this.btnLapPhieuThu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLapPhieuThu_ItemClick);
            // 
            // btnLapPhieuChi
            // 
            this.btnLapPhieuChi.Caption = "Lập phiếu chi";
            this.btnLapPhieuChi.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLapPhieuChi.Glyph")));
            this.btnLapPhieuChi.Id = 11;
            this.btnLapPhieuChi.Name = "btnLapPhieuChi";
            this.btnLapPhieuChi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLapPhieuChi_ItemClick);
            // 
            // btnTinhTrangThuChi
            // 
            this.btnTinhTrangThuChi.Caption = "Tình trạng thu chi";
            this.btnTinhTrangThuChi.Glyph = global::QuanLyBanHang.Properties.Resources.icon_ThuChi_32dp;
            this.btnTinhTrangThuChi.Id = 12;
            this.btnTinhTrangThuChi.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_ThuChi_32dp;
            this.btnTinhTrangThuChi.Name = "btnTinhTrangThuChi";
            this.btnTinhTrangThuChi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTinhTrangThuChi_ItemClick);
            // 
            // btnLapBaoGia
            // 
            this.btnLapBaoGia.Caption = "Lập báo giá";
            this.btnLapBaoGia.Glyph = global::QuanLyBanHang.Properties.Resources.icon_LapBaoGia_32dp;
            this.btnLapBaoGia.Id = 13;
            this.btnLapBaoGia.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_LapBaoGia_32dp;
            this.btnLapBaoGia.Name = "btnLapBaoGia";
            this.btnLapBaoGia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLapBaoGia_ItemClick);
            // 
            // btnBaoGia
            // 
            this.btnBaoGia.Caption = "Xem các báo giá";
            this.btnBaoGia.Glyph = global::QuanLyBanHang.Properties.Resources.icon_BaoGia_32dp;
            this.btnBaoGia.Id = 14;
            this.btnBaoGia.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_BaoGia_32dp;
            this.btnBaoGia.Name = "btnBaoGia";
            this.btnBaoGia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBaoGia_ItemClick);
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Caption = "Đăng xuất";
            this.btnDangXuat.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDangXuat.Glyph")));
            this.btnDangXuat.Id = 15;
            this.btnDangXuat.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnDangXuat.LargeGlyph")));
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDangXuat_ItemClick);
            // 
            // btnQuanLyNguoiDung
            // 
            this.btnQuanLyNguoiDung.Caption = "Quản lý người dùng";
            this.btnQuanLyNguoiDung.Glyph = ((System.Drawing.Image)(resources.GetObject("btnQuanLyNguoiDung.Glyph")));
            this.btnQuanLyNguoiDung.Id = 16;
            this.btnQuanLyNguoiDung.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnQuanLyNguoiDung.LargeGlyph")));
            this.btnQuanLyNguoiDung.Name = "btnQuanLyNguoiDung";
            this.btnQuanLyNguoiDung.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuanLyNguoiDung_ItemClick);
            // 
            // btnCaiDatNguoiDung
            // 
            this.btnCaiDatNguoiDung.Caption = "Cài đặt";
            this.btnCaiDatNguoiDung.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCaiDatNguoiDung.Glyph")));
            this.btnCaiDatNguoiDung.Id = 17;
            this.btnCaiDatNguoiDung.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCaiDatNguoiDung.LargeGlyph")));
            this.btnCaiDatNguoiDung.Name = "btnCaiDatNguoiDung";
            this.btnCaiDatNguoiDung.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCaiDatNguoiDung_ItemClick);
            // 
            // btnSaoLuu
            // 
            this.btnSaoLuu.Caption = "Sao lưu";
            this.btnSaoLuu.Glyph = global::QuanLyBanHang.Properties.Resources.icon_SaoLuu_32dp;
            this.btnSaoLuu.Id = 18;
            this.btnSaoLuu.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_SaoLuu_32dp;
            this.btnSaoLuu.Name = "btnSaoLuu";
            this.btnSaoLuu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSaoLuu_ItemClick);
            // 
            // btnPhucHoi
            // 
            this.btnPhucHoi.Caption = "Phục hồi";
            this.btnPhucHoi.Glyph = global::QuanLyBanHang.Properties.Resources.icon_PhucHoi_32dp;
            this.btnPhucHoi.Id = 19;
            this.btnPhucHoi.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_PhucHoi_32dp;
            this.btnPhucHoi.Name = "btnPhucHoi";
            this.btnPhucHoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPhucHoi_ItemClick);
            // 
            // btnThongTinCuaHang
            // 
            this.btnThongTinCuaHang.Caption = "Thông tin cửa hàng";
            this.btnThongTinCuaHang.Glyph = global::QuanLyBanHang.Properties.Resources.icon_CuaHang_32dp;
            this.btnThongTinCuaHang.Id = 20;
            this.btnThongTinCuaHang.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_CuaHang_32dp;
            this.btnThongTinCuaHang.Name = "btnThongTinCuaHang";
            this.btnThongTinCuaHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongTinCuaHang_ItemClick);
            // 
            // barWorkspaceMenuItem1
            // 
            this.barWorkspaceMenuItem1.Caption = "Đang đăng nhập";
            this.barWorkspaceMenuItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barWorkspaceMenuItem1.Glyph")));
            this.barWorkspaceMenuItem1.Id = 21;
            this.barWorkspaceMenuItem1.Name = "barWorkspaceMenuItem1";
            // 
            // barHeaderItem1
            // 
            this.barHeaderItem1.Caption = "<user>";
            this.barHeaderItem1.Id = 22;
            this.barHeaderItem1.Name = "barHeaderItem1";
            // 
            // barWorkspaceMenuItem2
            // 
            this.barWorkspaceMenuItem2.Caption = "barWorkspaceMenuItem2";
            this.barWorkspaceMenuItem2.Id = 23;
            this.barWorkspaceMenuItem2.Name = "barWorkspaceMenuItem2";
            // 
            // barDockingMenuItem1
            // 
            this.barDockingMenuItem1.Caption = "barDockingMenuItem1";
            this.barDockingMenuItem1.Id = 24;
            this.barDockingMenuItem1.Name = "barDockingMenuItem1";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "<user>";
            this.barSubItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.Glyph")));
            this.barSubItem1.Id = 25;
            this.barSubItem1.MenuCaption = "<user>";
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.ShowMenuCaption = true;
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "<username>";
            this.barButtonGroup1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonGroup1.Glyph")));
            this.barButtonGroup1.Id = 26;
            this.barButtonGroup1.ItemLinks.Add(this.barButtonItem19);
            this.barButtonGroup1.ItemLinks.Add(this.barButtonItem20);
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "barButtonItem19";
            this.barButtonItem19.Id = 27;
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "barButtonItem20";
            this.barButtonItem20.Id = 28;
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "<username>";
            this.barButtonItem21.Id = 29;
            this.barButtonItem21.Name = "barButtonItem21";
            // 
            // barWorkspaceMenuItem3
            // 
            this.barWorkspaceMenuItem3.Caption = "barWorkspaceMenuItem3";
            this.barWorkspaceMenuItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barWorkspaceMenuItem3.Glyph")));
            this.barWorkspaceMenuItem3.Id = 30;
            this.barWorkspaceMenuItem3.MenuCaption = "gf";
            this.barWorkspaceMenuItem3.Name = "barWorkspaceMenuItem3";
            this.barWorkspaceMenuItem3.ShowMenuCaption = true;
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "Sản phẩm";
            this.barButtonItem22.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.Glyph")));
            this.barButtonItem22.Id = 31;
            this.barButtonItem22.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.LargeGlyph")));
            this.barButtonItem22.Name = "barButtonItem22";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "Loại sản phẩm";
            this.barButtonItem23.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.Glyph")));
            this.barButtonItem23.Id = 32;
            this.barButtonItem23.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.LargeGlyph")));
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // btnTroGiup
            // 
            this.btnTroGiup.Caption = "Trợ giúp";
            this.btnTroGiup.Glyph = ((System.Drawing.Image)(resources.GetObject("btnTroGiup.Glyph")));
            this.btnTroGiup.Id = 33;
            this.btnTroGiup.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnTroGiup.LargeGlyph")));
            this.btnTroGiup.Name = "btnTroGiup";
            this.btnTroGiup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTroGiup_ItemClick);
            // 
            // btnThongTinPhanMem
            // 
            this.btnThongTinPhanMem.Caption = "Thông tin phần mềm";
            this.btnThongTinPhanMem.Glyph = ((System.Drawing.Image)(resources.GetObject("btnThongTinPhanMem.Glyph")));
            this.btnThongTinPhanMem.Id = 34;
            this.btnThongTinPhanMem.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnThongTinPhanMem.LargeGlyph")));
            this.btnThongTinPhanMem.Name = "btnThongTinPhanMem";
            this.btnThongTinPhanMem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongTinPhanMem_ItemClick);
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "barButtonItem26";
            this.barButtonItem26.Id = 35;
            this.barButtonItem26.Name = "barButtonItem26";
            // 
            // btnSanPham
            // 
            this.btnSanPham.Caption = "Sản phẩm";
            this.btnSanPham.Glyph = global::QuanLyBanHang.Properties.Resources.icon_SanPham_32dp;
            this.btnSanPham.Id = 36;
            this.btnSanPham.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_SanPham_32dp;
            this.btnSanPham.Name = "btnSanPham";
            this.btnSanPham.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSanPham_ItemClick);
            // 
            // btnLoaiSanPham
            // 
            this.btnLoaiSanPham.Caption = "Loại sản phẩm";
            this.btnLoaiSanPham.Glyph = global::QuanLyBanHang.Properties.Resources.icon_LoaiSanPham_32dp;
            this.btnLoaiSanPham.Id = 37;
            this.btnLoaiSanPham.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_LoaiSanPham_32dp;
            this.btnLoaiSanPham.Name = "btnLoaiSanPham";
            this.btnLoaiSanPham.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLoaiSanPham_ItemClick);
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Caption = "Khách hàng";
            this.btnKhachHang.Glyph = global::QuanLyBanHang.Properties.Resources.icon_KhachHang_32dp;
            this.btnKhachHang.Id = 38;
            this.btnKhachHang.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_KhachHang_32dp;
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhachHang_ItemClick);
            // 
            // btnNhaCungCap
            // 
            this.btnNhaCungCap.Caption = "Nhà cung cấp";
            this.btnNhaCungCap.Glyph = global::QuanLyBanHang.Properties.Resources.icon_NhaCungCap_32dp;
            this.btnNhaCungCap.Id = 39;
            this.btnNhaCungCap.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_NhaCungCap_32dp;
            this.btnNhaCungCap.Name = "btnNhaCungCap";
            this.btnNhaCungCap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhaCungCap_ItemClick);
            // 
            // lblTime
            // 
            this.lblTime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblTime.Caption = "<time now>";
            this.lblTime.Glyph = ((System.Drawing.Image)(resources.GetObject("lblTime.Glyph")));
            this.lblTime.Id = 42;
            this.lblTime.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("lblTime.LargeGlyph")));
            this.lblTime.Name = "lblTime";
            this.lblTime.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblStoreName
            // 
            this.lblStoreName.Caption = "<store name>";
            this.lblStoreName.Id = 47;
            this.lblStoreName.Name = "lblStoreName";
            this.lblStoreName.TextAlignment = System.Drawing.StringAlignment.Near;
            this.lblStoreName.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.lblStoreName_ItemClick);
            // 
            // lblDate
            // 
            this.lblDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblDate.Caption = "<date now>";
            this.lblDate.Glyph = ((System.Drawing.Image)(resources.GetObject("lblDate.Glyph")));
            this.lblDate.Id = 48;
            this.lblDate.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("lblDate.LargeGlyph")));
            this.lblDate.Name = "lblDate";
            this.lblDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblCurrentUser
            // 
            this.lblCurrentUser.Caption = "<user>";
            this.lblCurrentUser.Glyph = ((System.Drawing.Image)(resources.GetObject("lblCurrentUser.Glyph")));
            this.lblCurrentUser.Id = 50;
            this.lblCurrentUser.Name = "lblCurrentUser";
            this.lblCurrentUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnTrangDau
            // 
            this.btnTrangDau.Caption = "Trang đầu";
            this.btnTrangDau.Id = 163;
            this.btnTrangDau.LargeGlyph = global::QuanLyBanHang.Properties.Resources.icon_CuaHang_32dp;
            this.btnTrangDau.Name = "btnTrangDau";
            this.btnTrangDau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTrangDau_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6,
            this.ribbonPageGroup12,
            this.ribbonPageGroup13});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Nghiệp vụ";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnLapHDBH);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnHDBH);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Bán hàng";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnLapHDDH);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnHDDH);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Đặt hàng";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnNhapKho);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnXuatKho);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnKhoHang);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "Kho hàng";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.btnLapPhieuThu);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnLapPhieuChi);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnTinhTrangThuChi);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Thu chi";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.Glyph = global::QuanLyBanHang.Properties.Resources.icon_BaoGia_32dp;
            this.ribbonPageGroup6.ItemLinks.Add(this.btnLapBaoGia);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnBaoGia);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "Báo giá";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.ItemLinks.Add(this.btnSanPham);
            this.ribbonPageGroup12.ItemLinks.Add(this.btnLoaiSanPham);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            this.ribbonPageGroup12.ShowCaptionButton = false;
            this.ribbonPageGroup12.Text = "Sản phẩm";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.Glyph = global::QuanLyBanHang.Properties.Resources.icon_NhaCungCap_32dp;
            this.ribbonPageGroup13.ItemLinks.Add(this.btnKhachHang);
            this.ribbonPageGroup13.ItemLinks.Add(this.btnNhaCungCap);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.ShowCaptionButton = false;
            this.ribbonPageGroup13.Text = "Đầu mối";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup7,
            this.ribbonPageGroup8,
            this.ribbonPageGroup9});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Hệ thống";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Giao diện";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.Glyph = ((System.Drawing.Image)(resources.GetObject("ribbonPageGroup7.Glyph")));
            this.ribbonPageGroup7.ItemLinks.Add(this.btnDangXuat);
            this.ribbonPageGroup7.ItemLinks.Add(this.btnCaiDatNguoiDung);
            this.ribbonPageGroup7.ItemLinks.Add(this.btnQuanLyNguoiDung);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Người dùng";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.Glyph = global::QuanLyBanHang.Properties.Resources.icon_SaoLuu_16dp;
            this.ribbonPageGroup8.ItemLinks.Add(this.btnSaoLuu);
            this.ribbonPageGroup8.ItemLinks.Add(this.btnPhucHoi);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "Dữ liệu";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.btnThongTinCuaHang);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.ShowCaptionButton = false;
            this.ribbonPageGroup9.Text = "Cài đặt";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup11});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Trợ giúp";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.btnTroGiup);
            this.ribbonPageGroup11.ItemLinks.Add(this.btnThongTinPhanMem);
            this.ribbonPageGroup11.ItemLinks.Add(this.btnTrangDau);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.ShowCaptionButton = false;
            this.ribbonPageGroup11.Text = "Trợ giúp";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.lblTime);
            this.ribbonStatusBar.ItemLinks.Add(this.lblStoreName);
            this.ribbonStatusBar.ItemLinks.Add(this.lblDate);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 546);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(950, 23);
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "Office 2013";
            // 
            // xtraTabbedMdiManager
            // 
            this.xtraTabbedMdiManager.MdiParent = this;
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.barButtonItem22);
            this.ribbonPageGroup10.ItemLinks.Add(this.barButtonItem23);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "Sản phẩm";
            // 
            // tmTimeNow
            // 
            this.tmTimeNow.Tick += new System.EventHandler(this.tmTimeNow_Tick);
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 569);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Quản lý cửa hàng tạp hóa N17";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel;
        private DevExpress.XtraBars.BarButtonItem btnLapHDBH;
        private DevExpress.XtraBars.BarButtonItem btnHDBH;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btnLapHDDH;
        private DevExpress.XtraBars.BarButtonItem btnHDDH;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem btnNhapKho;
        private DevExpress.XtraBars.BarButtonItem btnXuatKho;
        private DevExpress.XtraBars.BarButtonItem btnKhoHang;
        private DevExpress.XtraBars.BarButtonItem btnLapPhieuThu;
        private DevExpress.XtraBars.BarButtonItem btnLapPhieuChi;
        private DevExpress.XtraBars.BarButtonItem btnTinhTrangThuChi;
        private DevExpress.XtraBars.BarButtonItem btnLapBaoGia;
        private DevExpress.XtraBars.BarButtonItem btnBaoGia;
        private DevExpress.XtraBars.BarButtonItem btnDangXuat;
        private DevExpress.XtraBars.BarButtonItem btnQuanLyNguoiDung;
        private DevExpress.XtraBars.BarButtonItem btnCaiDatNguoiDung;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem btnSaoLuu;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem btnPhucHoi;
        private DevExpress.XtraBars.BarButtonItem btnThongTinCuaHang;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem1;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItem1;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem2;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem3;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem btnTroGiup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.BarButtonItem btnThongTinPhanMem;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarButtonItem btnSanPham;
        private DevExpress.XtraBars.BarButtonItem btnLoaiSanPham;
        private DevExpress.XtraBars.BarButtonItem btnKhachHang;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
        private DevExpress.XtraBars.BarButtonItem btnNhaCungCap;
        private DevExpress.XtraBars.BarStaticItem lblTime;
        private DevExpress.XtraBars.BarStaticItem lblStoreName;
        private System.Windows.Forms.Timer tmTimeNow;
        private DevExpress.XtraBars.BarStaticItem lblDate;
        private DevExpress.XtraBars.BarStaticItem lblCurrentUser;
        private DevExpress.XtraBars.BarButtonItem btnTrangDau;
    }
}