﻿namespace QuanLyBanHang
{
    partial class frmQuanLyThuChi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            //FRM.frmQLTC = null;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQuanLyThuChi));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnShowPC = new DevExpress.XtraEditors.SimpleButton();
            this.btnShowPT = new DevExpress.XtraEditors.SimpleButton();
            this.gridPChi = new DevExpress.XtraGrid.GridControl();
            this.gridViewPChi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridPThu = new DevExpress.XtraGrid.GridControl();
            this.gridViewPThu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.chkTimTheoMPChi = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnTimPChi = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaPChi = new DevExpress.XtraEditors.TextEdit();
            this.dateToChi = new DevExpress.XtraEditors.DateEdit();
            this.dateFromChi = new DevExpress.XtraEditors.DateEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnXoaPChi = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaPChi = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemPChi = new DevExpress.XtraEditors.SimpleButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.chkTimTheoMPThu = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnTimPThu = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaPThu = new DevExpress.XtraEditors.TextEdit();
            this.dateToThu = new DevExpress.XtraEditors.DateEdit();
            this.dateFromThu = new DevExpress.XtraEditors.DateEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnXoaPThu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaPThu = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemPThu = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labStatePT = new DevExpress.XtraLayout.SimpleLabelItem();
            this.labStatePC = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPThu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPThu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToChi.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromChi.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPThu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToThu.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToThu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromThu.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromThu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatePT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatePC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnShowPC);
            this.layoutControl1.Controls.Add(this.btnShowPT);
            this.layoutControl1.Controls.Add(this.gridPChi);
            this.layoutControl1.Controls.Add(this.gridPThu);
            this.layoutControl1.Controls.Add(this.groupControl4);
            this.layoutControl1.Controls.Add(this.groupControl3);
            this.layoutControl1.Controls.Add(this.label2);
            this.layoutControl1.Controls.Add(this.label1);
            this.layoutControl1.Controls.Add(this.groupControl2);
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(597, 173, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1123, 557);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnShowPC
            // 
            this.btnShowPC.Image = ((System.Drawing.Image)(resources.GetObject("btnShowPC.Image")));
            this.btnShowPC.Location = new System.Drawing.Point(1005, 523);
            this.btnShowPC.Name = "btnShowPC";
            this.btnShowPC.Size = new System.Drawing.Size(106, 22);
            this.btnShowPC.StyleController = this.layoutControl1;
            this.btnShowPC.TabIndex = 8;
            this.btnShowPC.Text = "Hiện tất cả";
            this.btnShowPC.Click += new System.EventHandler(this.btnShowPC_Click);
            // 
            // btnShowPT
            // 
            this.btnShowPT.Image = ((System.Drawing.Image)(resources.GetObject("btnShowPT.Image")));
            this.btnShowPT.Location = new System.Drawing.Point(457, 523);
            this.btnShowPT.Name = "btnShowPT";
            this.btnShowPT.Size = new System.Drawing.Size(103, 22);
            this.btnShowPT.StyleController = this.layoutControl1;
            this.btnShowPT.TabIndex = 7;
            this.btnShowPT.Text = "Hiện tất cả";
            this.btnShowPT.Click += new System.EventHandler(this.btnShowPT_Click);
            // 
            // gridPChi
            // 
            this.gridPChi.Location = new System.Drawing.Point(564, 126);
            this.gridPChi.MainView = this.gridViewPChi;
            this.gridPChi.Name = "gridPChi";
            this.gridPChi.Size = new System.Drawing.Size(547, 393);
            this.gridPChi.TabIndex = 6;
            this.gridPChi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPChi});
            // 
            // gridViewPChi
            // 
            this.gridViewPChi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridViewPChi.GridControl = this.gridPChi;
            this.gridViewPChi.Name = "gridViewPChi";
            this.gridViewPChi.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPChi.OptionsView.ShowGroupPanel = false;
            this.gridViewPChi.RowCountChanged += new System.EventHandler(this.gridViewPChi_RowCountChanged);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Mã phiếu";
            this.gridColumn7.FieldName = "MaPhieu";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 63;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Nhân viên lập";
            this.gridColumn8.FieldName = "MaNhanVien";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Ngày lập";
            this.gridColumn9.FieldName = "NgayLap";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 102;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Người nhận";
            this.gridColumn10.FieldName = "TenNguoiNhan";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            this.gridColumn10.Width = 112;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.gridColumn11.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn11.Caption = "Số tiền";
            this.gridColumn11.FieldName = "SoTien";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            this.gridColumn11.Width = 96;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Lý do";
            this.gridColumn12.FieldName = "LyDo";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            // 
            // gridPThu
            // 
            this.gridPThu.Location = new System.Drawing.Point(12, 126);
            this.gridPThu.MainView = this.gridViewPThu;
            this.gridPThu.Name = "gridPThu";
            this.gridPThu.Size = new System.Drawing.Size(548, 393);
            this.gridPThu.TabIndex = 5;
            this.gridPThu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPThu});
            // 
            // gridViewPThu
            // 
            this.gridViewPThu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridViewPThu.GridControl = this.gridPThu;
            this.gridViewPThu.Name = "gridViewPThu";
            this.gridViewPThu.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPThu.OptionsView.ShowGroupPanel = false;
            this.gridViewPThu.RowCountChanged += new System.EventHandler(this.gridViewPThu_RowCountChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã phiếu";
            this.gridColumn1.FieldName = "MaPhieu";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 71;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Nhân viên lập";
            this.gridColumn2.FieldName = "MaNhanVien";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 79;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Ngày lập";
            this.gridColumn3.FieldName = "NgayLap";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 93;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Người nộp";
            this.gridColumn4.FieldName = "TenNguoiNop";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 93;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.gridColumn5.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Số tiền";
            this.gridColumn5.FieldName = "SoTien";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 93;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Lý do";
            this.gridColumn6.FieldName = "LyDo";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // groupControl4
            // 
            this.groupControl4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl4.Controls.Add(this.chkTimTheoMPChi);
            this.groupControl4.Controls.Add(this.label6);
            this.groupControl4.Controls.Add(this.label7);
            this.groupControl4.Controls.Add(this.btnTimPChi);
            this.groupControl4.Controls.Add(this.txtMaPChi);
            this.groupControl4.Controls.Add(this.dateToChi);
            this.groupControl4.Controls.Add(this.dateFromChi);
            this.groupControl4.Location = new System.Drawing.Point(737, 47);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(374, 75);
            this.groupControl4.TabIndex = 4;
            this.groupControl4.Text = "Tìm kiếm";
            // 
            // chkTimTheoMPChi
            // 
            this.chkTimTheoMPChi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkTimTheoMPChi.AutoSize = true;
            this.chkTimTheoMPChi.Location = new System.Drawing.Point(194, 27);
            this.chkTimTheoMPChi.Name = "chkTimTheoMPChi";
            this.chkTimTheoMPChi.Size = new System.Drawing.Size(113, 17);
            this.chkTimTheoMPChi.TabIndex = 14;
            this.chkTimTheoMPChi.Text = "Tìm theo Mã phiếu";
            this.chkTimTheoMPChi.UseVisualStyleBackColor = true;
            this.chkTimTheoMPChi.CheckedChanged += new System.EventHandler(this.chkTimTheoMPChi_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Đến";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Từ";
            // 
            // btnTimPChi
            // 
            this.btnTimPChi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimPChi.Image = ((System.Drawing.Image)(resources.GetObject("btnTimPChi.Image")));
            this.btnTimPChi.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnTimPChi.Location = new System.Drawing.Point(321, 24);
            this.btnTimPChi.Name = "btnTimPChi";
            this.btnTimPChi.Size = new System.Drawing.Size(48, 48);
            this.btnTimPChi.TabIndex = 11;
            this.btnTimPChi.Text = "Tìm";
            this.btnTimPChi.Click += new System.EventHandler(this.btnTimPChi_Click);
            // 
            // txtMaPChi
            // 
            this.txtMaPChi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaPChi.Location = new System.Drawing.Point(194, 51);
            this.txtMaPChi.Name = "txtMaPChi";
            this.txtMaPChi.Size = new System.Drawing.Size(117, 20);
            this.txtMaPChi.TabIndex = 9;
            // 
            // dateToChi
            // 
            this.dateToChi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateToChi.EditValue = null;
            this.dateToChi.Location = new System.Drawing.Point(45, 52);
            this.dateToChi.Name = "dateToChi";
            this.dateToChi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateToChi.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateToChi.Size = new System.Drawing.Size(126, 20);
            this.dateToChi.TabIndex = 8;
            // 
            // dateFromChi
            // 
            this.dateFromChi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateFromChi.EditValue = null;
            this.dateFromChi.Location = new System.Drawing.Point(45, 25);
            this.dateFromChi.Name = "dateFromChi";
            this.dateFromChi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFromChi.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFromChi.Size = new System.Drawing.Size(126, 20);
            this.dateFromChi.TabIndex = 7;
            // 
            // groupControl3
            // 
            this.groupControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl3.Controls.Add(this.btnXoaPChi);
            this.groupControl3.Controls.Add(this.btnSuaPChi);
            this.groupControl3.Controls.Add(this.btnThemPChi);
            this.groupControl3.Location = new System.Drawing.Point(564, 47);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(169, 75);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Quản lí";
            // 
            // btnXoaPChi
            // 
            this.btnXoaPChi.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaPChi.Image")));
            this.btnXoaPChi.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnXoaPChi.Location = new System.Drawing.Point(114, 24);
            this.btnXoaPChi.Name = "btnXoaPChi";
            this.btnXoaPChi.Size = new System.Drawing.Size(48, 48);
            this.btnXoaPChi.TabIndex = 5;
            this.btnXoaPChi.Text = "Xóa";
            this.btnXoaPChi.Click += new System.EventHandler(this.btnXoaPChi_Click);
            // 
            // btnSuaPChi
            // 
            this.btnSuaPChi.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaPChi.Image")));
            this.btnSuaPChi.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnSuaPChi.Location = new System.Drawing.Point(60, 24);
            this.btnSuaPChi.Name = "btnSuaPChi";
            this.btnSuaPChi.Size = new System.Drawing.Size(48, 48);
            this.btnSuaPChi.TabIndex = 4;
            this.btnSuaPChi.Text = "Sửa";
            this.btnSuaPChi.Click += new System.EventHandler(this.btnSuaPChi_Click);
            // 
            // btnThemPChi
            // 
            this.btnThemPChi.Image = ((System.Drawing.Image)(resources.GetObject("btnThemPChi.Image")));
            this.btnThemPChi.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnThemPChi.Location = new System.Drawing.Point(6, 24);
            this.btnThemPChi.Name = "btnThemPChi";
            this.btnThemPChi.Size = new System.Drawing.Size(48, 48);
            this.btnThemPChi.TabIndex = 3;
            this.btnThemPChi.Text = "Thêm";
            this.btnThemPChi.Click += new System.EventHandler(this.btnThemPChi_Click);
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(564, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(547, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "PHIẾU CHI";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(548, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "PHIẾU THU";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupControl2
            // 
            this.groupControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl2.Controls.Add(this.chkTimTheoMPThu);
            this.groupControl2.Controls.Add(this.label5);
            this.groupControl2.Controls.Add(this.label4);
            this.groupControl2.Controls.Add(this.btnTimPThu);
            this.groupControl2.Controls.Add(this.txtMaPThu);
            this.groupControl2.Controls.Add(this.dateToThu);
            this.groupControl2.Controls.Add(this.dateFromThu);
            this.groupControl2.Location = new System.Drawing.Point(185, 47);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(375, 75);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Tìm kiếm";
            // 
            // chkTimTheoMPThu
            // 
            this.chkTimTheoMPThu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkTimTheoMPThu.AutoSize = true;
            this.chkTimTheoMPThu.Location = new System.Drawing.Point(188, 27);
            this.chkTimTheoMPThu.Name = "chkTimTheoMPThu";
            this.chkTimTheoMPThu.Size = new System.Drawing.Size(113, 17);
            this.chkTimTheoMPThu.TabIndex = 7;
            this.chkTimTheoMPThu.Text = "Tìm theo Mã phiếu";
            this.chkTimTheoMPThu.UseVisualStyleBackColor = true;
            this.chkTimTheoMPThu.CheckedChanged += new System.EventHandler(this.chkTimTheoMPThu_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Đến";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Từ";
            // 
            // btnTimPThu
            // 
            this.btnTimPThu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimPThu.Image = ((System.Drawing.Image)(resources.GetObject("btnTimPThu.Image")));
            this.btnTimPThu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnTimPThu.Location = new System.Drawing.Point(316, 24);
            this.btnTimPThu.Name = "btnTimPThu";
            this.btnTimPThu.Size = new System.Drawing.Size(48, 48);
            this.btnTimPThu.TabIndex = 4;
            this.btnTimPThu.Text = "Tìm";
            this.btnTimPThu.Click += new System.EventHandler(this.btnTimPThu_Click);
            // 
            // txtMaPThu
            // 
            this.txtMaPThu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaPThu.Location = new System.Drawing.Point(188, 51);
            this.txtMaPThu.Name = "txtMaPThu";
            this.txtMaPThu.Size = new System.Drawing.Size(117, 20);
            this.txtMaPThu.TabIndex = 2;
            // 
            // dateToThu
            // 
            this.dateToThu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateToThu.EditValue = null;
            this.dateToThu.Location = new System.Drawing.Point(39, 52);
            this.dateToThu.Name = "dateToThu";
            this.dateToThu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateToThu.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateToThu.Size = new System.Drawing.Size(126, 20);
            this.dateToThu.TabIndex = 1;
            // 
            // dateFromThu
            // 
            this.dateFromThu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateFromThu.EditValue = null;
            this.dateFromThu.Location = new System.Drawing.Point(39, 25);
            this.dateFromThu.Name = "dateFromThu";
            this.dateFromThu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFromThu.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFromThu.Size = new System.Drawing.Size(126, 20);
            this.dateFromThu.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl1.Controls.Add(this.btnXoaPThu);
            this.groupControl1.Controls.Add(this.btnSuaPThu);
            this.groupControl1.Controls.Add(this.btnThemPThu);
            this.groupControl1.Location = new System.Drawing.Point(12, 47);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(169, 75);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Quản lí";
            // 
            // btnXoaPThu
            // 
            this.btnXoaPThu.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaPThu.Image")));
            this.btnXoaPThu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnXoaPThu.Location = new System.Drawing.Point(114, 24);
            this.btnXoaPThu.Name = "btnXoaPThu";
            this.btnXoaPThu.Size = new System.Drawing.Size(48, 48);
            this.btnXoaPThu.TabIndex = 2;
            this.btnXoaPThu.Text = "Xóa";
            this.btnXoaPThu.Click += new System.EventHandler(this.btnXoaPThu_Click);
            // 
            // btnSuaPThu
            // 
            this.btnSuaPThu.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaPThu.Image")));
            this.btnSuaPThu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnSuaPThu.Location = new System.Drawing.Point(60, 24);
            this.btnSuaPThu.Name = "btnSuaPThu";
            this.btnSuaPThu.Size = new System.Drawing.Size(48, 48);
            this.btnSuaPThu.TabIndex = 1;
            this.btnSuaPThu.Text = "Sửa";
            this.btnSuaPThu.Click += new System.EventHandler(this.btnSuaPThu_Click);
            // 
            // btnThemPThu
            // 
            this.btnThemPThu.Image = ((System.Drawing.Image)(resources.GetObject("btnThemPThu.Image")));
            this.btnThemPThu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnThemPThu.Location = new System.Drawing.Point(6, 24);
            this.btnThemPThu.Name = "btnThemPThu";
            this.btnThemPThu.Size = new System.Drawing.Size(48, 48);
            this.btnThemPThu.TabIndex = 0;
            this.btnThemPThu.Text = "Thêm";
            this.btnThemPThu.Click += new System.EventHandler(this.btnThemPThu_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.labStatePT,
            this.labStatePC,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1123, 557);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(173, 79);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(173, 79);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(173, 79);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupControl2;
            this.layoutControlItem2.Location = new System.Drawing.Point(173, 35);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 79);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(5, 79);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(379, 79);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(552, 35);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.label2;
            this.layoutControlItem4.Location = new System.Drawing.Point(552, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(551, 35);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupControl3;
            this.layoutControlItem5.Location = new System.Drawing.Point(552, 35);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(173, 79);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(173, 79);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(173, 79);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.groupControl4;
            this.layoutControlItem6.Location = new System.Drawing.Point(725, 35);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 79);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(5, 79);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(378, 79);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridPThu;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(552, 397);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridPChi;
            this.layoutControlItem8.Location = new System.Drawing.Point(552, 114);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(551, 397);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // labStatePT
            // 
            this.labStatePT.AllowHotTrack = false;
            this.labStatePT.Location = new System.Drawing.Point(0, 511);
            this.labStatePT.Name = "labStatePT";
            this.labStatePT.Size = new System.Drawing.Size(445, 26);
            this.labStatePT.TextSize = new System.Drawing.Size(78, 13);
            // 
            // labStatePC
            // 
            this.labStatePC.AllowHotTrack = false;
            this.labStatePC.Location = new System.Drawing.Point(552, 511);
            this.labStatePC.Name = "labStatePC";
            this.labStatePC.Size = new System.Drawing.Size(441, 26);
            this.labStatePC.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnShowPT;
            this.layoutControlItem9.Location = new System.Drawing.Point(445, 511);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(107, 26);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnShowPC;
            this.layoutControlItem10.Location = new System.Drawing.Point(993, 511);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(110, 26);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // frmQuanLyThuChi
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 557);
            this.Controls.Add(this.layoutControl1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Name = "frmQuanLyThuChi";
            this.Text = "Tình Trạng Thu Chi";
            this.Load += new System.EventHandler(this.frmQuanLyThuChi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPThu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPThu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToChi.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromChi.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaPThu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToThu.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToThu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromThu.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFromThu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatePT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatePC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnThemPThu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton btnXoaPThu;
        private DevExpress.XtraEditors.SimpleButton btnSuaPThu;
        private DevExpress.XtraEditors.SimpleButton btnXoaPChi;
        private DevExpress.XtraEditors.SimpleButton btnSuaPChi;
        private DevExpress.XtraEditors.SimpleButton btnThemPChi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.SimpleButton btnTimPChi;
        private DevExpress.XtraEditors.TextEdit txtMaPChi;
        private DevExpress.XtraEditors.DateEdit dateToChi;
        private DevExpress.XtraEditors.DateEdit dateFromChi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SimpleButton btnTimPThu;
        private DevExpress.XtraEditors.TextEdit txtMaPThu;
        private DevExpress.XtraEditors.DateEdit dateToThu;
        private DevExpress.XtraEditors.DateEdit dateFromThu;
        private DevExpress.XtraGrid.GridControl gridPChi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPChi;
        private DevExpress.XtraGrid.GridControl gridPThu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPThu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.CheckBox chkTimTheoMPChi;
        private System.Windows.Forms.CheckBox chkTimTheoMPThu;
        private DevExpress.XtraEditors.SimpleButton btnShowPC;
        private DevExpress.XtraEditors.SimpleButton btnShowPT;
        private DevExpress.XtraLayout.SimpleLabelItem labStatePT;
        private DevExpress.XtraLayout.SimpleLabelItem labStatePC;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}