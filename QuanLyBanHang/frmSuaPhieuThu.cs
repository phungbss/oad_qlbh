﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmSuaPhieuThu : DevExpress.XtraEditors.XtraForm
    {
        public BusinessLogic.PhieuThu pt_bus;
        public DataTransfer.PhieuThu pt_dt;
        public frmSuaPhieuThu(BusinessLogic.PhieuThu pt = null, DataTransfer.PhieuThu pt_new = null)
        {
            pt_bus = pt;
            pt_dt = pt_new;
            if (pt_bus == null)
                pt_bus = new BusinessLogic.PhieuThu();
            InitializeComponent();
        }
        public void NapGiaoDien()
        {
            txtMaPhieu.Text = pt_dt.MaPhieu;
            dateNgayLap.EditValue = pt_dt.NgayLap;
            txtLyDo.Text = pt_dt.LyDo;
            txtSoTien.Text = pt_dt.SoTien.ToString();
            txtNguoiNop.Text = pt_dt.TenNguoiNop;
        }

        private void txtSoTien_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsDigit(e.KeyChar)&&!Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void frmThemPhieuThu_Load(object sender, EventArgs e)
        {
           
            NapGiaoDien();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(txtNguoiNop.Text != string.Empty && txtSoTien.Text != string.Empty)
            {
                try
                {
                    DataTransfer.PhieuThu pt = new DataTransfer.PhieuThu(txtMaPhieu.Text,
                        "ND000002", dateNgayLap.DateTime, txtNguoiNop.Text,
                        double.Parse(txtSoTien.Text), txtLyDo.Text);
                    if(pt_bus.Sua(pt))
                    {
                        XtraMessageBox.Show("Phiếu thu " + txtMaPhieu.Text + " đã được cập nhật", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    this.Close();
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập đầy đủ các thông tin", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}