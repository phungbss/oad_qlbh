﻿namespace QuanLyBanHang
{
    partial class frmHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHomePage));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblHuongDanSuDung = new System.Windows.Forms.LinkLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnNhaCungCap = new System.Windows.Forms.Button();
            this.btnKhachHang = new System.Windows.Forms.Button();
            this.btnLapPhieuChi = new System.Windows.Forms.Button();
            this.btnLapPhieuThu = new System.Windows.Forms.Button();
            this.btnThuChi = new System.Windows.Forms.Button();
            this.btnXuatKho = new System.Windows.Forms.Button();
            this.btnNhapKho = new System.Windows.Forms.Button();
            this.btnBaoGia = new System.Windows.Forms.Button();
            this.btnLapBaoGia = new System.Windows.Forms.Button();
            this.btnKhoHang = new System.Windows.Forms.Button();
            this.btnHoaDonDatHang = new System.Windows.Forms.Button();
            this.btnHoaDonBanHang = new System.Windows.Forms.Button();
            this.btnLapHoaDonDatHang = new System.Windows.Forms.Button();
            this.btnLapHoaDonBanHang = new System.Windows.Forms.Button();
            this.btnLoaiSanPham = new System.Windows.Forms.Button();
            this.btnSanPham = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.lblHoaDonBanHang = new DevExpress.XtraNavBar.NavBarItem();
            this.lblHoaDonDatHang = new DevExpress.XtraNavBar.NavBarItem();
            this.lblKhoHang = new DevExpress.XtraNavBar.NavBarItem();
            this.lblThuChi = new DevExpress.XtraNavBar.NavBarItem();
            this.lblBaoGia = new DevExpress.XtraNavBar.NavBarItem();
            this.lblKhachHang = new DevExpress.XtraNavBar.NavBarItem();
            this.lblNhaCungCap = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.lblLapHoaDonBanHang = new DevExpress.XtraNavBar.NavBarItem();
            this.lblLapHoaDonDatHang = new DevExpress.XtraNavBar.NavBarItem();
            this.lblLapPhieuNhapKho = new DevExpress.XtraNavBar.NavBarItem();
            this.lblLapPhieuXuatKho = new DevExpress.XtraNavBar.NavBarItem();
            this.lblLapPhieuThu = new DevExpress.XtraNavBar.NavBarItem();
            this.lblLapPhieuChi = new DevExpress.XtraNavBar.NavBarItem();
            this.lblLapBaoGia = new DevExpress.XtraNavBar.NavBarItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panel3);
            this.layoutControl1.Controls.Add(this.panel2);
            this.layoutControl1.Controls.Add(this.panel1);
            this.layoutControl1.Controls.Add(this.navBarControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(493, 149, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1119, 527);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblHuongDanSuDung);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(232, 467);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(875, 48);
            this.panel3.TabIndex = 7;
            // 
            // lblHuongDanSuDung
            // 
            this.lblHuongDanSuDung.AutoSize = true;
            this.lblHuongDanSuDung.Location = new System.Drawing.Point(106, 26);
            this.lblHuongDanSuDung.Name = "lblHuongDanSuDung";
            this.lblHuongDanSuDung.Size = new System.Drawing.Size(174, 15);
            this.lblHuongDanSuDung.TabIndex = 6;
            this.lblHuongDanSuDung.TabStop = true;
            this.lblHuongDanSuDung.Text = "Hướng dẫn sử dụng phần mềm";
            this.lblHuongDanSuDung.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblHuongDanSuDung_LinkClicked);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(279, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "của chúng tôi.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Xem qua";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(91)))), ((int)(((byte)(136)))));
            this.label4.Location = new System.Drawing.Point(52, -2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Bạn cần trợ giúp?";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btnNhaCungCap);
            this.panel2.Controls.Add(this.btnKhachHang);
            this.panel2.Controls.Add(this.btnLapPhieuChi);
            this.panel2.Controls.Add(this.btnLapPhieuThu);
            this.panel2.Controls.Add(this.btnThuChi);
            this.panel2.Controls.Add(this.btnXuatKho);
            this.panel2.Controls.Add(this.btnNhapKho);
            this.panel2.Controls.Add(this.btnBaoGia);
            this.panel2.Controls.Add(this.btnLapBaoGia);
            this.panel2.Controls.Add(this.btnKhoHang);
            this.panel2.Controls.Add(this.btnHoaDonDatHang);
            this.panel2.Controls.Add(this.btnHoaDonBanHang);
            this.panel2.Controls.Add(this.btnLapHoaDonDatHang);
            this.panel2.Controls.Add(this.btnLapHoaDonBanHang);
            this.panel2.Controls.Add(this.btnLoaiSanPham);
            this.panel2.Controls.Add(this.btnSanPham);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(232, 78);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(875, 385);
            this.panel2.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Chọn một tác vụ để bắt đầu.";
            // 
            // btnNhaCungCap
            // 
            this.btnNhaCungCap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnNhaCungCap.FlatAppearance.BorderSize = 0;
            this.btnNhaCungCap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNhaCungCap.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnNhaCungCap.ForeColor = System.Drawing.Color.White;
            this.btnNhaCungCap.Location = new System.Drawing.Point(465, 305);
            this.btnNhaCungCap.Name = "btnNhaCungCap";
            this.btnNhaCungCap.Size = new System.Drawing.Size(124, 40);
            this.btnNhaCungCap.TabIndex = 19;
            this.btnNhaCungCap.Text = "NHÀ CUNG CẤP";
            this.btnNhaCungCap.UseVisualStyleBackColor = false;
            this.btnNhaCungCap.Click += new System.EventHandler(this.btnNhaCungCap_Click);
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnKhachHang.FlatAppearance.BorderSize = 0;
            this.btnKhachHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKhachHang.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnKhachHang.ForeColor = System.Drawing.Color.White;
            this.btnKhachHang.Location = new System.Drawing.Point(321, 305);
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.Size = new System.Drawing.Size(124, 40);
            this.btnKhachHang.TabIndex = 18;
            this.btnKhachHang.Text = "KHÁCH HÀNG";
            this.btnKhachHang.UseVisualStyleBackColor = false;
            this.btnKhachHang.Click += new System.EventHandler(this.btnKhachHang_Click);
            // 
            // btnLapPhieuChi
            // 
            this.btnLapPhieuChi.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnLapPhieuChi.FlatAppearance.BorderSize = 0;
            this.btnLapPhieuChi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapPhieuChi.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLapPhieuChi.ForeColor = System.Drawing.Color.White;
            this.btnLapPhieuChi.Location = new System.Drawing.Point(577, 257);
            this.btnLapPhieuChi.Name = "btnLapPhieuChi";
            this.btnLapPhieuChi.Size = new System.Drawing.Size(155, 40);
            this.btnLapPhieuChi.TabIndex = 17;
            this.btnLapPhieuChi.Text = "LẬP PHIẾU CHI";
            this.btnLapPhieuChi.UseVisualStyleBackColor = false;
            this.btnLapPhieuChi.Click += new System.EventHandler(this.btnLapPhieuChi_Click);
            // 
            // btnLapPhieuThu
            // 
            this.btnLapPhieuThu.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnLapPhieuThu.FlatAppearance.BorderSize = 0;
            this.btnLapPhieuThu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapPhieuThu.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLapPhieuThu.ForeColor = System.Drawing.Color.White;
            this.btnLapPhieuThu.Location = new System.Drawing.Point(187, 257);
            this.btnLapPhieuThu.Name = "btnLapPhieuThu";
            this.btnLapPhieuThu.Size = new System.Drawing.Size(155, 40);
            this.btnLapPhieuThu.TabIndex = 16;
            this.btnLapPhieuThu.Text = "LẬP PHIẾU THU";
            this.btnLapPhieuThu.UseVisualStyleBackColor = false;
            this.btnLapPhieuThu.Click += new System.EventHandler(this.btnLapPhieuThu_Click);
            // 
            // btnThuChi
            // 
            this.btnThuChi.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnThuChi.FlatAppearance.BorderSize = 0;
            this.btnThuChi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThuChi.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnThuChi.ForeColor = System.Drawing.Color.White;
            this.btnThuChi.Location = new System.Drawing.Point(398, 257);
            this.btnThuChi.Name = "btnThuChi";
            this.btnThuChi.Size = new System.Drawing.Size(124, 40);
            this.btnThuChi.TabIndex = 15;
            this.btnThuChi.Text = "THU CHI";
            this.btnThuChi.UseVisualStyleBackColor = false;
            this.btnThuChi.Click += new System.EventHandler(this.btnThuChi_Click);
            // 
            // btnXuatKho
            // 
            this.btnXuatKho.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnXuatKho.FlatAppearance.BorderSize = 0;
            this.btnXuatKho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXuatKho.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnXuatKho.ForeColor = System.Drawing.Color.White;
            this.btnXuatKho.Location = new System.Drawing.Point(594, 184);
            this.btnXuatKho.Name = "btnXuatKho";
            this.btnXuatKho.Size = new System.Drawing.Size(111, 40);
            this.btnXuatKho.TabIndex = 14;
            this.btnXuatKho.Text = "XUẤT KHO";
            this.btnXuatKho.UseVisualStyleBackColor = false;
            this.btnXuatKho.Click += new System.EventHandler(this.btnXuatKho_Click);
            // 
            // btnNhapKho
            // 
            this.btnNhapKho.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnNhapKho.FlatAppearance.BorderSize = 0;
            this.btnNhapKho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNhapKho.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnNhapKho.ForeColor = System.Drawing.Color.White;
            this.btnNhapKho.Location = new System.Drawing.Point(210, 184);
            this.btnNhapKho.Name = "btnNhapKho";
            this.btnNhapKho.Size = new System.Drawing.Size(111, 40);
            this.btnNhapKho.TabIndex = 13;
            this.btnNhapKho.Text = "NHẬP KHO";
            this.btnNhapKho.UseVisualStyleBackColor = false;
            this.btnNhapKho.Click += new System.EventHandler(this.btnNhapKho_Click);
            // 
            // btnBaoGia
            // 
            this.btnBaoGia.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnBaoGia.FlatAppearance.BorderSize = 0;
            this.btnBaoGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBaoGia.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnBaoGia.ForeColor = System.Drawing.Color.White;
            this.btnBaoGia.Location = new System.Drawing.Point(414, 208);
            this.btnBaoGia.Name = "btnBaoGia";
            this.btnBaoGia.Size = new System.Drawing.Size(88, 40);
            this.btnBaoGia.TabIndex = 12;
            this.btnBaoGia.Text = "BÁO GIÁ";
            this.btnBaoGia.UseVisualStyleBackColor = false;
            this.btnBaoGia.Click += new System.EventHandler(this.btnBaoGia_Click);
            // 
            // btnLapBaoGia
            // 
            this.btnLapBaoGia.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnLapBaoGia.FlatAppearance.BorderSize = 0;
            this.btnLapBaoGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapBaoGia.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLapBaoGia.ForeColor = System.Drawing.Color.White;
            this.btnLapBaoGia.Location = new System.Drawing.Point(401, 156);
            this.btnLapBaoGia.Name = "btnLapBaoGia";
            this.btnLapBaoGia.Size = new System.Drawing.Size(115, 40);
            this.btnLapBaoGia.TabIndex = 11;
            this.btnLapBaoGia.Text = "LẬP BÁO GIÁ";
            this.btnLapBaoGia.UseVisualStyleBackColor = false;
            this.btnLapBaoGia.Click += new System.EventHandler(this.btnLapBaoGia_Click);
            // 
            // btnKhoHang
            // 
            this.btnKhoHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnKhoHang.FlatAppearance.BorderSize = 0;
            this.btnKhoHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKhoHang.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.btnKhoHang.ForeColor = System.Drawing.Color.White;
            this.btnKhoHang.Location = new System.Drawing.Point(405, 103);
            this.btnKhoHang.Name = "btnKhoHang";
            this.btnKhoHang.Size = new System.Drawing.Size(106, 40);
            this.btnKhoHang.TabIndex = 10;
            this.btnKhoHang.Text = "KHO HÀNG";
            this.btnKhoHang.UseVisualStyleBackColor = false;
            this.btnKhoHang.Click += new System.EventHandler(this.btnKhoHang_Click);
            // 
            // btnHoaDonDatHang
            // 
            this.btnHoaDonDatHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(64)))), ((int)(((byte)(159)))));
            this.btnHoaDonDatHang.FlatAppearance.BorderSize = 0;
            this.btnHoaDonDatHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHoaDonDatHang.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnHoaDonDatHang.ForeColor = System.Drawing.Color.White;
            this.btnHoaDonDatHang.Location = new System.Drawing.Point(653, 133);
            this.btnHoaDonDatHang.Name = "btnHoaDonDatHang";
            this.btnHoaDonDatHang.Size = new System.Drawing.Size(156, 40);
            this.btnHoaDonDatHang.TabIndex = 9;
            this.btnHoaDonDatHang.Text = "HÓA ĐƠN ĐẶT HÀNG";
            this.btnHoaDonDatHang.UseVisualStyleBackColor = false;
            this.btnHoaDonDatHang.Click += new System.EventHandler(this.btnHoaDonDatHang_Click);
            // 
            // btnHoaDonBanHang
            // 
            this.btnHoaDonBanHang.BackColor = System.Drawing.Color.Teal;
            this.btnHoaDonBanHang.FlatAppearance.BorderSize = 0;
            this.btnHoaDonBanHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHoaDonBanHang.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnHoaDonBanHang.ForeColor = System.Drawing.Color.White;
            this.btnHoaDonBanHang.Location = new System.Drawing.Point(109, 133);
            this.btnHoaDonBanHang.Name = "btnHoaDonBanHang";
            this.btnHoaDonBanHang.Size = new System.Drawing.Size(156, 40);
            this.btnHoaDonBanHang.TabIndex = 8;
            this.btnHoaDonBanHang.Text = "HÓA ĐƠN BÁN HÀNG";
            this.btnHoaDonBanHang.UseVisualStyleBackColor = false;
            this.btnHoaDonBanHang.Click += new System.EventHandler(this.btnHoaDonBanHang_Click);
            // 
            // btnLapHoaDonDatHang
            // 
            this.btnLapHoaDonDatHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(64)))), ((int)(((byte)(159)))));
            this.btnLapHoaDonDatHang.FlatAppearance.BorderSize = 0;
            this.btnLapHoaDonDatHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapHoaDonDatHang.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLapHoaDonDatHang.ForeColor = System.Drawing.Color.White;
            this.btnLapHoaDonDatHang.Location = new System.Drawing.Point(594, 68);
            this.btnLapHoaDonDatHang.Name = "btnLapHoaDonDatHang";
            this.btnLapHoaDonDatHang.Size = new System.Drawing.Size(164, 40);
            this.btnLapHoaDonDatHang.TabIndex = 7;
            this.btnLapHoaDonDatHang.Text = "LẬP HÓA ĐƠN ĐẶT HÀNG";
            this.btnLapHoaDonDatHang.UseVisualStyleBackColor = false;
            this.btnLapHoaDonDatHang.Click += new System.EventHandler(this.btnLapHoaDonDatHang_Click);
            // 
            // btnLapHoaDonBanHang
            // 
            this.btnLapHoaDonBanHang.BackColor = System.Drawing.Color.Teal;
            this.btnLapHoaDonBanHang.FlatAppearance.BorderSize = 0;
            this.btnLapHoaDonBanHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapHoaDonBanHang.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLapHoaDonBanHang.ForeColor = System.Drawing.Color.White;
            this.btnLapHoaDonBanHang.Location = new System.Drawing.Point(160, 68);
            this.btnLapHoaDonBanHang.Name = "btnLapHoaDonBanHang";
            this.btnLapHoaDonBanHang.Size = new System.Drawing.Size(164, 40);
            this.btnLapHoaDonBanHang.TabIndex = 6;
            this.btnLapHoaDonBanHang.Text = "LẬP HÓA ĐƠN BÁN HÀNG";
            this.btnLapHoaDonBanHang.UseVisualStyleBackColor = false;
            this.btnLapHoaDonBanHang.Click += new System.EventHandler(this.btnLapHoaDonBanHang_Click);
            // 
            // btnLoaiSanPham
            // 
            this.btnLoaiSanPham.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(176)))), ((int)(((byte)(68)))));
            this.btnLoaiSanPham.FlatAppearance.BorderSize = 0;
            this.btnLoaiSanPham.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoaiSanPham.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnLoaiSanPham.ForeColor = System.Drawing.Color.White;
            this.btnLoaiSanPham.Location = new System.Drawing.Point(462, 36);
            this.btnLoaiSanPham.Name = "btnLoaiSanPham";
            this.btnLoaiSanPham.Size = new System.Drawing.Size(127, 40);
            this.btnLoaiSanPham.TabIndex = 5;
            this.btnLoaiSanPham.Text = "LOẠI SẢN PHẨM";
            this.btnLoaiSanPham.UseVisualStyleBackColor = false;
            this.btnLoaiSanPham.Click += new System.EventHandler(this.btnLoaiSanPham_Click);
            // 
            // btnSanPham
            // 
            this.btnSanPham.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(176)))), ((int)(((byte)(68)))));
            this.btnSanPham.FlatAppearance.BorderSize = 0;
            this.btnSanPham.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSanPham.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnSanPham.ForeColor = System.Drawing.Color.White;
            this.btnSanPham.Location = new System.Drawing.Point(330, 36);
            this.btnSanPham.Name = "btnSanPham";
            this.btnSanPham.Size = new System.Drawing.Size(122, 40);
            this.btnSanPham.TabIndex = 4;
            this.btnSanPham.Text = "SẢN PHẨM";
            this.btnSanPham.UseVisualStyleBackColor = false;
            this.btnSanPham.Click += new System.EventHandler(this.btnSanPham_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::QuanLyBanHang.Properties.Resources.nghiep_vu;
            this.pictureBox1.Location = new System.Drawing.Point(109, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(700, 317);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(91)))), ((int)(((byte)(136)))));
            this.label3.Location = new System.Drawing.Point(52, -3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(276, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Hãy xem qua quy trình nghiệp vụ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblWelcome);
            this.panel1.Location = new System.Drawing.Point(232, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(875, 62);
            this.panel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(328, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bắt đầu một ngày làm việc với phần mềm Quản lý cửa hàng tạp hóa của nhóm 17 - OOAD.";
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.lblWelcome.ForeColor = System.Drawing.Color.Green;
            this.lblWelcome.Location = new System.Drawing.Point(52, 13);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(177, 30);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Xin chào, <user>";
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.navBarGroup2});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.lblHoaDonBanHang,
            this.lblHoaDonDatHang,
            this.lblKhoHang,
            this.lblThuChi,
            this.lblBaoGia,
            this.lblKhachHang,
            this.lblNhaCungCap,
            this.lblLapHoaDonBanHang,
            this.lblLapHoaDonDatHang,
            this.lblLapPhieuNhapKho,
            this.lblLapPhieuXuatKho,
            this.lblLapPhieuThu,
            this.lblLapPhieuChi,
            this.lblLapBaoGia});
            this.navBarControl1.Location = new System.Drawing.Point(12, 12);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 214;
            this.navBarControl1.Size = new System.Drawing.Size(214, 503);
            this.navBarControl1.TabIndex = 4;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Quản lý";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblHoaDonBanHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblHoaDonDatHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblKhoHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblThuChi),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblBaoGia),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblKhachHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblNhaCungCap)});
            this.navBarGroup1.Name = "navBarGroup1";
            this.navBarGroup1.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarGroup1.SmallImage")));
            // 
            // lblHoaDonBanHang
            // 
            this.lblHoaDonBanHang.Caption = "Hóa đơn bán hàng";
            this.lblHoaDonBanHang.Name = "lblHoaDonBanHang";
            this.lblHoaDonBanHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblHoaDonBanHang_LinkClicked);
            // 
            // lblHoaDonDatHang
            // 
            this.lblHoaDonDatHang.Caption = "Hóa đơn đặt hàng";
            this.lblHoaDonDatHang.Name = "lblHoaDonDatHang";
            this.lblHoaDonDatHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblHoaDonDatHang_LinkClicked);
            // 
            // lblKhoHang
            // 
            this.lblKhoHang.Caption = "Kho hàng";
            this.lblKhoHang.Name = "lblKhoHang";
            this.lblKhoHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblKhoHang_LinkClicked);
            // 
            // lblThuChi
            // 
            this.lblThuChi.Caption = "Thu chi";
            this.lblThuChi.Name = "lblThuChi";
            this.lblThuChi.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblThuChi_LinkClicked);
            // 
            // lblBaoGia
            // 
            this.lblBaoGia.Caption = "Báo giá";
            this.lblBaoGia.Name = "lblBaoGia";
            this.lblBaoGia.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblBaoGia_LinkClicked);
            // 
            // lblKhachHang
            // 
            this.lblKhachHang.Caption = "Khách hàng";
            this.lblKhachHang.Name = "lblKhachHang";
            this.lblKhachHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblKhachHang_LinkClicked);
            // 
            // lblNhaCungCap
            // 
            this.lblNhaCungCap.Caption = "Nhà cung cấp";
            this.lblNhaCungCap.Name = "lblNhaCungCap";
            this.lblNhaCungCap.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblNhaCungCap_LinkClicked);
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Tác vụ";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapHoaDonBanHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapHoaDonDatHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapPhieuNhapKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapPhieuXuatKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapPhieuThu),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapPhieuChi),
            new DevExpress.XtraNavBar.NavBarItemLink(this.lblLapBaoGia)});
            this.navBarGroup2.Name = "navBarGroup2";
            this.navBarGroup2.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarGroup2.SmallImage")));
            // 
            // lblLapHoaDonBanHang
            // 
            this.lblLapHoaDonBanHang.Caption = "Lập Hóa đơn bán hàng";
            this.lblLapHoaDonBanHang.Name = "lblLapHoaDonBanHang";
            this.lblLapHoaDonBanHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapHoaDonBanHang_LinkClicked);
            // 
            // lblLapHoaDonDatHang
            // 
            this.lblLapHoaDonDatHang.Caption = "Lập Hóa đơn đặt hàng";
            this.lblLapHoaDonDatHang.Name = "lblLapHoaDonDatHang";
            this.lblLapHoaDonDatHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapHoaDonDatHang_LinkClicked);
            // 
            // lblLapPhieuNhapKho
            // 
            this.lblLapPhieuNhapKho.Caption = "Lập Phiếu nhập kho";
            this.lblLapPhieuNhapKho.Name = "lblLapPhieuNhapKho";
            this.lblLapPhieuNhapKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapPhieuNhapKho_LinkClicked);
            // 
            // lblLapPhieuXuatKho
            // 
            this.lblLapPhieuXuatKho.Caption = "Lập Phiếu xuất kho";
            this.lblLapPhieuXuatKho.Name = "lblLapPhieuXuatKho";
            this.lblLapPhieuXuatKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapPhieuXuatKho_LinkClicked);
            // 
            // lblLapPhieuThu
            // 
            this.lblLapPhieuThu.Caption = "Lập Phiếu thu";
            this.lblLapPhieuThu.Name = "lblLapPhieuThu";
            this.lblLapPhieuThu.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapPhieuThu_LinkClicked);
            // 
            // lblLapPhieuChi
            // 
            this.lblLapPhieuChi.Caption = "Lập Phiếu chi";
            this.lblLapPhieuChi.Name = "lblLapPhieuChi";
            this.lblLapPhieuChi.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapPhieuChi_LinkClicked);
            // 
            // lblLapBaoGia
            // 
            this.lblLapBaoGia.Caption = "Lập Báo giá";
            this.lblLapBaoGia.Name = "lblLapBaoGia";
            this.lblLapBaoGia.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.lblLapBaoGia_LinkClicked);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.simpleSeparator1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1119, 527);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.navBarControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(218, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(218, 14);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(218, 507);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panel1;
            this.layoutControlItem2.Location = new System.Drawing.Point(220, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 66);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(104, 66);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(879, 66);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panel2;
            this.layoutControlItem3.Location = new System.Drawing.Point(220, 66);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(879, 389);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.panel3;
            this.layoutControlItem4.Location = new System.Drawing.Point(220, 455);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 52);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(104, 52);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(879, 52);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(218, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(2, 507);
            // 
            // frmHomePage
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 527);
            this.Controls.Add(this.layoutControl1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Name = "frmHomePage";
            this.Text = "Trang đầu";
            this.Load += new System.EventHandler(this.frmHomePage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem lblHoaDonBanHang;
        private DevExpress.XtraNavBar.NavBarItem lblHoaDonDatHang;
        private DevExpress.XtraNavBar.NavBarItem lblKhoHang;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraNavBar.NavBarItem lblThuChi;
        private DevExpress.XtraNavBar.NavBarItem lblBaoGia;
        private DevExpress.XtraNavBar.NavBarItem lblKhachHang;
        private DevExpress.XtraNavBar.NavBarItem lblNhaCungCap;
        private DevExpress.XtraNavBar.NavBarItem lblLapHoaDonBanHang;
        private DevExpress.XtraNavBar.NavBarItem lblLapHoaDonDatHang;
        private DevExpress.XtraNavBar.NavBarItem lblLapPhieuNhapKho;
        private DevExpress.XtraNavBar.NavBarItem lblLapPhieuXuatKho;
        private DevExpress.XtraNavBar.NavBarItem lblLapPhieuThu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblWelcome;
        private DevExpress.XtraNavBar.NavBarItem lblLapPhieuChi;
        private DevExpress.XtraNavBar.NavBarItem lblLapBaoGia;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private System.Windows.Forms.LinkLabel lblHuongDanSuDung;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSanPham;
        private System.Windows.Forms.Button btnNhaCungCap;
        private System.Windows.Forms.Button btnKhachHang;
        private System.Windows.Forms.Button btnLapPhieuChi;
        private System.Windows.Forms.Button btnLapPhieuThu;
        private System.Windows.Forms.Button btnThuChi;
        private System.Windows.Forms.Button btnXuatKho;
        private System.Windows.Forms.Button btnNhapKho;
        private System.Windows.Forms.Button btnBaoGia;
        private System.Windows.Forms.Button btnLapBaoGia;
        private System.Windows.Forms.Button btnKhoHang;
        private System.Windows.Forms.Button btnHoaDonDatHang;
        private System.Windows.Forms.Button btnHoaDonBanHang;
        private System.Windows.Forms.Button btnLapHoaDonDatHang;
        private System.Windows.Forms.Button btnLapHoaDonBanHang;
        private System.Windows.Forms.Button btnLoaiSanPham;
    }
}