﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using QuanLyBanHang.Properties;
using System.Diagnostics;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            frmDangNhap dangNhap = new frmDangNhap();
            dangNhap.ShowDialog();

            currentUser = dangNhap.NguoiDung;

            InitializeComponent();
            InitializeDefaultFonts();

            Settings.Default.User_CurrentUser = currentUser != null ? currentUser.MaNguoiDung : null;
        }

        private DataTransfer.NguoiDung currentUser;

        /// <summary>
        /// Thiết lập font chữ mặc định cho ứng dụng
        /// </summary>
        public void InitializeDefaultFonts()
        {
            Font f = new Font("Segoe UI", 9, FontStyle.Regular);
            DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = f;
            DevExpress.XtraEditors.WindowsFormsSettings.DefaultMenuFont = f;
            DevExpress.XtraEditors.WindowsFormsSettings.DefaultPrintFont = f;
        }

        public void NapStatusBar()
        {
            lblStoreName.Caption = Settings.Default.StoreInfo_Name;
        }

        /// <summary>
        /// Kiểm tra một kiểu form đã tồn tại hay chưa
        /// </summary>
        /// <param name="type">Kiểu form</param>
        /// <returns>Form thuộc kiểu đang được kích hoạt</returns>
        public Form IsActive(Type type)
        {
            foreach (var f in this.MdiChildren)
            {
                if (f.GetType() == type)
                    return f;
            }
            return null;
        }

        public void NapGiaoDienPhanQuyen(DataTransfer.NguoiDung nd = null)
        {
            //quyền Khách
            if (nd == null)
            {
                btnLapHDBH.Enabled = false;
                btnHDBH.Enabled = false;
                btnLapHDDH.Enabled = false;
                btnHDDH.Enabled = false;
                btnNhapKho.Enabled = false;
                btnXuatKho.Enabled = false;
                btnKhoHang.Enabled = false;
                btnLapPhieuThu.Enabled = false;
                btnLapPhieuChi.Enabled = false;
                btnTinhTrangThuChi.Enabled = false;
                btnLapBaoGia.Enabled = false;
                btnBaoGia.Enabled = false;
                btnKhachHang.Enabled = false;
                btnNhaCungCap.Enabled = false;
                btnCaiDatNguoiDung.Enabled = false;
                btnQuanLyNguoiDung.Enabled = false;
                btnSaoLuu.Enabled = false;
                btnPhucHoi.Enabled = false;
                btnThongTinCuaHang.Enabled = false;
                lblStoreName.Enabled = false;

                lblCurrentUser.Caption = "Khách";
            }
            else if (nd.MaDacQuyen == "DQ002") //quyền staff
            {
                btnHDBH.Enabled = false;
                btnHDDH.Enabled = false;
                btnTinhTrangThuChi.Enabled = false;
                btnBaoGia.Enabled = false;
                btnQuanLyNguoiDung.Enabled = false;
                btnSaoLuu.Enabled = false;
                btnPhucHoi.Enabled = false;
                btnThongTinCuaHang.Enabled = false;
                lblStoreName.Enabled = false;
            }

            if (nd != null && (nd.MaDacQuyen == "DQ001" || nd.MaDacQuyen == "DQ002"))
            {
                lblCurrentUser.Caption = nd.HoTen;

                //nạp Homepage
                Form homePage = IsActive(typeof(frmHomePage));
                if (homePage != null)
                    homePage.Activate();
                else
                {
                    homePage = new frmHomePage(currentUser);
                    homePage.MdiParent = this;
                    homePage.Show();
                }
            }
        }

        private void btnLapHDBH_ItemClick(object sender, ItemClickEventArgs e)
        {
            //kiểm tra form đã có sẵn chưa
            Form lapHDBH = IsActive(typeof(frmLapHoaDonBanHang));

            if (lapHDBH != null)
                lapHDBH.Activate();
            else
            {
                lapHDBH = new frmLapHoaDonBanHang();
                lapHDBH.MdiParent = this;
                lapHDBH.Show();
            }
        }

        private void btnSanPham_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form SanPham = IsActive(typeof(frmSanPham));

            if (SanPham != null)
                SanPham.Activate();
            else
            {
                SanPham = new frmSanPham(currentUser);
                SanPham.MdiParent = this;
                SanPham.Show();
            }
        }

        private void btnLoaiSanPham_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form LoaiSanPham = IsActive(typeof(frmLoaiSanPham));

            if (LoaiSanPham != null)
                LoaiSanPham.Activate();
            else
            {
                LoaiSanPham = new frmLoaiSanPham(currentUser);
                LoaiSanPham.MdiParent = this;
                LoaiSanPham.Show();
            }
        }

        private void btnSaoLuu_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form SaoLuu = IsActive(typeof(frmSaoLuu));

            if (SaoLuu != null)
                SaoLuu.Activate();
            else
            {
                SaoLuu = new frmSaoLuu();
                SaoLuu.MdiParent = this;
                SaoLuu.Show();
            }
        }

        private void btnPhucHoi_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form PhucHoi = IsActive(typeof(frmPhucHoi));

            if (PhucHoi != null)
                PhucHoi.Activate();
            else
            {
                PhucHoi = new frmPhucHoi();
                PhucHoi.MdiParent = this;
                PhucHoi.Show();
            }
        }

        private void btnHDBH_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form ctHDBH = IsActive(typeof(frmHoaDonBanHang));

            if (ctHDBH != null)
                ctHDBH.Activate();
            else
            {
                ctHDBH = new frmHoaDonBanHang();
                ctHDBH.MdiParent = this;
                ctHDBH.Show();
            }
        }

        private void btnHDDH_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form ctHDDH = IsActive(typeof(frmHoaDonDatHang));

            if (ctHDDH != null)
                ctHDDH.Activate();
            else
            {
                ctHDDH = new frmHoaDonDatHang();
                ctHDDH.MdiParent = this;
                ctHDDH.Show();
            }
        }

        private void btnLapHDDH_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form lapHDDH = IsActive(typeof(frmLapHoaDonDatHang));

            if (lapHDDH != null)
                lapHDDH.Activate();
            else
            {
                lapHDDH = new frmLapHoaDonDatHang();
                lapHDDH.MdiParent = this;
                lapHDDH.Show();
            }
        }

        private void btnThongTinCuaHang_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form cuaHang = new frmThongTinCuaHang();
            cuaHang.ShowDialog();
            NapStatusBar();
        }

        private void btnThongTinPhanMem_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form about = new frmThongTinPhanMem();
            about.Show();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            NapGiaoDienPhanQuyen(currentUser);

            NapStatusBar();
            tmTimeNow.Start();
            if (Properties.Settings.Default.Backup_CheckStart)
            {
                BusinessLogic.SaoLuu Sl = new BusinessLogic.SaoLuu();
                Sl.BackUp_full(Properties.Settings.Default.Backup_Path, "SL_full");
            }
        }

        private void tmTimeNow_Tick(object sender, EventArgs e)
        {
            lblTime.Caption = DateTime.Now.ToString("HH:mm:ss tt");
            lblDate.Caption = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void btnQuanLyNguoiDung_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form quanLyNguoiDung = IsActive(typeof(frmQuanLyNguoiDung));

            if (quanLyNguoiDung != null)
                quanLyNguoiDung.Activate();
            else
            {
                quanLyNguoiDung = new frmQuanLyNguoiDung();
                quanLyNguoiDung.MdiParent = this;
                quanLyNguoiDung.Show();
            }
        }

        private void btnCaiDatNguoiDung_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form caiDatNguoiDung = new frmCaiDatNguoiDung();
            caiDatNguoiDung.ShowDialog();

        }

        private void lblStoreName_ItemClick(object sender, ItemClickEventArgs e)
        {
            btnThongTinCuaHang_ItemClick(sender, e);
        }

        private void btnDangXuat_ItemClick(object sender, ItemClickEventArgs e)
        {
            Application.Restart();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Properties.Settings.Default.Backup_CheckExit)
            {
                BusinessLogic.SaoLuu Sl = new BusinessLogic.SaoLuu();
                Sl.BackUp_full(Properties.Settings.Default.Backup_Path, "SL_full");
            }

            if (Properties.Settings.Default.Backup_XoaDinhKy)
            {
                frmSaoLuu frmSL = new frmSaoLuu();
                frmSL.XoaDinhKy();
            }
        }

        private void btnKhachHang_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form khachHang = IsActive(typeof(frmKhachHang));

            if (khachHang != null)
                khachHang.Activate();
            else
            {
                khachHang = new frmKhachHang();
                khachHang.MdiParent = this;
                khachHang.Show();
            }
        }

        private void btnNhaCungCap_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form nhaCungCap = IsActive(typeof(frmNhaCungCap));

            if (nhaCungCap != null)
                nhaCungCap.Activate();
            else
            {
                nhaCungCap = new frmNhaCungCap();
                nhaCungCap.MdiParent = this;
                nhaCungCap.Show();
            }
        }

        private void btnTinhTrangThuChi_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form QLTC = IsActive(typeof(frmQuanLiThuChi));
            if (QLTC != null)
                QLTC.Activate();
            else
            {
                QLTC = new frmQuanLiThuChi();
                QLTC.MdiParent = this;
                QLTC.Show();
            }
        }

        private void btnBaoGia_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form bg = IsActive(typeof(frmQuanLiBaoGia));
            if (bg != null)
                bg.Activate();
            else
            {
                bg = new frmQuanLiBaoGia();
                bg.MdiParent = this;
                bg.Show();
            }
        }

        private void btnLapPhieuThu_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form f = new frmThemPhieuThu();
            f.Show();
        }

        private void btnNhapKho_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form bg = IsActive(typeof(frmLapPhieuNhap));
            if (bg != null)
                bg.Activate();
            else
            {
                bg = new frmLapPhieuNhap();
                bg.MdiParent = this;
                bg.Show();
            }
        }

        private void btnXuatKho_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form bg = IsActive(typeof(frmLapPhieuXuat));
            if (bg != null)
                bg.Activate();
            else
            {
                bg = new frmLapPhieuXuat();
                bg.MdiParent = this;
                bg.Show();
            }
        }

        private void btnKhoHang_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form bg = IsActive(typeof(frmTonKho));
            if (bg != null)
                bg.Activate();
            else
            {
                bg = new frmTonKho();
                bg.MdiParent = this;
                bg.Show();
            }
        }

        private void btnLapBaoGia_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form bg = IsActive(typeof(frmLapBaoGia));
            if (bg != null)
                bg.Activate();
            else
            {
                bg = new frmLapBaoGia();
                bg.MdiParent = this;
                bg.Show();
            }
        }

        private void btnLapPhieuChi_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form f = new frmThemPhieuChi();
            f.Show();
        }

        private void btnTrangDau_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form homePage = IsActive(typeof(frmHomePage));
            if (homePage != null)
                homePage.Activate();
            else
            {
                homePage = new frmHomePage(currentUser);
                homePage.MdiParent = this;
                homePage.Show();
            }
        }

        private void btnTroGiup_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Process.Start(Application.StartupPath + "\\Help\\Help.pdf");
            }
            catch
            {
                XtraMessageBox.Show("Không thể hiển thị trợ giúp." + Environment.NewLine + "File không tồn tại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}