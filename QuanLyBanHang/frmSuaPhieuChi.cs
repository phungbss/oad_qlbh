﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmSuaPhieuChi : DevExpress.XtraEditors.XtraForm
    {
        public BusinessLogic.PhieuChi pc_bus;
        public DataTransfer.PhieuChi pc_dt;
        public frmSuaPhieuChi(BusinessLogic.PhieuChi pc = null, DataTransfer.PhieuChi pc_new = null)
        {
            pc_bus = pc;
            pc_dt = pc_new;
            if (pc_bus == null)
                pc_bus = new BusinessLogic.PhieuChi();
            InitializeComponent();
        }
        public void NapGiaoDien()
        {
            txtMaPhieu.Text = pc_dt.MaPhieu;
            dateNgayLap.EditValue = pc_dt.NgayLap;
            txtLyDo.Text = pc_dt.LyDo;
            txtSoTien.Text = pc_dt.SoTien.ToString();
            txtNguoiNop.Text = pc_dt.TenNguoiNhan;
        }

        private void txtSoTien_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsDigit(e.KeyChar)&&!Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void frmThemPhieuThu_Load(object sender, EventArgs e)
        {
           
            NapGiaoDien();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(txtNguoiNop.Text != string.Empty && txtSoTien.Text != string.Empty)
            {
                try
                {
                    DataTransfer.PhieuChi pc = new DataTransfer.PhieuChi(txtMaPhieu.Text,
                        "ND000002", dateNgayLap.DateTime, txtNguoiNop.Text,
                        double.Parse(txtSoTien.Text), txtLyDo.Text);
                    if(pc_bus.Sua(pc))
                    {
                        XtraMessageBox.Show("Phiếu chi " + txtMaPhieu.Text + " đã được cập nhật", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    this.Close();
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập đầy đủ các thông tin", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}