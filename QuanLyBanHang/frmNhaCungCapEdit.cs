﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmNhaCungCapEdit : DevExpress.XtraEditors.XtraForm
    {
        public frmNhaCungCapEdit(DataTransfer.NhaCungCap ncc = null)
        {
            InitializeComponent();
            if (BUS.NhaCungCap == null)
                BUS.NhaCungCap = new BusinessLogic.NhaCungCap();
            this.ncc = ncc;
            isInsert = ncc == null;
        }

        private DataTransfer.NhaCungCap ncc;
        private bool isInsert;

        /// <summary>
        /// Nạp biến lên giao diện
        /// </summary>
        /// <param name="ncc"></param>
        public void NapGiaoDien(DataTransfer.NhaCungCap ncc = null)
        {
            if (ncc == null)
            {
                this.ncc = new DataTransfer.NhaCungCap(BUS.NhaCungCap.AutoGenerateID(), string.Empty, string.Empty, string.Empty, string.Empty);
                NapGiaoDien(this.ncc);
            }
            else
            {
                txtMaNCC.Text = ncc.MaNhaCungCap;
                txtTenNCC.Text = ncc.TenNhaCungCap;
                txtDiaChi.Text = ncc.DiaChi;
                txtSDT.Text = ncc.SoDienThoai;
                txtEmail.Text = ncc.Email;
            }         
        }

        /// <summary>
        /// Nạp giao diện xuống biến
        /// </summary>
        /// <returns></returns>
        public DataTransfer.NhaCungCap NapNhaCungCap()
        {
            ncc.MaNhaCungCap = txtMaNCC.Text;
            ncc.TenNhaCungCap = txtTenNCC.Text;
            ncc.DiaChi = txtDiaChi.Text;
            ncc.SoDienThoai = txtSDT.Text;
            ncc.Email = txtEmail.Text;
            if (ncc.TenNhaCungCap == string.Empty ||
                ncc.DiaChi == string.Empty ||
                ncc.SoDienThoai == string.Empty ||
                ncc.Email == string.Empty)
                throw new ArgumentException("Vui lòng nhập đầy đủ thông tin.");
            return ncc;
        }

        private void frmNhaCungCapEdit_Load(object sender, EventArgs e)
        {
            NapGiaoDien(ncc);

            txtTenNCC.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (isInsert)
                {
                    BUS.NhaCungCap.Them(NapNhaCungCap());
                    XtraMessageBox.Show("Đã thêm nhà cung cấp " + ncc.TenNhaCungCap + " thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    BUS.NhaCungCap.Sua(NapNhaCungCap());
                    XtraMessageBox.Show("Thay đổi thông tin nhà cung cấp " + ncc.TenNhaCungCap + " thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show(ex.Message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch
            {
                XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}