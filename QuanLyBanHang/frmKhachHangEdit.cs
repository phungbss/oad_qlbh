﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmKhachHangEdit : DevExpress.XtraEditors.XtraForm
    {
        public frmKhachHangEdit(DataTransfer.KhachHang kh = null)
        {
            InitializeComponent();
            if (BUS.KhachHang == null)
                BUS.KhachHang = new BusinessLogic.KhachHang();
            this.kh = kh;
            isInsert = kh == null;
        }

        private DataTransfer.KhachHang kh;
        private bool isInsert;

        /// <summary>
        /// Nạp biến lên giao diện
        /// </summary>
        /// <param name="kh"></param>
        public void NapGiaoDien(DataTransfer.KhachHang kh = null)
        {
            cboGioiTinh.Properties.Items.Clear();
            cboGioiTinh.Properties.Items.AddRange(new string[] { "Nam", "Nữ" });
            if (kh == null)
            {
                this.kh = new DataTransfer.KhachHang(BUS.KhachHang.AutoGenerateID(), string.Empty, "Nam", string.Empty, string.Empty, string.Empty);
                NapGiaoDien(this.kh);
            }
            else
            {
                txtMaKH.Text = kh.MaKhachHang;
                txtHoTen.Text = kh.HoTen;
                cboGioiTinh.Text = kh.GioiTinh;
                txtDiaChi.Text = kh.DiaChi;
                txtSDT.Text = kh.SoDienThoai;
                txtEmail.Text = kh.Email;
            }
        }

        /// <summary>
        /// Nạp giao diện xuống biến
        /// </summary>
        /// <returns></returns>
        public DataTransfer.KhachHang NapKhachHang()
        {
            kh.MaKhachHang = txtMaKH.Text;
            kh.HoTen = txtHoTen.Text;
            kh.GioiTinh = cboGioiTinh.Text;
            kh.DiaChi = txtDiaChi.Text;
            kh.SoDienThoai = txtSDT.Text;
            kh.Email = txtEmail.Text;
            if (kh.HoTen == string.Empty || kh.DiaChi == string.Empty || kh.SoDienThoai == string.Empty)
                throw new ArgumentException("Vui lòng nhập đầy đủ thông tin.");
            return kh;
        }

        private void frmKhachHangEdit_Load(object sender, EventArgs e)
        {
            NapGiaoDien(kh);

            txtHoTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (isInsert)
                {
                    BUS.KhachHang.Them(NapKhachHang());
                    XtraMessageBox.Show("Đã thêm khách hàng " + kh.HoTen + " thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    BUS.KhachHang.Sua(NapKhachHang());
                    XtraMessageBox.Show("Thay đổi thông tin khách hàng " + kh.HoTen + " thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.Close();
            }
            catch(ArgumentException ex)
            {
                XtraMessageBox.Show(ex.Message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch
            {
                XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}