﻿using System;
using System.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemSanPham : DevExpress.XtraEditors.XtraForm
    {
        public frmThemSanPham()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }

        public bool IsInsert = false;
        public EventHandler LamMoi;
        public string ID = "";

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.LoaiSanPham == null)
                BUS.LoaiSanPham = new BusinessLogic.LoaiSanPham();
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
        }

        /// <summary>
        /// Dữ liệu được nạp vào khi load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmThemSanPham_Load(object sender, EventArgs e)
        {
            //load combobox Loại sản phẩm
            RepositoryItemComboBox properties = cboTenloai.Properties;
            properties.Items.Clear();
            foreach (DataRow item in BUS.LoaiSanPham.Table.Rows)
            {
                properties.Items.Add(item["TenLoai"].ToString() + " [" + item["MaLoai"].ToString() + "]");
            }

            if (IsInsert) // Phương thức thêm mới sản phẩm
            {
                Text = "Thêm Sản Phẩm";
                btnLuu.Text = "Thêm";
                txtMaSanPham.Text = BUS.SanPham.AutoGenerateID();
                txtMaSanPham.ReadOnly = true;
            }
            else // Phương thức sửa sản phẩm
            {
                Text = "Sửa Sản Phẩm";
                btnLuu.Text = "Lưu";
                txtMaSanPham.ReadOnly = true;
                DataTable dt = BUS.SanPham.Table;
                DataRow r = dt.Rows.Find(ID);
                txtMaSanPham.Text = ID;
                txtTenSanPham.Text = r["TenSanPham"].ToString();
                txtGiaNhap.Text = r["GiaNhap"].ToString();
                txtGiaBan.Text = r["GiaBan"].ToString();
                txtTonKho.Text = r["TonKho"].ToString();
                txtMoTa.Text = r["MoTa"].ToString();
                txtDonVi.Text = r["DonVi"].ToString();
                txtVAT.Text = r["VAT"].ToString();
                txtChietKhau.Text = r["ChietKhau"].ToString();
                dt = BUS.LoaiSanPham.Table;
                r = dt.Rows.Find(r["MaLoai"]);
                cboTenloai.Text = r["TenLoai"].ToString() + " [" + r["MaLoai"] + "]";
            }
        }

        /// <summary>
        /// Lưu sự thay đổi của các phương thức
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsInsert) // Phương thức thêm mới sản phẩm
                {
                    if (txtMaSanPham.Text.Length == 0 || txtTenSanPham.Text.Length == 0 || txtDonVi.Text.Length == 0  || (double)txtGiaNhap.Value < 0 
                    || (double)txtGiaBan.Value < 0 || (int)txtTonKho.Value < 0 || (double)txtVAT.Value < 0 || (double)txtChietKhau.Value < 0 ) // Kiểm tra thông tin cần thiết
                        throw new IndexOutOfRangeException();

                    DataTransfer.SanPham _Sanpham = new DataTransfer.SanPham(txtMaSanPham.Text, txtTenSanPham.Text, // Tạo mới đối tượng sản phẩm
                    (double)txtGiaNhap.Value, (double)txtGiaBan.Value, cboTenloai.Text.Substring(cboTenloai.Text.IndexOf('[') + 1, 8),
                    (int)txtTonKho.Value, txtMoTa.Text, txtDonVi.Text, (double)txtVAT.Value, (double)txtChietKhau.Value);

                    if (BUS.SanPham.Them(_Sanpham)) // Sửa sản phẩm
                        XtraMessageBox.Show("Thêm mới thành công!");

                    LamMoi?.Invoke(sender, e); // Load lại dữ liệu form frmSanPham
                    Close();
                }
                else // Phương thức sửa sản phẩm
                {
                    if (txtMaSanPham.Text.Length == 0 || txtTenSanPham.Text.Length == 0 || txtDonVi.Text.Length == 0 || (double)txtGiaNhap.Value < 0
                    || (double)txtGiaBan.Value < 0 || (int)txtTonKho.Value < 0 || (double)txtVAT.Value < 0 || (double)txtChietKhau.Value < 0) // Kiểm tra thông tin cần thiết
                        throw new IndexOutOfRangeException();

                    DataTransfer.SanPham _Sanpham = new DataTransfer.SanPham(txtMaSanPham.Text, txtTenSanPham.Text,
                    (double)txtGiaNhap.Value, (double)txtGiaBan.Value, cboTenloai.Text.Substring(cboTenloai.Text.IndexOf('[') + 1, 8),
                    (int)txtTonKho.Value, txtMoTa.Text, txtDonVi.Text, (double)txtVAT.Value, (double)txtChietKhau.Value);
                      
                    if (BUS.SanPham.Sua(_Sanpham))
                        XtraMessageBox.Show("Sửa thành công!");
                    LamMoi?.Invoke(sender, e); // Load lại dữ liệu form frmSảnPhẩm
                    Close();
                }
            }
            catch(IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Điền các dòng được in đậm hoặc các giá trị không được là số âm ." , "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Đóng form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHuy_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}