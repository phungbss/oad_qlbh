﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DevExpress.Skins.SkinManager.EnableFormSkins();
            Application.Run(new frmMain());
            //Application.Run(new frmLapPhieuNhap());
            //Application.Run(new frmLapHoaDonBanHang());
            //Application.Run(new frmLapPhieuXuat());
            //Application.Run(new frmTonKho());
            //Application.Run(new frmQuanLiThuChi());
            //Application.Run(new frmThemPhieuThu());
            //Application.Run(new frmLapBaoGia());
            //Application.Run(new frmQuanLiBaoGia());
        }
    }
}
