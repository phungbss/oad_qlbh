﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace QuanLyBanHang
{
    public partial class frmHomePage : DevExpress.XtraEditors.XtraForm
    {
        public frmHomePage(DataTransfer.NguoiDung currUser)
        {
            InitializeComponent();
            currentUser = currUser;
        }

        private DataTransfer.NguoiDung currentUser;

        /// <summary>
        /// Nạp giao diện trang đầu
        /// </summary>
        /// <param name="nd"></param>
        public void NapGiaoDien(DataTransfer.NguoiDung nd)
        {
            if(nd.MaDacQuyen == "DQ002")
            {
                lblHoaDonBanHang.Enabled = false;
                lblHoaDonDatHang.Enabled = false;
                lblThuChi.Enabled = false;
                lblBaoGia.Enabled = false;

                btnHoaDonBanHang.Enabled = false;
                btnHoaDonDatHang.Enabled = false;
                btnThuChi.Enabled = false;
                btnBaoGia.Enabled = false;
            }   
            lblWelcome.Text = "Xin chào, " + nd.HoTen;      
        }
        private void frmHomePage_Load(object sender, EventArgs e)
        {
            NapGiaoDien(currentUser);
        }


        private void lblHoaDonBanHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmHoaDonBanHang();
            hdbh.MdiParent = this.MdiParent;
            hdbh.Show();
        }

        private void lblHoaDonDatHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hddh = new frmHoaDonDatHang();
            hddh.MdiParent = this.MdiParent;
            hddh.Show();
        }

        private void lblKhoHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form kho = new frmTonKho();
            kho.MdiParent = this.MdiParent;
            kho.Show();
        }

        private void lblThuChi_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form thuChi = new frmQuanLiThuChi();
            thuChi.MdiParent = this.MdiParent;
            thuChi.Show();
        }

        private void lblBaoGia_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form baoGia = new frmQuanLiBaoGia();
            baoGia.MdiParent = this.MdiParent;
            baoGia.Show();
        }

        private void lblKhachHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form khachHang = new frmKhachHang();
            khachHang.MdiParent = this.MdiParent;
            khachHang.Show();
        }

        private void lblNhaCungCap_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form ncc = new frmNhaCungCap();
            ncc.MdiParent = this.MdiParent;
            ncc.Show();
        }

        private void lblLapHoaDonBanHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmLapHoaDonBanHang();
            hdbh.MdiParent = this.MdiParent;
            hdbh.Show();
        }

        private void lblLapHoaDonDatHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmLapHoaDonDatHang();
            hdbh.MdiParent = this.MdiParent;
            hdbh.Show();
        }

        private void lblLapPhieuNhapKho_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmLapPhieuNhap();
            hdbh.MdiParent = this.MdiParent;
            hdbh.Show();
        }

        private void lblLapPhieuXuatKho_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmLapPhieuXuat();
            hdbh.MdiParent = this.MdiParent;
            hdbh.Show();
        }

        private void lblLapPhieuThu_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmThemPhieuThu();
            hdbh.Show();
        }

        private void lblLapPhieuChi_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmThemPhieuChi();
            hdbh.Show();
        }

        private void lblLapBaoGia_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form hdbh = new frmLapBaoGia();
            hdbh.MdiParent = this.MdiParent;
            hdbh.Show();
        }

        private void btnSanPham_Click(object sender, EventArgs e)
        {
            Form sanPham = new frmSanPham(currentUser);
            sanPham.MdiParent = this.MdiParent;
            sanPham.Show();
        }

        private void btnLoaiSanPham_Click(object sender, EventArgs e)
        {
            Form loaiSP = new frmLoaiSanPham(currentUser);
            loaiSP.MdiParent = this.MdiParent;
            loaiSP.Show();
        }

        private void btnLapHoaDonBanHang_Click(object sender, EventArgs e)
        {
            lblLapHoaDonBanHang_LinkClicked(sender, null);
        }

        private void btnHoaDonBanHang_Click(object sender, EventArgs e)
        {
            lblHoaDonBanHang_LinkClicked(sender, null);
        }

        private void btnKhoHang_Click(object sender, EventArgs e)
        {
            lblKhoHang_LinkClicked(sender, null);
        }

        private void btnNhapKho_Click(object sender, EventArgs e)
        {
            lblLapPhieuNhapKho_LinkClicked(sender, null);
        }

        private void btnLapPhieuThu_Click(object sender, EventArgs e)
        {
            lblLapPhieuThu_LinkClicked(sender, null);
        }

        private void btnKhachHang_Click(object sender, EventArgs e)
        {
            lblKhachHang_LinkClicked(sender, null);
        }

        private void btnNhaCungCap_Click(object sender, EventArgs e)
        {
            lblNhaCungCap_LinkClicked(sender, null);
        }

        private void btnLapBaoGia_Click(object sender, EventArgs e)
        {
            lblLapBaoGia_LinkClicked(sender, null);
        }

        private void btnBaoGia_Click(object sender, EventArgs e)
        {
            lblBaoGia_LinkClicked(sender, null);
        }

        private void btnThuChi_Click(object sender, EventArgs e)
        {
            lblThuChi_LinkClicked(sender, null);
        }

        private void btnLapPhieuChi_Click(object sender, EventArgs e)
        {
            lblLapPhieuChi_LinkClicked(sender, null);
        }

        private void btnLapHoaDonDatHang_Click(object sender, EventArgs e)
        {
            lblLapHoaDonDatHang_LinkClicked(sender, null);
        }

        private void btnHoaDonDatHang_Click(object sender, EventArgs e)
        {
            lblHoaDonDatHang_LinkClicked(sender, null);
        }

        private void btnXuatKho_Click(object sender, EventArgs e)
        {
            lblLapPhieuXuatKho_LinkClicked(sender, null);
        }

        private void lblHuongDanSuDung_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(Application.StartupPath + "\\Help\\Help.pdf");
            }
            catch
            {
                XtraMessageBox.Show("Không thể hiển thị trợ giúp." + Environment.NewLine + "File không tồn tại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}