﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmSanPham : XtraForm
    {
        public frmSanPham(DataTransfer.NguoiDung currUser = null)
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
            currentUser = currUser;
        }

        private string MaSanPham;
        private bool CheckLsp_Click = false;
        private DataTransfer.NguoiDung currentUser;

        /// <summary>
        /// Load giao diện và dữ liệu sản phẩm
        /// </summary>
        private void ShowScreenSP()
        {
            GridSanPham.DataSource = BUS.SanPham.Table;
            CountSumSP(BUS.SanPham.SelectAll());            
            gridViewSanpham.ExpandAllGroups();
        }

        public void NapGiaoDienPhanQuyen(DataTransfer.NguoiDung nd)
        {
            //quyền Khách
            if (nd == null)
            {
                btnThemLoai.Enabled = false;
                btnSuaLoai.Enabled = false;
                btnXoaLoai.Enabled = false;
                btnThemSanpham.Enabled = false;
                btnSuaSanpham.Enabled = false;
                btnXoa.Enabled = false;
            }
        }

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.LoaiSanPham == null)
                BUS.LoaiSanPham = new BusinessLogic.LoaiSanPham();
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
        }

        /// <summary>
        /// Load giao diện và dữ liệu loại sản phẩm
        /// </summary>
        private void ShowScreenLSP()
        {
            GridLoaiSanPham.DataSource = BUS.LoaiSanPham.Table;
            CountSumLSP(BUS.LoaiSanPham.Table); 
            gridViewLsp.ExpandAllGroups();
        }

        /// <summary>
        /// Đếm tổng số sản phẩm
        /// </summary>
        /// <param name="dtSanpham">Bảng sản phẩm</param>
        private void CountSumSP(DataTable dtSanpham)
        {
            lbSumSp.Text = dtSanpham.Rows.Count.ToString() + " Cái"; 
        }

        /// <summary>
        /// Đếm tổng số loại sản phẩm
        /// </summary>
        /// <param name="dtLoaisanpham">Bảng loại sản phẩm</param>
        private void CountSumLSP(DataTable dtLoaisanpham)
        {
            lbSumLsp.Text = dtLoaisanpham.Rows.Count.ToString() + " Loại";
        }

        /// <summary>
        /// Lấy dữ liệu khi load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void frmSanPham_Load(object sender, EventArgs e)
        {
            ShowScreenSP();
            ShowScreenLSP();
            NapGiaoDienPhanQuyen(currentUser);
        }

        /// <summary>
        /// Sự kiện load lại các dữ liệu sản phẩm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshSP(object sender, EventArgs e)
        {
            if (CheckLsp_Click)
                ShowScreenSPinLSP(MaSanPham);
            else
                ShowScreenSP();
        }

        /// <summary>
        /// Sự kiện load lại các dữ liệu loại sản phẩm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshLSP(object sender, EventArgs e)
        {
            ShowScreenLSP();
        }

        #region Các sự kiện trong sản phẩm

        /// <summary>
        /// Xóa sản phẩm được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnXoaSanPham_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("Bạn có muốn xóa sản phẩm " + gridViewSanpham.GetRowCellValue(gridViewSanpham.FocusedRowHandle, gridViewSanpham.Columns[1]).ToString() + " ?"
                   + "\nThao tác này sẽ xóa các hóa đơn liên quan đến sản phẩm này .", "Thông báo", MessageBoxButtons.YesNo) ==
                    DialogResult.Yes)
                {
                    string ID = gridViewSanpham.GetRowCellValue(gridViewSanpham.FocusedRowHandle, gridViewSanpham.Columns[0]).ToString();
                    if (BUS.SanPham.Xoa(ID))
                        XtraMessageBox.Show("Đã xóa thông tin thành công!");

                    if (CheckLsp_Click)
                        ShowScreenSPinLSP(MaSanPham);
                    else
                        ShowScreenSP();
                }
            }
            catch
            {
                XtraMessageBox.Show("Bạn chưa chọn sản phẩm muốn xóa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Sự kiện thêm sản phẩm khi bấm vào nút thêm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnThemSanPham_Click(object sender, EventArgs e)
        {
            frmThemSanPham frmThemsp = new frmThemSanPham();
            frmThemsp.IsInsert = true;
            frmThemsp.LamMoi += new EventHandler(RefreshSP);
            frmThemsp.ShowInTaskbar = false;
            frmThemsp.ShowDialog();
        }

        /// <summary>
        /// Chạy form sửa sản phẩm khi Double click vào bảng dữ liệu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void msds_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnSuaSanpham_Click(sender, e);
        }

        /// <summary>
        /// Sửa sản phẩm được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSuaSanpham_Click(object sender, EventArgs e)
        {
            try
            {
                frmThemSanPham frmThemsp = new frmThemSanPham();
                frmThemsp.IsInsert = false;
                frmThemsp.ID = gridViewSanpham.GetRowCellValue(gridViewSanpham.FocusedRowHandle, gridViewSanpham.Columns[0]).ToString();
                frmThemsp.LamMoi += new EventHandler(RefreshSP);
                frmThemsp.ShowInTaskbar = false;
                frmThemsp.ShowDialog();
            }
            catch
            {
            }
        }

        #endregion

        #region Các sự kiện trong loại sản phẩm

        /// <summary>
        /// Sự kiện thêm loại sản phẩm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnThemLoai_Click(object sender, EventArgs e)
        {
            frmThemLoaiSanPham frm = new frmThemLoaiSanPham();
            frm.IsInsert = true;
            frm.LamMoi += new EventHandler(RefreshLSP);
            frm.ShowInTaskbar = false;
            frm.ShowDialog();
        }

        /// <summary>
        /// sửa loại sản phẩm được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSuaLoai_Click(object sender, EventArgs e)
        {
            try
            {
                frmThemLoaiSanPham frmSuaLoai = new frmThemLoaiSanPham();
                frmSuaLoai.IsInsert = false;
                frmSuaLoai.IDLoai = gridViewLsp.GetRowCellValue(gridViewLsp.FocusedRowHandle, gridViewLsp.Columns[0]).ToString();
                frmSuaLoai.LamMoi += new EventHandler(RefreshLSP);
                frmSuaLoai.ShowInTaskbar = false;
                frmSuaLoai.ShowDialog();
            }
            catch
            {
                XtraMessageBox.Show("Bạn chưa chọn loại sản phẩm muốn sửa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// xóa loại sản phẩm được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnXoaLoai_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("Bạn có muốn xóa loại sản phẩm " + gridViewLsp.GetRowCellValue(gridViewLsp.FocusedRowHandle, gridViewLsp.Columns[1]).ToString() + " ?\nThao tác này sẽ xóa các sản phẩm , hóa đơn liên quan đến loại sản phẩm",
                "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string IDLoai = gridViewLsp.GetRowCellValue(gridViewLsp.FocusedRowHandle, gridViewLsp.Columns[0]).ToString();

                    DataRow[] cthd_row = BUS.SanPham.Table.Select("MaLoai like '" + IDLoai + "'");
                    foreach (DataRow item in cthd_row)
                        BUS.SanPham.Xoa( item["MaSanPham"].ToString());

                    if (BUS.LoaiSanPham.Xoa(IDLoai))
                        {
                        CheckLsp_Click = false;
                        XtraMessageBox.Show("Đã xóa thông tin thành công!");
                        }
                    ShowScreenLSP();
                    ShowScreenSP();
                }
            }
            catch
            {
                XtraMessageBox.Show("Bạn chưa chọn loại sản phẩm muốn xóa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Lấy dữ liệu sản phẩm của loại sản phẩm được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void msds1_Click(object sender, EventArgs e)
        {
            try
            {
                CheckLsp_Click = true;
                MaSanPham = gridViewLsp.GetRowCellValue(gridViewLsp.FocusedRowHandle, gridViewLsp.Columns[0]).ToString();
                ShowScreenSPinLSP(MaSanPham);
            }
            catch
            {
                XtraMessageBox.Show("Chọn Loại sản phẩm !");
            }
        }

        /// <summary>
        /// Hiển thị dữ liệu sản phẩm của loại sản phẩm
        /// </summary>
        /// <param name="MaLoai"></param>
        private void ShowScreenSPinLSP(string MaLoai)
        {
            GridSanPham.DataSource = BUS.SanPham.SelectAll();

            string filter = "MaLoai='" + MaLoai + "'";

            DataTable tbl = (DataTable)GridSanPham.DataSource;
            DataRow[] rows = tbl.Select(filter);

            tbl = ((DataTable)GridSanPham.DataSource).Clone();
            for (int i = 0; i < rows.Length; i++)
            {
                DataRow row = tbl.NewRow();
                row[0] = rows[i].ItemArray[0].ToString();
                row[1] = rows[i].ItemArray[1].ToString();
                row[2] = rows[i].ItemArray[2].ToString();
                row[3] = rows[i].ItemArray[3].ToString();
                row[4] = rows[i].ItemArray[4].ToString();
                row[5] = rows[i].ItemArray[5].ToString();
                row[6] = rows[i].ItemArray[6].ToString();
                row[7] = rows[i].ItemArray[7].ToString();
                row[8] = rows[i].ItemArray[8].ToString();
                row[9] = rows[i].ItemArray[9].ToString();
                tbl.Rows.Add(row);
            }
            GridSanPham.DataSource = tbl;
            CountSumSP(tbl);
            gridViewSanpham.ExpandAllGroups();
        }

        #endregion
    }
}