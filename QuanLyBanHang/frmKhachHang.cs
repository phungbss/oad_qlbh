﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmKhachHang : DevExpress.XtraEditors.XtraForm
    {
        public frmKhachHang()
        {
            InitializeComponent();
            if (BUS.KhachHang == null)
                BUS.KhachHang = new BusinessLogic.KhachHang();
        }

        DataTransfer.KhachHang kh;

        /// <summary>
        /// Nạp giao diện
        /// </summary>
        public void NapGiaoDien()
        {
            gridDSKH.DataSource = BUS.KhachHang.Table;
            lblTongCong.Text = string.Format("Tổng cộng: {0} khách hàng", gridViewDSKH.RowCount);
        }


        private void frmKhachHang_Load(object sender, EventArgs e)
        {
            NapGiaoDien();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Form khEdit = new frmKhachHangEdit();
            khEdit.Text = "Thêm khách hàng";
            khEdit.ShowDialog();
            NapGiaoDien();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            DataRow r = gridViewDSKH.GetDataRow(gridViewDSKH.FocusedRowHandle);
            kh = new DataTransfer.KhachHang(r["MaKhachHang"].ToString(), r["HoTen"].ToString(),
                r["GioiTinh"].ToString(), r["DiaChi"].ToString(), r["SoDienThoai"].ToString(), r["Email"].ToString());

            Form khEdit = new frmKhachHangEdit(kh);
            khEdit.Text = "Sửa thông tin khách hàng";
            khEdit.ShowDialog();
            NapGiaoDien();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewDSKH.GetDataRow(gridViewDSKH.FocusedRowHandle);
                if (XtraMessageBox.Show("Bạn có muốn xóa khách hàng " + r["HoTen"].ToString() + "?" + Environment.NewLine +
                    "Thao tác này sẽ xóa tất cả các Hóa đơn bán hàng và Hóa đơn đặt hàng của khách hàng này!",
                    "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (BUS.KhachHang.Xoa(r["MaKhachHang"].ToString()))
                    {
                        XtraMessageBox.Show("Đã xóa khách hàng thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NapGiaoDien();
                    }
                    else
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch
            {
                XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridDSKH_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(sender, e);
        }
    }
}