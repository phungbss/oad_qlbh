﻿using System;
using QuanLyBanHang.Properties;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmThongTinCuaHang : DevExpress.XtraEditors.XtraForm
    {
        public frmThongTinCuaHang()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Nạp thông tin cài đặt lên giao diện
        /// </summary>
        public void NapThongTin()
        {
            txtTenCuaHang.Text = Settings.Default.StoreInfo_Name;
            txtDiaChi.Text = Settings.Default.StoreInfo_Address;
            txtSoDienThoai.Text = Settings.Default.StoreInfo_Tel;
            txtEmail.Text = Settings.Default.StoreInfo_Email;
        }

        /// <summary>
        /// Lưu thông tin từ giao diện xuống cài đặt
        /// </summary>
        public void LuuThongTin()
        {
            if (string.IsNullOrEmpty(txtTenCuaHang.Text) ||
                   string.IsNullOrEmpty(txtDiaChi.Text) ||
                   string.IsNullOrEmpty(txtSoDienThoai.Text))
                throw new ArgumentException("Vui lòng nhập đầy đủ thông tin");

            Settings.Default.StoreInfo_Name = txtTenCuaHang.Text;
            Settings.Default.StoreInfo_Address = txtDiaChi.Text;
            Settings.Default.StoreInfo_Tel = txtSoDienThoai.Text;
            Settings.Default.StoreInfo_Email = txtEmail.Text;
            Settings.Default.Save();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSoDienThoai_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                LuuThongTin();
                XtraMessageBox.Show("Đã lưu thông tin thành công", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (ArgumentException ex)
            {
                XtraMessageBox.Show(ex.Message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void frmThongTinCuaHang_Load(object sender, EventArgs e)
        {
            NapThongTin();
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                try
                {
                    LuuThongTin();
                    XtraMessageBox.Show("Đã lưu thông tin thành công", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                catch (ArgumentException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}