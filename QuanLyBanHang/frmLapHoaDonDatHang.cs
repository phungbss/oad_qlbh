﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;

namespace QuanLyBanHang
{
    public partial class frmLapHoaDonDatHang : DevExpress.XtraEditors.XtraForm
    {

        private DataTransfer.HoaDonDatHang hddh;
        DataTable tbCTHD = new DataTable();
        private bool isInsert;

        public frmLapHoaDonDatHang(DataTransfer.HoaDonDatHang hddh = null)
        {
            InitializeComponent();

            this.hddh = hddh;

            isInsert = hddh == null;

            KhoiTaoCacBUSCanThiet();
        }

        /// <summary>
        /// Nạp giao diện mặc định hoặc nạp hd lên form
        /// </summary>
        /// <param name="hd">Đối tượng hd để xác định là sửa hay thêm mới</param>
        public void NapGiaoDien(DataTransfer.HoaDonDatHang hd = null)
        {
            if (hd == null)
            {
                hddh = new DataTransfer.HoaDonDatHang(BUS.HoaDonDatHang.AutoGenerateID(), string.Empty, string.Empty,DateTime.Now,
                    DateTime.Now, 0, 0, false, string.Empty);
                NapGiaoDien(this.hddh);
            }
            else
            {
                //load combobox Khách hàng
                cboKhachHang.Properties.Columns.Add(new LookUpColumnInfo("HoTen"));
                cboKhachHang.Properties.DataSource = BUS.KhachHang.Table;
                cboKhachHang.Properties.DisplayMember = "HoTen";
                cboKhachHang.Properties.ValueMember = "MaKhachHang";

                //load hóa đơn
                txtMaHoaDon.Text = hd.MaHoaDon;
                dateNgayLap.EditValue = hd.NgayLap;
                dateNgayDuKienGiao.EditValue = hd.NgayDuKienGiao;
                cboKhachHang.EditValue = hd.MaKhachHang;
                txtGhiChu.Text = hd.GhiChu;
                txtTongTien.Value = (decimal)hd.TongTien;
                txtThanhTien.Value = (decimal)hd.ThanhTien;
                chkDaGiao.Checked = hd.DaGiao;

                //load chi tiết hóa đơn của hóa đơn
                tbCTHD.Rows.Clear();
                DataRow[] rows = BUS.CTHoaDonDatHang.Table.Select("MaHoaDon = '" + hd.MaHoaDon + "'");
                foreach (DataRow item in rows)
                {
                    DataRow r = tbCTHD.NewRow();
                    DataRow _r = BUS.SanPham.Table.Select("MaSanPham = '" + item["MaSanPham"] + "'")[0];
                    r["MaSanPham"] = item["MaSanPham"];
                    r["TenSanPham"] = _r["TenSanPham"];
                    r["DonVi"] = _r["DonVi"];
                    r["SoLuong"] = item["SoLuong"];
                    r["ChietKhau"] = _r["ChietKhau"];
                    r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                    tbCTHD.Rows.Add(r);
                }
            }

            lblTongSanPham.Text = "Tổng cộng: " + BUS.SanPham.Table.Rows.Count + " sản phẩm.";
            lblTongChonSanPham.Text = "Tổng cộng: " + tbCTHD.Rows.Count + " sản phẩm.";

        }

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
            if (BUS.KhachHang == null)
                BUS.KhachHang = new BusinessLogic.KhachHang();
            if (BUS.HoaDonDatHang == null)
                BUS.HoaDonDatHang = new BusinessLogic.HoaDonDatHang();
        }

        /// <summary>
        /// Khởi tạo bảng cho tbCTHD
        /// </summary>
        public void KhoiTaoTableCTHD()
        {
            tbCTHD.Columns.Add("MaSanPham");
            tbCTHD.Columns.Add("TenSanPham");
            tbCTHD.Columns.Add("DonVi");
            tbCTHD.Columns.Add("SoLuong");
            tbCTHD.Columns.Add("ThanhTien");
            tbCTHD.Columns.Add("ChietKhau");
            tbCTHD.PrimaryKey = new DataColumn[] { tbCTHD.Columns[0] };
            tbCTHD.RowChanged += TbCTHD_RowChanged;
            tbCTHD.RowDeleted += TbCTHD_RowChanged;
            tbCTHD.TableCleared += TbCTHD_TableCleared;
        }

        private void TbCTHD_TableCleared(object sender, DataTableClearEventArgs e)
        {
            lblTongChonSanPham.Text = "Tổng cộng: " + tbCTHD.Rows.Count + " sản phẩm.";
        }

        private void TbCTHD_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            lblTongChonSanPham.Text = "Tổng cộng: " + tbCTHD.Rows.Count + " sản phẩm.";
        }

        /// <summary>
        /// Cập nhật số tiền của hóa đơn
        /// </summary>
        public void CapNhatSoTienHoaDon()
        {
            long tongTien = 0;
            long thanhTien = 0;
            foreach (DataRow item in tbCTHD.Rows)
            {
                tongTien += Convert.ToInt64(item["ThanhTien"]);
                thanhTien += Convert.ToInt64(item["ThanhTien"]) * (100 - Convert.ToInt32(item["ChietKhau"])) / 100;
            }
            txtTongTien.Value = tongTien;
            txtThanhTien.Value = thanhTien;
        }

        /// <summary>
        /// Nạp từ form xuống đối tượng dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTransfer.HoaDonDatHang NapHoaDon()
        {
            hddh.MaHoaDon = txtMaHoaDon.Text;
            hddh.MaKhachHang = cboKhachHang.EditValue.ToString();
            hddh.MaNhanVien = "ND000001";
            hddh.NgayLap = dateNgayLap.DateTime;
            hddh.NgayDuKienGiao = dateNgayDuKienGiao.DateTime;
            hddh.TongTien = (double)txtTongTien.Value;
            hddh.ThanhTien = (double)txtThanhTien.Value;
            hddh.DaGiao = chkDaGiao.Checked;

            if (string.IsNullOrEmpty(hddh.MaKhachHang))
                throw new ArgumentException("Vui lòng nhập đầy đủ thông tin.");
            return hddh;
        }

        private void frmLapHoaDonBanHang_Load(object sender, EventArgs e)
        {
            BUS.CTHoaDonDatHang = new BusinessLogic.CTHoaDonDatHang();

            KhoiTaoTableCTHD();
            gridChiTietSanPham.DataSource = tbCTHD;
            gridChonSanPham.DataSource = BUS.SanPham.Table;

            NapGiaoDien(hddh);
        }

        private void gridChonSanPham_Click(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSP.GetRowCellValue(gridViewChonSP.FocusedRowHandle, gridViewChonSP.Columns[0]).ToString();
            }
            catch
            {
            }
        }

        private void btnChonSanPham_Click(object sender, EventArgs e)
        {
            string nameItem = "";
            try
            {
                DataRow r = tbCTHD.NewRow();
                DataRow _r = gridViewChonSP.GetDataRow(gridViewChonSP.FocusedRowHandle);
                nameItem = Convert.ToString(_r["TenSanPham"]);
                DataRow _r1 = BUS.SanPham.Table.Select("MaSanPham = '" + Convert.ToString(_r["MaSanPham"]) + "'")[0];
                if (Convert.ToInt32(_r1["TonKho"]) == 0)
                    throw new ArgumentException("OutNumber");
                r["MaSanPham"] = _r["MaSanPham"];
                r["TenSanPham"] = _r["TenSanPham"];
                r["DonVi"] = _r["DonVi"];
                r["SoLuong"] = 1;
                r["ChietKhau"] = _r1["ChietKhau"];
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                tbCTHD.Rows.Add(r);
                gridViewCTSP.SelectRow(gridViewCTSP.FindRow(r));
                gridViewCTSP.Focus();
                CapNhatSoTienHoaDon();
            }
            catch (ConstraintException)
            {
                XtraMessageBox.Show("Bạn đã chọn sản phẩm " + txtChonSanPham.Text + " rồi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException)
            {
                XtraMessageBox.Show("Sản phẩm " + nameItem + " tạm hết hàng !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                XtraMessageBox.Show("Lựa chọn không hợp lệ , xin hãy lựa chọn lại !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private void gridChonSanPham_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSP.GetRowCellValue(gridViewChonSP.FocusedRowHandle, gridViewChonSP.Columns[0]).ToString();

                btnChonSanPham_Click(sender, e);
            }
            catch { }
        }

        private void btnSLTang1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                if (Convert.ToInt32(r["SoLuong"]) != Convert.ToInt32(_r["TonKho"]))
                {
                    r["SoLuong"] = Convert.ToInt32(r["SoLuong"]) + 1;
                    r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                    CapNhatSoTienHoaDon();
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            catch(IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Số lượng sản phẩm xuất không được lớn hơn số sản phẩm tồn kho ");
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSLGiam1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                int sl = Convert.ToInt32(r["SoLuong"]);
                r["SoLuong"] = sl > 1 ? sl - 1 : sl;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                CapNhatSoTienHoaDon();
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                tbCTHD.Rows.Remove(gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle));
            }
            catch
            {
                XtraMessageBox.Show("Không có gì để xóa!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoaTatCa_Click(object sender, EventArgs e)
        {
            tbCTHD.Rows.Clear();
        }

        private void btnTaoHoaDon_Click(object sender, EventArgs e)
        {
            if (DateTime.Compare(dateNgayDuKienGiao.DateTime, dateNgayLap.DateTime) != -1 || 
                dateNgayDuKienGiao.DateTime.ToShortDateString() == dateNgayLap.DateTime.ToShortDateString() ) 
            {
                if (tbCTHD.Rows.Count > 0)
                {
                    try
                    {
                        //thêm hóa đơn chính
                        this.hddh = NapHoaDon();
                        if (isInsert)
                        {
                            BUS.HoaDonDatHang.Them(hddh);

                        }
                        else
                        {
                            BUS.HoaDonDatHang.Sua(hddh);

                            //xóa các sản phẩm của hóa đơn
                            DataTransfer.CTHoaDonDatHang _ct;
                            DataRow[] rows = BUS.CTHoaDonDatHang.Table.Select("MaHoaDon = '" + hddh.MaHoaDon + "'");
                            foreach (DataRow item in rows)
                            {
                                _ct = new DataTransfer.CTHoaDonDatHang(item["MaHoaDon"].ToString(), item["MaSanPham"].ToString(), Convert.ToInt16(item["SoLuong"]));
                                BUS.CTHoaDonDatHang.Xoa(_ct.MaHoaDon, _ct.MaSanPham);
                            }
                        }

                        //thêm sản phẩm của hóa đơn
                        DataTransfer.CTHoaDonDatHang ct;
                        foreach (DataRow r in tbCTHD.Rows)
                        {
                            ct = new DataTransfer.CTHoaDonDatHang(txtMaHoaDon.Text, r["MaSanPham"].ToString(), Convert.ToInt32(r["SoLuong"]));
                            if (hddh.DaGiao == true)
                                BUS.CTHoaDonDatHang.Them(ct, true);
                            else
                                BUS.CTHoaDonDatHang.Them(ct);
                        }

                        XtraMessageBox.Show("Hóa đơn đã được lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        if (isInsert)
                            NapGiaoDien();
                        else
                            Close();
                    }
                    catch (ArgumentException ex)
                    {
                        XtraMessageBox.Show(ex.Message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    catch
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    XtraMessageBox.Show("Phải chọn ít nhất một sản phẩm để tạo hóa đơn", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                XtraMessageBox.Show("Ngày dự kiến giao phải lớn hơn ngày lập", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (isInsert)
                NapGiaoDien();
            else
                Close();
        }

        private void btnThemKhachHang_Click(object sender, EventArgs e)
        {
            Form khEdit = new frmKhachHangEdit();
            khEdit.Text = "Thêm khách hàng";
            khEdit.ShowDialog();
        }

        private void chkDaGiao_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}