﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmLapBaoGia : DevExpress.XtraEditors.XtraForm
    {
        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
            if (BUS.LoaiSanPham == null)
                BUS.LoaiSanPham = new BusinessLogic.LoaiSanPham();
            if (BUS.BaoGia == null)
                BUS.BaoGia = new BusinessLogic.BaoGia();
            if (BUS.CTBaoGia == null)
                BUS.CTBaoGia = new BusinessLogic.CTBaoGia();
        }
        DataTable tbBG = new DataTable();
        DataTable tbSP = new DataTable();
        public frmLapBaoGia()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }
        public void NapGiaoDien()
        {
            txtMaBG.Text = BUS.BaoGia.AutoGenerateID();
            dateNgayLap.EditValue = DateTime.Now;
            txtGhiChu.Text = string.Empty;
            tbBG.Rows.Clear();
            tbSP.Rows.Clear();
        }
        public void KhoiTaoBangCTBG()
        {
            tbBG.Columns.Add("MaSanPham");
            tbBG.Columns.Add("TenSanPham");
            tbBG.Columns.Add("GiaBan");
            tbBG.Columns.Add("DonVi");
            tbBG.Columns.Add("GhiChu");
            tbBG.PrimaryKey = new DataColumn[] { tbBG.Columns[0] };
        }
        public void KhoiTaoBangSP()
        {
            tbSP.Columns.Add("MaSanPham");
            tbSP.Columns.Add("TenSanPham");
            tbSP.PrimaryKey = new DataColumn[] { tbSP.Columns[0] };
        }

        private void frmLapBaoGia_Load(object sender, EventArgs e)
        {
            KhoiTaoBangCTBG();
            KhoiTaoBangSP();

            gridSP.DataSource = tbSP;
            gridLoaiSP.DataSource = BUS.LoaiSanPham.Table;
            gridBaoGia.DataSource = tbBG;

            //labLoaiSP.Text = lsp_bus.Table.Rows.Count.ToString() + " loại sản phẩm";
            //labSP.Text = "Sản phẩm";
            //labBG.Text = "Danh sách sản phẩm báo giá";

            NapGiaoDien();
        }

        private void gridLoaiSP_Click(object sender, EventArgs e)
        {
            string LoaiSP = gridViewLoaiSP.GetRowCellValue(gridViewLoaiSP.FocusedRowHandle, gridViewLoaiSP.Columns[0]).ToString();
            try
            {
                tbSP.Rows.Clear();
                foreach(DataRow _r in BUS.SanPham.Table.Rows)
                {
                    if(_r["MaLoai"].ToString() == LoaiSP)
                    {
                        DataRow r = tbSP.NewRow();
                        r["MaSanPham"] = _r["MaSanPham"];
                        r["TenSanPham"] = _r["TenSanPham"];
                        tbSP.Rows.Add(r);
                    }
                }
                gridSP.DataSource = tbSP;
                //labSP.Text = tbSP.Rows.Count.ToString() + " sản phẩm";
            }
            catch(ConstraintException)
            {
                XtraMessageBox.Show("Bạn đã chọn sản phẩm " + LoaiSP + " rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridSP_Click(object sender, EventArgs e)
        {
            try
            {
                string MaSanPham = gridViewSP.GetRowCellValue(gridViewSP.FocusedRowHandle, gridViewSP.Columns[0]).ToString();
                
                foreach(DataRow _r in BUS.SanPham.Table.Rows)
                {
                    if(_r["MaSanPham"].ToString() == MaSanPham)
                    {
                        DataRow r = tbBG.NewRow();
                        r["MaSanPham"] = _r["MaSanPham"];
                        r["TenSanPham"] = _r["TenSanPham"];
                        r["GiaBan"] = _r["GiaBan"];
                        r["DonVi"] = _r["DonVi"];
                        tbBG.Rows.Add(r);
                    }
                }
                gridBaoGia.DataSource = tbBG;
                //labBG.Text = tbBG.Rows.Count.ToString() + " sản phẩm trong danh sách";
            }
            catch { }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                tbBG.Rows.Remove(gridViewBaoGia.GetDataRow(gridViewBaoGia.FocusedRowHandle));
            }
            catch
            {
                XtraMessageBox.Show("Không thể xóa khi danh sách trống!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoaTatCa_Click(object sender, EventArgs e)
        {
            tbBG.Rows.Clear();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(tbBG.Rows.Count > 0)
            {
                try
                {
                    //thêm báo giá
                    DataTransfer.BaoGia bg = new DataTransfer.BaoGia(txtMaBG.Text,
                        "ND000002", dateNgayLap.DateTime, txtGhiChu.Text);
                    if(BUS.BaoGia.Them(bg))
                    {
                        XtraMessageBox.Show("Báo giá " + txtMaBG.Text + " đã được thêm thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    //thêm chi tiết báo giá
                    DataTransfer.CTBaoGia ct;
                    foreach(DataRow r in tbBG.Rows)
                    {
                        ct = new DataTransfer.CTBaoGia(txtMaBG.Text, r["MaSanPham"].ToString(), double.Parse(r["GiaBan"].ToString()), r["GhiChu"].ToString());
                        BUS.CTBaoGia.Them(ct);                      
                    }
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Chưa có sản phẩm nào trong Báo giá", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnHuy_CheckedChanged(object sender, EventArgs e)
        {
            NapGiaoDien();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            //Not Available Yet
        }
    }
}