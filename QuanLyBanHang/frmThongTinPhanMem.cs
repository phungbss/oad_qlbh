﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace QuanLyBanHang
{
    public partial class frmThongTinPhanMem : DevExpress.XtraEditors.XtraForm
    {
        public frmThongTinPhanMem()
        {
            InitializeComponent();
        }

        public void NapThongTin()
        {
            lblPhienBan.Text = Application.ProductVersion;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThongTinPhanMem_Load(object sender, EventArgs e)
        {
            NapThongTin();
        }

        private void lblLienHe_Click(object sender, EventArgs e)
        {
            Process.Start("mailto:nguyentuanit96@gmail.com");
        }
    }
}