﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmQuanLiThuChi : DevExpress.XtraEditors.XtraForm
    {
        //BusinessLogic.PhieuThu pt_bus;
        //BusinessLogic.PhieuChi pc_bus;
        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.PhieuThu == null)
                BUS.PhieuThu = new BusinessLogic.PhieuThu();
            if (BUS.PhieuChi == null)
                BUS.PhieuChi = new BusinessLogic.PhieuChi();
        }
        DataTable pt = new DataTable();
        DataTable pc = new DataTable();
        public frmQuanLiThuChi()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }
        /// <summary>
        /// Nạp giao diện
        /// </summary>
        public void NapGiaoDien()
        {
            //Thu
            txtMaPThu.Text = "PT";
            dateToThu.EditValue = DateTime.Now;
            dateFromThu.EditValue = DateTime.Now.AddMonths(-1);
            chkTimTheoMPThu.Checked = false;
            txtMaPThu.Enabled = false;
            //Chi
            txtMaPChi.Text = "PC";
            dateToChi.EditValue = DateTime.Now;
            dateFromChi.EditValue = DateTime.Now.AddMonths(-1);
            chkTimTheoMPChi.Checked = false;
            txtMaPChi.Enabled = false;
        }
        public void KhoiTaoTablePT()
        {
            pt.Columns.Add("MaPhieu");
            pt.Columns.Add("MaNhanVien");
            pt.Columns.Add("NgayLap");
            pt.Columns.Add("SoTien");
            pt.Columns.Add("TenNguoiNop");
            pt.Columns.Add("LyDo");
            pt.PrimaryKey = new DataColumn[] { pt.Columns[0] };
        }
        public void KhoiTaoTablePC()
        {
            pc.Columns.Add("MaPhieu");
            pc.Columns.Add("MaNhanVien");
            pc.Columns.Add("NgayLap");
            pc.Columns.Add("SoTien");
            pc.Columns.Add("TenNguoiNhan");
            pc.Columns.Add("LyDo");
            pc.PrimaryKey = new DataColumn[] { pc.Columns[0] };
        }

        private void frmQuanLiThuChi_Load(object sender, EventArgs e)
        {
            KhoiTaoTablePC();
            KhoiTaoTablePT();
            //gridPThu.DataSource = pt;
            //gridPChi.DataSource = pc;
            gridPThu.DataSource = BUS.PhieuThu.Table;
            gridPChi.DataSource = BUS.PhieuChi.Table;
            labStatePT.Text = BUS.PhieuThu.Table.Rows.Count.ToString() + " phiếu thu trong danh sách";
            labStatePC.Text = BUS.PhieuChi.Table.Rows.Count.ToString() + " phiếu chi trong danh sách";

            NapGiaoDien();
        }
        private void btnTimPThu_Click(object sender, EventArgs e)
        {
            if (chkTimTheoMPThu.Checked)
            {
                pt.Rows.Clear();
                foreach (DataRow _r in BUS.PhieuThu.Table.Rows)
                {
                    if (_r["MaPhieu"].ToString() == txtMaPThu.Text)
                    {
                        DataRow r = pt.NewRow();
                        r["MaPhieu"] = _r["MaPhieu"];
                        r["MaNhanVien"] = _r["MaNhanVien"];
                        r["NgayLap"] = _r["NgayLap"];
                        r["TenNguoiNop"] = _r["TenNguoiNop"];
                        r["SoTien"] = _r["SoTien"];
                        r["LyDo"] = _r["LyDo"];
                        pt.Rows.Add(r);
                    }
                    gridPThu.DataSource = pt;
                }
            }
            else
            {
                pt.Rows.Clear();
                DateTime dateFrom = dateFromThu.DateTime;
                DateTime dateTo = dateToThu.DateTime;
                foreach (DataRow _r in BUS.PhieuThu.Table.Rows)
                {
                    DateTime date = DateTime.Parse(_r["NgayLap"].ToString());
                    if (DateTime.Compare(date, dateFrom) >= 0 && DateTime.Compare(date, dateTo) <= 0)
                    {
                        DataRow r = pt.NewRow();
                        r["MaPhieu"] = _r["MaPhieu"];
                        r["MaNhanVien"] = _r["MaNhanVien"];
                        r["NgayLap"] = _r["NgayLap"];
                        r["TenNguoiNop"] = _r["TenNguoiNop"];
                        r["SoTien"] = _r["SoTien"];
                        r["LyDo"] = _r["LyDo"];
                        pt.Rows.Add(r);
                    }
                    gridPThu.DataSource = pt;
                }
            }
        }

        private void chkTimTheoMPThu_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTimTheoMPThu.Checked)
                txtMaPThu.Enabled = true;
            else
                txtMaPThu.Enabled = false;
        }

        private void chkTimTheoMPChi_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTimTheoMPChi.Checked)
                txtMaPChi.Enabled = true;
            else
                txtMaPChi.Enabled = false;
        }
        
        private void btnTimPChi_Click(object sender, EventArgs e)
        {
            if (chkTimTheoMPChi.Checked)
            {
                pc.Rows.Clear();
                foreach (DataRow _r in BUS.PhieuChi.Table.Rows)
                {
                    if (_r["MaPhieu"].ToString() == txtMaPChi.Text)
                    {
                        DataRow r = pc.NewRow();
                        r["MaPhieu"] = _r["MaPhieu"];
                        r["MaNhanVien"] = _r["MaNhanVien"];
                        r["NgayLap"] = _r["NgayLap"];
                        r["TenNguoiNhan"] = _r["TenNguoiNhan"];
                        r["SoTien"] = _r["SoTien"];
                        r["LyDo"] = _r["LyDo"];
                        pc.Rows.Add(r);
                    }
                    gridPChi.DataSource = pc;
                }
            }
            else
            {
                pc.Rows.Clear();
                DateTime dateFrom = dateFromChi.DateTime;
                DateTime dateTo = dateToChi.DateTime;
                foreach (DataRow _r in BUS.PhieuChi.Table.Rows)
                {
                    DateTime date = DateTime.Parse(_r["NgayLap"].ToString());
                    if (DateTime.Compare(date, dateFrom) >= 0 && DateTime.Compare(date, dateTo) <= 0)
                    {
                        DataRow r = pc.NewRow();
                        r["MaPhieu"] = _r["MaPhieu"];
                        r["MaNhanVien"] = _r["MaNhanVien"];
                        r["NgayLap"] = _r["NgayLap"];
                        r["TenNguoiNhan"] = _r["TenNguoiNhan"];
                        r["SoTien"] = _r["SoTien"];
                        r["LyDo"] = _r["LyDo"];
                        pc.Rows.Add(r);
                    }
                    gridPChi.DataSource = pc;
                }
            }
        }

        private void btnShowPT_Click(object sender, EventArgs e)
        {
            gridPThu.DataSource = BUS.PhieuThu.Table;
        }

        private void btnShowPC_Click(object sender, EventArgs e)
        {
            gridPChi.DataSource = BUS.PhieuChi.Table;
        }

        private void btnThemPThu_Click(object sender, EventArgs e)
        {
            frmThemPhieuThu f2 = new frmThemPhieuThu(BUS.PhieuThu);
            f2.Show();
            labStatePT.Text = BUS.PhieuThu.Table.Rows.Count.ToString() + " phiếu thu trong danh sách";
        }

        private void btnSuaPThu_Click(object sender, EventArgs e)
        {
            string maPhieu = gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[0]).ToString();
            string maNhanVien = gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[1]).ToString();
            DateTime ngayLap = DateTime.Parse(gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[2]).ToString());
            string nguoiNop = gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[3]).ToString();
            Double soTien = Double.Parse(gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[4]).ToString());
            string lyDo = gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[5]).ToString();

            DataTransfer.PhieuThu pt = new DataTransfer.PhieuThu(maPhieu, maNhanVien, ngayLap, nguoiNop, soTien, lyDo);
            frmSuaPhieuThu f = new frmSuaPhieuThu(BUS.PhieuThu, pt);
            f.Show();
        }

        private void btnXoaPThu_Click(object sender, EventArgs e)
        {
            string maPhieu = gridViewPThu.GetRowCellValue(gridViewPThu.FocusedRowHandle, gridViewPThu.Columns[0]).ToString();
            DialogResult dlr = XtraMessageBox.Show("Bạn có chắc chắn muốn xóa phiếu thu " + maPhieu + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(dlr == DialogResult.Yes)
            {
                if (BUS.PhieuThu.Xoa(maPhieu))
                {
                    XtraMessageBox.Show("Phiếu thu " + maPhieu + " đã được xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    labStatePT.Text = BUS.PhieuThu.Table.Rows.Count.ToString() + " phiếu thu trong danh sách";
                }
                else
                {
                    XtraMessageBox.Show("Có lỗi xảy ra", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }            
        }

        private void btnThemPChi_Click(object sender, EventArgs e)
        {
            frmThemPhieuChi f = new frmThemPhieuChi(BUS.PhieuChi);
            f.Show();
            labStatePC.Text = BUS.PhieuChi.Table.Rows.Count.ToString() + " phiếu chi trong danh sách";
        }

        private void btnSuaPChi_Click(object sender, EventArgs e)
        {
            string maPhieu = gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[0]).ToString();
            string maNhanVien = gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[1]).ToString();
            DateTime ngayLap = DateTime.Parse(gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[2]).ToString());
            string nguoiNop = gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[3]).ToString();
            Double soTien = Double.Parse(gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[4]).ToString());
            string lyDo = gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[5]).ToString();

            DataTransfer.PhieuChi pt = new DataTransfer.PhieuChi(maPhieu, maNhanVien, ngayLap, nguoiNop, soTien, lyDo);
            frmSuaPhieuChi f = new frmSuaPhieuChi(BUS.PhieuChi, pt);
            f.Show();
        }

        private void btnXoaPChi_Click(object sender, EventArgs e)
        {
            string maPhieu = gridViewPChi.GetRowCellValue(gridViewPChi.FocusedRowHandle, gridViewPChi.Columns[0]).ToString();
            DialogResult dlr = XtraMessageBox.Show("Bạn có chắc chắn muốn xóa phiếu chi " + maPhieu + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dlr == DialogResult.Yes)
            {
                if (BUS.PhieuChi.Xoa(maPhieu))
                {
                    XtraMessageBox.Show("Phiếu chi " + maPhieu + " đã được xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    labStatePC.Text = BUS.PhieuChi.Table.Rows.Count.ToString() + " phiếu chi trong danh sách";
                }
                else
                {
                    XtraMessageBox.Show("Có lỗi xảy ra", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}