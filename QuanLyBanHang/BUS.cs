﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuanLyBanHang
{
    /// <summary>
    /// Lớp dùng chung tất cả các đối tượng BUS
    /// </summary>
    public static class BUS
    {
        #region Fields
        private static BusinessLogic.BaoGia _baogia = null;
        private static BusinessLogic.CTBaoGia _ctBaoGia = null;
        private static BusinessLogic.CTPhieuNhap _ctPhieuNhap = null;
        private static BusinessLogic.CTPhieuXuat _ctPhieuXuat = null;
        private static BusinessLogic.CTHoaDonBanHang _ctHoaDonBanHang = null;
        private static BusinessLogic.CTHoaDonDatHang _ctHoaDonDatHang = null;
        private static BusinessLogic.DacQuyen _dacQuyen = null;
        private static BusinessLogic.HoaDonBanHang _hoaDonBanHang = null;
        private static BusinessLogic.HoaDonDatHang _hoaDonDatHang = null;
        private static BusinessLogic.KhachHang _khachHang = null;
        private static BusinessLogic.LoaiSanPham _loaiSanPham = null;
        private static BusinessLogic.NhaCungCap _nhaCungCap = null;
        private static BusinessLogic.NguoiDung _nguoiDung = null;
        private static BusinessLogic.PhieuChi _phieuChi = null;
        private static BusinessLogic.PhieuNhap _phieuNhap = null;
        private static BusinessLogic.PhieuThu _phieuThu = null;
        private static BusinessLogic.PhieuXuat _phieuXuat = null;
        private static BusinessLogic.SanPham _sanPham = null;
        private static BusinessLogic.SaoLuu _saoLuu = null;
        #endregion

        #region Properties
        public static BusinessLogic.BaoGia BaoGia
        {
            get { return _baogia; }
            set { _baogia = value; }
        }

        public static BusinessLogic.CTBaoGia CTBaoGia
        {
            get { return _ctBaoGia; }
            set { _ctBaoGia = value; }
        }

        public static BusinessLogic.CTPhieuNhap CTPhieuNhap
        {
            get { return _ctPhieuNhap; }
            set { _ctPhieuNhap = value; }
        }

        public static BusinessLogic.CTPhieuXuat CTPhieuXuat
        {
            get { return _ctPhieuXuat; }
            set { _ctPhieuXuat = value; }
        }

        public static BusinessLogic.CTHoaDonBanHang CTHoaDonBanHang
        {
            get { return _ctHoaDonBanHang; }
            set { _ctHoaDonBanHang = value; }
        }

        public static BusinessLogic.CTHoaDonDatHang CTHoaDonDatHang
        {
            get { return _ctHoaDonDatHang; }
            set { _ctHoaDonDatHang = value; }
        }

        public static BusinessLogic.DacQuyen DacQuyen
        {
            get { return _dacQuyen; }
            set { _dacQuyen = value; }
        }

        public static BusinessLogic.HoaDonBanHang HoaDonBanHang
        {
            get { return _hoaDonBanHang; }
            set { _hoaDonBanHang = value; }
        }

        public static BusinessLogic.HoaDonDatHang HoaDonDatHang
        {
            get { return _hoaDonDatHang; }
            set { _hoaDonDatHang = value; }
        }

        public static BusinessLogic.KhachHang KhachHang
        {
            get { return _khachHang; }
            set { _khachHang = value; }
        }

        public static BusinessLogic.LoaiSanPham LoaiSanPham
        {
            get { return _loaiSanPham; }
            set { _loaiSanPham = value; }
        }

        public static BusinessLogic.NguoiDung NguoiDung
        {
            get { return _nguoiDung; }
            set { _nguoiDung = value; }
        }

        public static BusinessLogic.NhaCungCap NhaCungCap
        {
            get { return _nhaCungCap; }
            set { _nhaCungCap = value; }
        }

        public static BusinessLogic.PhieuChi PhieuChi
        {
            get { return _phieuChi; }
            set { _phieuChi = value; }
        }

        public static BusinessLogic.PhieuNhap PhieuNhap
        {
            get { return _phieuNhap; }
            set { _phieuNhap = value; }
        }

        public static BusinessLogic.PhieuThu PhieuThu
        {
            get { return _phieuThu; }
            set { _phieuThu = value; }
        }

        public static BusinessLogic.PhieuXuat PhieuXuat
        {
            get { return _phieuXuat; }
            set { _phieuXuat = value; }
        }

        public static BusinessLogic.SanPham SanPham
        {
            get { return _sanPham; }
            set { _sanPham = value; }
        }

        public static BusinessLogic.SaoLuu SaoLuu
        {
            get { return _saoLuu; }
            set { _saoLuu = value; }
        }
        #endregion
    }
}
