﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;

namespace QuanLyBanHang
{
    public partial class frmLapHoaDonBanHang : DevExpress.XtraEditors.XtraForm
    {
        private DataTransfer.HoaDonBanHang hdbh;
        DataTable tbCTHD = new DataTable();
        private bool isInsert;

        public frmLapHoaDonBanHang(DataTransfer.HoaDonBanHang hdbh = null)
        {
            InitializeComponent();

            this.hdbh = hdbh;

            isInsert = hdbh == null;

            KhoiTaoCacBUSCanThiet();
        }

        /// <summary>
        /// Nạp giao diện mặc định hoặc nạp hd lên form
        /// </summary>
        /// <param name="hd">Đối tượng hd để xác định là sửa hay thêm mới</param>
        public void NapGiaoDien(DataTransfer.HoaDonBanHang hd = null)
        {
            if (hd == null)
            {
                hdbh = new DataTransfer.HoaDonBanHang(BUS.HoaDonBanHang.AutoGenerateID(), string.Empty, string.Empty,
                    DateTime.Now, 0, 0, false, string.Empty);
                NapGiaoDien(this.hdbh);
            }
            else
            {
                //load combobox Khách hàng
                cboKhachHang.Properties.Columns.Add(new LookUpColumnInfo("HoTen"));
                cboKhachHang.Properties.DataSource = BUS.KhachHang.Table;
                cboKhachHang.Properties.DisplayMember = "HoTen";
                cboKhachHang.Properties.ValueMember = "MaKhachHang";

                //load hóa đơn
                txtMaHoaDon.Text = hd.MaHoaDon;
                dateNgayLap.EditValue = hd.NgayLap;
                cboKhachHang.EditValue = hd.MaKhachHang;
                txtGhiChu.Text = hd.GhiChu;
                txtTongTien.Value = (decimal)hd.TongTien;
                txtThanhTien.Value = (decimal)hd.ThanhTien;
                chkDaThu.Checked = hd.DaThu;

                //load chi tiết hóa đơn của hóa đơn
                tbCTHD.Rows.Clear();
                DataRow[] rows = BUS.CTHoaDonBanHang.Table.Select("MaHoaDon = '" + hd.MaHoaDon + "'");
                foreach (DataRow item in rows)
                {
                    DataRow r = tbCTHD.NewRow();
                    DataRow _r = BUS.SanPham.Table.Select("MaSanPham = '" + item["MaSanPham"] + "'")[0];
                    r["MaSanPham"] = item["MaSanPham"];
                    r["TenSanPham"] = _r["TenSanPham"];
                    r["DonVi"] = _r["DonVi"];
                    r["SoLuong"] = item["SoLuong"];
                    r["ChietKhau"] = _r["ChietKhau"];
                    r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                    tbCTHD.Rows.Add(r);
                }
            }

            lblTongSanPham.Text = "Tổng cộng: " + BUS.SanPham.Table.Rows.Count + " sản phẩm.";
            lblTongChonSanPham.Text = "Tổng cộng: " + tbCTHD.Rows.Count + " sản phẩm.";

        }

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
            if (BUS.KhachHang == null)
                BUS.KhachHang = new BusinessLogic.KhachHang();
            if (BUS.HoaDonBanHang == null)
                BUS.HoaDonBanHang = new BusinessLogic.HoaDonBanHang();
            if (BUS.CTHoaDonBanHang == null)
                BUS.CTHoaDonBanHang = new BusinessLogic.CTHoaDonBanHang();
        }
        
        /// <summary>
        /// Khởi tạo bảng cho tbCTHD
        /// </summary>
        public void KhoiTaoTableCTHD()
        {
            tbCTHD.Columns.Add("MaSanPham");
            tbCTHD.Columns.Add("TenSanPham");
            tbCTHD.Columns.Add("DonVi");
            tbCTHD.Columns.Add("SoLuong");
            tbCTHD.Columns.Add("ThanhTien");
            tbCTHD.Columns.Add("ChietKhau");
            tbCTHD.PrimaryKey = new DataColumn[] { tbCTHD.Columns[0] };
            tbCTHD.RowChanged += TbCTHD_RowChanged;
            tbCTHD.RowDeleted += TbCTHD_RowChanged;
            tbCTHD.TableCleared += TbCTHD_TableCleared;
        }

        private void TbCTHD_TableCleared(object sender, DataTableClearEventArgs e)
        {
            lblTongChonSanPham.Text = "Tổng cộng: " + tbCTHD.Rows.Count + " sản phẩm.";
        }

        private void TbCTHD_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            lblTongChonSanPham.Text = "Tổng cộng: " + tbCTHD.Rows.Count + " sản phẩm.";
        }

        /// <summary>
        /// Cập nhật số tiền của hóa đơn
        /// </summary>
        public void CapNhatSoTienHoaDon()
        {
            long tongTien = 0;
            long thanhTien = 0;
            foreach (DataRow item in tbCTHD.Rows)
            {
                tongTien += Convert.ToInt64(item["ThanhTien"]);
                thanhTien += Convert.ToInt64(item["ThanhTien"]) * ( 100 - Convert.ToInt32(item["ChietKhau"])) / 100; 
            }
            txtTongTien.Value = tongTien;
            txtThanhTien.Value = thanhTien;
        }

        /// <summary>
        /// Nạp từ form xuống đối tượng dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTransfer.HoaDonBanHang NapHoaDon()
        {
            hdbh.MaHoaDon = txtMaHoaDon.Text;
            hdbh.MaKhachHang = cboKhachHang.EditValue.ToString();
            hdbh.MaNhanVien = "ND000001";
            hdbh.NgayLap = dateNgayLap.DateTime;
            hdbh.TongTien = (double)txtTongTien.Value;
            hdbh.ThanhTien = (double)txtThanhTien.Value;
            hdbh.DaThu = chkDaThu.Checked;

            if (string.IsNullOrEmpty(hdbh.MaKhachHang))
                throw new ArgumentException("Vui lòng nhập đầy đủ thông tin.");
            return hdbh;
        }

        private void frmLapHoaDonBanHang_Load(object sender, EventArgs e)
        { 
            KhoiTaoTableCTHD();
            gridChiTietSanPham.DataSource = tbCTHD;
            gridChonSanPham.DataSource = BUS.SanPham.Table;

            NapGiaoDien(hdbh);
        }

        private void gridChonSanPham_Click(object sender, EventArgs e)
        {
            txtChonSanPham.Text = gridViewChonSP.GetRowCellValue(gridViewChonSP.FocusedRowHandle, gridViewChonSP.Columns[0]).ToString();
        }

        private void btnChonSanPham_Click(object sender, EventArgs e)
        {
            string nameItem = "";
            try
            {
                DataRow r = tbCTHD.NewRow();
                DataRow _r = gridViewChonSP.GetDataRow(gridViewChonSP.FocusedRowHandle);
                nameItem = Convert.ToString(_r["TenSanPham"]);
                DataRow _r1 = BUS.SanPham.Table.Select("MaSanPham = '" + Convert.ToString(_r["MaSanPham"]) + "'")[0];
                if (Convert.ToInt32(_r1["TonKho"]) == 0)
                    throw new ArgumentException("OutNumber");
                r["MaSanPham"] = _r["MaSanPham"];
                r["TenSanPham"] = _r["TenSanPham"];
                r["DonVi"] = _r["DonVi"];
                r["SoLuong"] = 1;
                r["ChietKhau"] = _r1["ChietKhau"];
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                tbCTHD.Rows.Add(r);
                gridViewCTSP.SelectRow(gridViewCTSP.FindRow(r));
                gridViewCTSP.Focus();
                CapNhatSoTienHoaDon();
            }
            catch (ConstraintException)
            {
                XtraMessageBox.Show("Bạn đã chọn sản phẩm " + txtChonSanPham.Text + " rồi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException)
            {
                XtraMessageBox.Show("Sản phẩm " + nameItem + " tạm hết hàng !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                XtraMessageBox.Show("Vui lòng chọn một sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridChonSanPham_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSP.GetRowCellValue(gridViewChonSP.FocusedRowHandle, gridViewChonSP.Columns[0]).ToString();

                btnChonSanPham_Click(sender, e);
            }
            catch { }
        }

        private void btnSLTang1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                if (Convert.ToInt32(r["SoLuong"]) != Convert.ToInt32(_r["TonKho"]))
                {
                    r["SoLuong"] = Convert.ToInt32(r["SoLuong"]) + 1;
                    r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                    CapNhatSoTienHoaDon();
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            catch (IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Số lượng sản phẩm xuất không được lớn hơn số sản phẩm tồn kho ");
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSLGiam1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                int sl = Convert.ToInt32(r["SoLuong"]);
                r["SoLuong"] = sl > 1 ? sl - 1 : sl;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                CapNhatSoTienHoaDon();
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                tbCTHD.Rows.Remove(gridViewCTSP.GetDataRow(gridViewCTSP.FocusedRowHandle));
            }
            catch
            {
                XtraMessageBox.Show("Không có gì để xóa!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoaTatCa_Click(object sender, EventArgs e)
        {
            tbCTHD.Rows.Clear();
        }

        private void btnTaoHoaDon_Click(object sender, EventArgs e)
        {
            if (tbCTHD.Rows.Count > 0)
            {
                try
                {
                    //thêm hóa đơn chính
                    this.hdbh = NapHoaDon();
                    if (isInsert)
                    {
                        BUS.HoaDonBanHang.Them(hdbh);
                        
                    }
                    else
                    {
                        BUS.HoaDonBanHang.Sua(hdbh);

                        //xóa các sản phẩm của hóa đơn
                        DataTransfer.CTHoaDonBanHang _ct;
                        DataRow[] rows = BUS.CTHoaDonBanHang.Table.Select("MaHoaDon = '" + hdbh.MaHoaDon + "'");
                        foreach (DataRow item in rows)
                        {
                            _ct = new DataTransfer.CTHoaDonBanHang(item["MaHoaDon"].ToString(), item["MaSanPham"].ToString(), Convert.ToInt16(item["SoLuong"]));
                            BUS.CTHoaDonBanHang.Xoa(_ct.MaHoaDon, _ct.MaSanPham, true);
                        }
                    }

                    //thêm sản phẩm của hóa đơn
                    DataTransfer.CTHoaDonBanHang ct;
                    foreach (DataRow r in tbCTHD.Rows)
                    {
                        ct = new DataTransfer.CTHoaDonBanHang(txtMaHoaDon.Text, r["MaSanPham"].ToString(), Convert.ToInt32(r["SoLuong"]));
                        BUS.CTHoaDonBanHang.Them(ct);
                    }

                    XtraMessageBox.Show("Hóa đơn đã được lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if (isInsert)
                        NapGiaoDien();
                    else
                        Close();
                }
                catch (ArgumentException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                XtraMessageBox.Show("Phải chọn ít nhất một sản phẩm để tạo hóa đơn", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);          
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (isInsert)
                NapGiaoDien();
            else
                Close();
        }

        private void btnThemKhachHang_Click(object sender, EventArgs e)
        {
            Form khEdit = new frmKhachHangEdit();
            khEdit.Text = "Thêm khách hàng";
            khEdit.ShowDialog();
        }
    }
}