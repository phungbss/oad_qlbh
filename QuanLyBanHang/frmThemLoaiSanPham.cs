﻿using System;
using System.Data;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemLoaiSanPham : DevExpress.XtraEditors.XtraForm
    {
        public frmThemLoaiSanPham()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }

        public bool IsInsert = false;
        public EventHandler LamMoi;
        public string IDLoai;

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.LoaiSanPham == null)
                BUS.LoaiSanPham = new BusinessLogic.LoaiSanPham();
        }

        /// <summary>
        /// Lưu sự thay đổi của các phương thức
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                DataTransfer.LoaiSanPham _lsp = new DataTransfer.LoaiSanPham(txtMaLoai.Text, txtTenLoai.Text, txtGhiChu.Text);
                if (IsInsert)  // Phương thức thêm mới 
                {
                    if (txtMaLoai.Text.Length == 0 || txtTenLoai.Text.Length == 0) // ném lỗi khi chưa chọn loại sản phẩm
                        throw new IndexOutOfRangeException();
                    if (BUS.LoaiSanPham.Them(_lsp))
                        XtraMessageBox.Show("Thêm thành công!");
                    LamMoi?.Invoke(sender, e); // Load lại dữ liệu from frmSanPham
                    Close();
                }
                else  // Phương thức sửa 
                {
                    if (txtTenLoai.Text.Length == 0) 
                        throw new IndexOutOfRangeException();
                    if (BUS.LoaiSanPham.Sua(_lsp))
                        XtraMessageBox.Show("Sửa thành công!");
                    LamMoi?.Invoke(sender, e);
                    this.Close();
                }
            }
            catch (IndexOutOfRangeException) // bắt ngoại lệ
            {
                XtraMessageBox.Show("Nhập Thông tin đầy đủ dòng in đậm !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Đóng form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHuy_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Nạp dữ liệu khi load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmThemLoaiSanPham_Load(object sender, EventArgs e)
        {          
            if (IsInsert) // Phương thức thêm mới loại sản phẩm
            {
                Text = "Thêm Loại Sản Phẩm";
                btnLuu.Text = "Thêm";
                txtMaLoai.Text = BUS.LoaiSanPham.AutoGenerateID(); 
                txtMaLoai.ReadOnly = true;
            }
            else // Phương thức sửa loại sản phẩm
            {
                Text = "Sửa Loại Sản Phẩm";
                btnLuu.Text = "Sửa";
                txtMaLoai.ReadOnly = true;
                DataTable dt = BUS.LoaiSanPham.Table;
                DataRow r = dt.Rows.Find(IDLoai);
                txtMaLoai.Text = IDLoai;
                txtTenLoai.Text = r["TenLoai"].ToString();
                txtGhiChu.Text = r["GhiChu"].ToString();
            }
        }
    }
}