﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

namespace QuanLyBanHang
{
    public partial class frmLapPhieuNhap : DevExpress.XtraEditors.XtraForm
    {
        //BusinessLogic.SanPham sp_bus;
        //BusinessLogic.NguoiDung nd_bus;
        //BusinessLogic.NhaCungCap ncc_bus;
        //BusinessLogic.PhieuNhap pn_bus;
        //BusinessLogic.CTPhieuNhap ctpn_bus;
        DataTable tbCTPN = new DataTable();

        public frmLapPhieuNhap()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
            KhoiTaoTableCTPN();
        }
        /// <summary>
        /// Nạp giao diện
        /// </summary>
        public void NapGiaoDien()
        {
            txtMaPhieu.Text = BUS.PhieuNhap.AutoGenerateID();
            cboMaNhaCungCap.Text = string.Empty;
            //cboMaNhanVien.Text = string.Empty;
            dateNgayLap.EditValue = DateTime.Now;
            txtGhiChu.Text = string.Empty;
            labTongTien.Text = "đ";
            txtChonSanPham.Text = string.Empty;
            tbCTPN.Rows.Clear();

            //load combobox Mã nhà cung cấp
            RepositoryItemComboBox properties_ncc = cboMaNhaCungCap.Properties;
            properties_ncc.Items.Clear();
            foreach (DataRow item in BUS.NhaCungCap.Table.Rows)
            {
                properties_ncc.Items.Add(item["TenNhaCungCap"].ToString() + " [" + item["MaNhaCungCap"].ToString() + "]");
            }
            //load combobox loại phiếu
            RepositoryItemComboBox properties_lp = cboLoaiPhieu.Properties;
            properties_lp.Items.Clear();
            properties_lp.Items.Add("Nhập hàng mới");
            properties_lp.Items.Add("Nhập điều chỉnh kho");
        }
        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.NguoiDung == null)
                BUS.NguoiDung = new BusinessLogic.NguoiDung();
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
            if (BUS.NhaCungCap == null)
                BUS.NhaCungCap = new BusinessLogic.NhaCungCap();
            if (BUS.PhieuNhap == null)
                BUS.PhieuNhap = new BusinessLogic.PhieuNhap();
            if (BUS.CTPhieuNhap == null)
                BUS.CTPhieuNhap = new BusinessLogic.CTPhieuNhap();

        }
        /// <summary>
        /// Khởi tạo bảng cho tbCTPN
        /// </summary>
        public void KhoiTaoTableCTPN()
        {
            tbCTPN.Columns.Add("MaSanPham");
            tbCTPN.Columns.Add("TenSanPham");
            tbCTPN.Columns.Add("SoLuong");
            tbCTPN.Columns.Add("DonVi");
            tbCTPN.Columns.Add("ThanhTien");
            tbCTPN.Columns.Add("GhiChu");
            tbCTPN.PrimaryKey = new DataColumn[] { tbCTPN.Columns[0] };
        }
        /// <summary>
        /// Cập nhật tổng tiền
        /// </summary>
        public void CapNhatSoTien()
        {
            long tongTien = 0;
            foreach (DataRow item in tbCTPN.Rows)
            {
                tongTien += Convert.ToInt32(item["ThanhTien"]);
            }
            labTongTien.Text = tongTien.ToString() + "đ";
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            NapGiaoDien();
            labStateSPDaChon.Text = "Chưa có sản phẩm nào được chọn";
        }

        private void frmLapPhieuNhap_Load(object sender, EventArgs e)
        {
           // KhoiTaoTableCTPN();
            gridChiTietSanPham.DataSource = tbCTPN;
            gridChonSanPham.DataSource = BUS.SanPham.Table;
            labStateChonSP.Text = BUS.SanPham.Table.Rows.Count.ToString() + " sản phẩm";
            NapGiaoDien();
        }

        private void gridChonSanPham_Click(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSanPham.GetRowCellValue(gridViewChonSanPham.FocusedRowHandle, gridViewChonSanPham.Columns[0]).ToString();
            }
            catch { }
        }

        private void btnChonSanPham_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = tbCTPN.NewRow();
                DataRow _r = gridViewChonSanPham.GetDataRow(gridViewChonSanPham.FocusedRowHandle);
                r["MaSanPham"] = _r["MaSanPham"];
                r["TenSanPham"] = _r["TenSanPham"];
                r["DonVi"] = _r["DonVi"];
                r["SoLuong"] = 1;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                tbCTPN.Rows.Add(r);
                gridViewCTSanPham.SelectRow(gridViewCTSanPham.FindRow(r));
                gridViewCTSanPham.Focus();
                labStateSPDaChon.Text = tbCTPN.Rows.Count.ToString() + " sản phẩm trong danh sách";
                CapNhatSoTien();
            }
            catch (ConstraintException)
            {
                XtraMessageBox.Show("Bạn đã chọn sản phẩm " + txtChonSanPham.Text + " rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridChonSanPham_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtChonSanPham.Text = gridViewChonSanPham.GetRowCellValue(gridViewChonSanPham.FocusedRowHandle, gridViewChonSanPham.Columns[0]).ToString();
                btnChonSanPham_Click(sender, e);
            }
            catch { }
        }

        private void btcCongMot_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSanPham.GetDataRow(gridViewCTSanPham.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                r["SoLuong"] = Convert.ToInt32(r["SoLuong"]) + 1;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);
                CapNhatSoTien();
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnTruMot_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow r = gridViewCTSanPham.GetDataRow(gridViewCTSanPham.FocusedRowHandle);
                DataRow _r = BUS.SanPham.Table.Rows.Find(r["MaSanPham"]);

                int sl = Convert.ToInt32(r["SoLuong"]);
                r["SoLuong"] = sl > 1 ? sl - 1 : sl;
                r["ThanhTien"] = Convert.ToInt32(r["SoLuong"]) * Convert.ToInt32(_r["GiaBan"]);

                CapNhatSoTien();
            }
            catch
            {
                XtraMessageBox.Show("Thay đổi số lượng không hợp lệ!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                tbCTPN.Rows.Remove(gridViewCTSanPham.GetDataRow(gridViewCTSanPham.FocusedRowHandle));
                CapNhatSoTien();
            }
            catch
            {
                XtraMessageBox.Show("Không thể xóa khi danh sách trống!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoaTatCa_Click(object sender, EventArgs e)
        {
            tbCTPN.Rows.Clear();
            labTongTien.Text = "0đ";
        }

        private void btnTaoPhieu_Click(object sender, EventArgs e)
        {
            if (tbCTPN.Rows.Count > 0)
            {
                try
                {
                    //thêm phiếu nhập
                    DataTransfer.PhieuNhap pn = new DataTransfer.PhieuNhap(txtMaPhieu.Text,
                        "ND000000", cboMaNhaCungCap.Text.Substring(cboMaNhaCungCap.Text.IndexOf('[') + 1, 8),
                        dateNgayLap.DateTime, "Nhập hàng",
                        Convert.ToDouble(labTongTien.Text.Substring(0, labTongTien.Text.Length - 1)),
                        txtGhiChu.Text);
                    if (BUS.PhieuNhap.Them(pn))
                    {
                        XtraMessageBox.Show("Phiếu nhập " + txtMaPhieu.Text + " đã được thêm thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    //thêm chi tiết phiếu
                    DataTransfer.CTPhieuNhap ct;
                    foreach (DataRow r in tbCTPN.Rows)
                    {
                        ct = new DataTransfer.CTPhieuNhap(txtMaPhieu.Text, r["MaSanPham"].ToString(), Convert.ToInt32(r["SoLuong"]), "");
                        BUS.CTPhieuNhap.Them(ct);
                    }                  
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                XtraMessageBox.Show("Chưa có sản phẩm nào trong Phiếu Nhập", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Form khEdit = new frmNhaCungCapEdit();
            khEdit.Text = "Thêm NCC";
            khEdit.ShowDialog();
        }
    }
}