﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmThemPhieuThu : DevExpress.XtraEditors.XtraForm
    {
        public BusinessLogic.PhieuThu pt_bus;
        public frmThemPhieuThu(BusinessLogic.PhieuThu pt = null)
        {
            pt_bus = pt;
            if (pt_bus == null)
                pt_bus = new BusinessLogic.PhieuThu();
            InitializeComponent();
        }
        /// <summary>
        /// Nạp Giao diện mặc định
        /// </summary>
        public void NapGiaoDien()
        {
            txtMaPhieu.Text = pt_bus.AutoGenerateID();
            dateNgayLap.EditValue = DateTime.Now;
            txtLyDo.Text = txtSoTien.Text = txtNguoiNop.Text = string.Empty;
        }
        /// <summary>
        /// Giới hạn kí tự nhập(chỉ nhập số)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSoTien_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsDigit(e.KeyChar)&&!Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void frmThemPhieuThu_Load(object sender, EventArgs e)
        {
           
            NapGiaoDien();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(txtNguoiNop.Text != string.Empty && txtSoTien.Text != string.Empty)
            {
                try
                {
                    DataTransfer.PhieuThu pt = new DataTransfer.PhieuThu(txtMaPhieu.Text,
                        "ND000002", dateNgayLap.DateTime, txtNguoiNop.Text,
                        double.Parse(txtSoTien.Text), txtLyDo.Text);
                    if(pt_bus.Them(pt))
                    {
                        XtraMessageBox.Show("Phiếu thu đã được thêm thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    NapGiaoDien();
                }
                catch
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập đầy đủ các thông tin", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void kkk(object sender, EventArgs e)
        {

        }
    }
}