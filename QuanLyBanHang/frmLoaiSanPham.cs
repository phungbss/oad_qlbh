﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmLoaiSanPham : XtraForm
    {
        public frmLoaiSanPham(DataTransfer.NguoiDung currUser)
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
            currentUser = currUser;
        }

        private bool IsInsert = false;
        private bool checkGrid_Click = false;
        private DataTransfer.NguoiDung currentUser;

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.LoaiSanPham == null)
                BUS.LoaiSanPham = new BusinessLogic.LoaiSanPham();
        }

        public void NapGiaoDienPhanQuyen(DataTransfer.NguoiDung nd)
        {
            //quyền Khách
            if (nd == null)
            {
                btnThem.Enabled = false;
                btnSua.Enabled = false;
                btnXoa.Enabled = false;
            }
        }

        void LockControl() // Khoa cac control
        {
            txtMaLoai.ReadOnly = true;
            txtTenLoai.ReadOnly = true;
            txtGhiChu.ReadOnly = true;

            btnThem.Enabled = true;
            btnLuu.Enabled = false;
            btnXoa.Enabled = true;
            btnSua.Enabled = true;
        }

        void UnLockControl() // Mo khoa control
        {
            txtMaLoai.ReadOnly = true;
            txtTenLoai.ReadOnly = false;
            txtGhiChu.ReadOnly = false;

            btnThem.Enabled = false;
            btnLuu.Enabled = true;
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
        }

        void DeleteText() // Xoa dong text
        {
            txtMaLoai.Text = string.Empty;
            txtTenLoai.Text = string.Empty;
            txtGhiChu.Text = string.Empty;
        }


        void ShowScreen() // refresh lai man hinh
        {
            GridLoaiSanPham.DataSource = BUS.LoaiSanPham.Table;
            lbSumLoaisp.Text = BUS.LoaiSanPham.Table.Rows.Count.ToString() + " Loại";
        }

        private void frmLoaiSanPham_Load(object sender, EventArgs e)
        {
            LockControl();
            ShowScreen();
            NapGiaoDienPhanQuyen(currentUser);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            UnLockControl();
            DeleteText();
            IsInsert = true;
            txtMaLoai.Text = BUS.LoaiSanPham.AutoGenerateID();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkGrid_Click)
                {
                    UnLockControl();
                    txtMaLoai.Enabled = false;
                    IsInsert = false;
                }
                else
                    throw new IndexOutOfRangeException();
            }
            catch (IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Chọn loại sản phẩm muốn sửa !");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {              
                if (XtraMessageBox.Show("Bạn có muốn xóa loại sản phẩm " + gridViewLsp.GetRowCellValue(gridViewLsp.FocusedRowHandle, gridViewLsp.Columns[1]).ToString() + "?" +
                    "\nThao tác này sẽ xóa các sản phẩm , hóa đơn liên quan đến loại sản phẩm . "
                    , "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string ID = gridViewLsp.GetRowCellValue(gridViewLsp.FocusedRowHandle, gridViewLsp.Columns[0]).ToString();

                    DataRow[] Sp = BUS.SanPham.Table.Select("MaLoai like '" + ID + "'");
                    foreach (DataRow item in Sp)
                        BUS.SanPham.Xoa(item["MaSanPham"].ToString());

                    if (BUS.LoaiSanPham.Xoa(ID))
                        XtraMessageBox.Show("Đã xóa thông tin thành công!");
                    checkGrid_Click = false;
                    DeleteText();
                    LockControl();
                    ShowScreen();
                }
            }
            catch
            {
                XtraMessageBox.Show("Chọn loại sản phẩm muốn xóa !");
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                DataTransfer.LoaiSanPham obj = new DataTransfer.LoaiSanPham(txtMaLoai.Text, txtTenLoai.Text, txtGhiChu.Text);
                if (IsInsert)
                {
                    if (txtMaLoai.Text.Length != 0 && txtTenLoai.Text.Length != 0)
                    {
                        // Thêm vào loại sản phẩm
                        if (BUS.LoaiSanPham.Them(obj))
                            XtraMessageBox.Show("Thêm Loại sản phẩm thành công!");
                        ShowScreen();
                        DeleteText();
                        LockControl();
                    }
                    else
                    {
                        throw new IndexOutOfRangeException();
                    }
                }
                else
                {
                    //  Cập nhật loại sản phẩm
                    if (txtTenLoai.Text.Length == 0)
                        throw new IndexOutOfRangeException();
                    if (BUS.LoaiSanPham.Sua(obj))
                        XtraMessageBox.Show("Lưu Loại sản phẩm thành công!");
                    ShowScreen();
                    DeleteText();
                    LockControl();
                }
            }
            catch (IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Điền đầy đủ thông tin vào dòng in đậm !");
            }
        }

        private void msds_Click(object sender, EventArgs e)
        {
            LockControl();
            checkGrid_Click = true;
            DataTable dt = BUS.LoaiSanPham.Table;
            try
            {
                txtMaLoai.Text = dt.Rows[gridViewLsp.FocusedRowHandle][0].ToString();
                txtTenLoai.Text = dt.Rows[gridViewLsp.FocusedRowHandle][1].ToString();
                txtGhiChu.Text = dt.Rows[gridViewLsp.FocusedRowHandle][2].ToString();
            }
            catch
            {
            }
        }
    }
}