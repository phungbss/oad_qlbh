﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuanLyBanHang.Properties;
using System.IO;

namespace QuanLyBanHang
{
    public partial class frmSaoLuu : DevExpress.XtraEditors.XtraForm
    {
        public frmSaoLuu()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }

        private DataTransfer.SaoLuu _Sl;
        private string MaSL;

        /// <summary>
        /// Hiển thị giao diện và dữ liệu lịch sử sao lưu
        /// </summary>
        private void ShowScreenSL()
        {
            GridSaoLuu.DataSource = BUS.SaoLuu.SelectAll();
            lbSumSl.Text = CountSum(BUS.SaoLuu.SelectAll()).ToString() + " Bản";
            gridViewSaoluu.ExpandAllGroups();
        }

        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.SaoLuu == null)
                BUS.SaoLuu = new BusinessLogic.SaoLuu();
        }

        /// <summary>
        /// Đếm số bản sao lưu
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private int CountSum(DataTable dt)
        {
            return dt.Rows.Count;
        }

        /// <summary>
        /// Load giao diện
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSaoLuu_Load(object sender, EventArgs e)
        {
            ShowScreenSL();
        }

        /// <summary>
        /// Lấy thông tin bản sao lưu được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void msds_Click(object sender, EventArgs e)
        {
            try
            {
                MaSL = gridViewSaoluu.GetRowCellValue(gridViewSaoluu.FocusedRowHandle, gridViewSaoluu.Columns[0]).ToString();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Xóa bản sao lưu được chọn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (MaSL == null) // Không có bẳn sao nào được chọn
                    throw new Exception();

                if (XtraMessageBox.Show("Bạn có muốn xóa bản sao lưu " + MaSL + " ?\nBản sao lưu sẽ được xóa vĩnh viễn khỏi máy tính. " ,
                    "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string Duongdan = gridViewSaoluu.GetRowCellValue(gridViewSaoluu.FocusedRowHandle, gridViewSaoluu.Columns[1]).ToString();
                    DateTime Thoigian = Convert.ToDateTime(gridViewSaoluu.GetRowCellValue(gridViewSaoluu.FocusedRowHandle, gridViewSaoluu.Columns[2]));

                    DeleteFile(Get_pathfile(Duongdan, Thoigian));
                    
                    if (BUS.SaoLuu.Xoa(MaSL))
                        XtraMessageBox.Show("Xóa thành công !");    
                                
                    ShowScreenSL();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Bạn chưa chọn bản sao lưu muốn xóa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Xóa tất cả các bản sao lưu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnXoaAll_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Sẽ không còn bản sao lưu nào khi máy bạn gặp sự cố \nBạn có chắc chắn muốn xóa chứ ?", "Thông báo", MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
                try
            {
                DataTable dt = BUS.SaoLuu.SelectAll();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    BUS.SaoLuu.Xoa(dt.Rows[i][0].ToString());
                    DeleteFile(Get_pathfile(dt.Rows[i][1].ToString(), Convert.ToDateTime(dt.Rows[i][2])));
                }
                XtraMessageBox.Show("Xóa thành công !");
                ShowScreenSL();
            }
            catch
            {
                XtraMessageBox.Show("Xóa không thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Lấy đường dẫn thư mục chứa file backup do người dùng chỉ định
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpen_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FolderBrowser = new FolderBrowserDialog();
            if (FolderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtDuongdan.Text = FolderBrowser.SelectedPath;
                txtNoiDung.Text = "Sao lưu ";
            }
        }

        /// <summary>
        /// Lấy đường dẫn thư mục chứa file backup mặc định
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpen1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FolderBrowser = new FolderBrowserDialog();
            if (FolderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtDuongdan1.Text = FolderBrowser.SelectedPath; 
                Settings.Default.Backup_Path = FolderBrowser.SelectedPath;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Thực hiện hành động sao lưu 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaoLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDuongdan.Text.Length == 0) // Kiểm tra đường dẫn có tồn tại
                    throw new IndexOutOfRangeException();

                MaSL = BUS.SaoLuu.AutoGenerateID();
                _Sl = new DataTransfer.SaoLuu(MaSL, txtDuongdan.Text, DateTime.Now, txtNoiDung.Text);

                BUS.SaoLuu.Them(_Sl);

                string TenSl = "SL_"+ ConverDateTimeToString(DateTime.Now);

                if (BUS.SaoLuu.BackUp_full(txtDuongdan.Text, TenSl ))
                    XtraMessageBox.Show("Sao lưu thành công !");

                if (txtDuongdan1.Text.Length != 0) 
                    BUS.SaoLuu.BackUp_full(txtDuongdan1.Text, "Sl_full");

                ShowScreenSL();
            }
            catch (IndexOutOfRangeException)
            {
                XtraMessageBox.Show("Bạn chưa chọn thư mục lưu !", "Thông báo", MessageBoxButtons.OK , MessageBoxIcon.Warning);
            }
            catch
            {
                XtraMessageBox.Show("Lỗi hệ thống !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Lưu các thông số thay đổi trên thanh check edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (ckeBackupwhenstart.Checked)
                Properties.Settings.Default.Backup_CheckStart = true;
            else
                Properties.Settings.Default.Backup_CheckStart = false;

            if (ckeBackupwhenexit.Checked)
                Properties.Settings.Default.Backup_CheckExit = true;
            else
                Properties.Settings.Default.Backup_CheckExit = false;

            if (ckeXoaDinhKy.Checked)
                Properties.Settings.Default.Backup_XoaDinhKy = true;
            else
                Properties.Settings.Default.Backup_XoaDinhKy = false;
            XtraMessageBox.Show("Đã lưu thành công !");
        }

        /// <summary>
        /// Chuyển dữ liệu kiểu thời gian thành kiểu string
        /// </summary>
        /// <param name="DateTimeNow"></param>
        /// <returns></returns>
        private string ConverDateTimeToString(DateTime DateTimeNow)
        {
            string Thoigian = DateTimeNow.ToShortDateString().Replace('/','.');
            return Thoigian;
        }

        /// <summary>
        /// Xóa file
        /// </summary>
        /// <param name="Duongdanfile"></param>
        public void DeleteFile(string Duongdanfile)
        {
                FileInfo file = new FileInfo(Duongdanfile);

                if(file.Exists) // kiểm tra xem file có tồn tại không
                    file.Delete();
        }

        /// <summary>
        /// Lấy đường dẫn file
        /// </summary>
        /// <param name="Duongdan"></param>
        /// <param name="Thoigian"></param>
        /// <returns></returns>
        private string Get_pathfile(string Duongdan, DateTime Thoigian)
        {
            string Duongdanfile = Duongdan + @"\Sl_" + ConverDateTimeToString(Thoigian) + ".bak";
            return Duongdanfile;
        }

        /// <summary>
        /// Hàm xóa các bản sao lưu định kỳ 15 ngày
        /// </summary>
        public void XoaDinhKy()
        {
            DataTable tbSaoLuu = BUS.SaoLuu.SelectAll();
            for(int i=0; i < tbSaoLuu.Rows.Count; i++)
            {
                TimeSpan Days = DateTime.Now - Convert.ToDateTime(tbSaoLuu.Rows[i][2]);
                int TongSoNgay = Days.Days;
                if (TongSoNgay > 15)
                {
                    DeleteFile(Get_pathfile(Convert.ToString(tbSaoLuu.Rows[i][1]), Convert.ToDateTime(tbSaoLuu.Rows[i][2])));
                    BUS.SaoLuu.Xoa(Convert.ToString(tbSaoLuu.Rows[i][0]));
                }
                else
                    break;
            }
        }

    }
}