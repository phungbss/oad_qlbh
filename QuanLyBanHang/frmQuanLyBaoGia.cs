﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyBanHang
{
    public partial class frmQuanLyBaoGia : DevExpress.XtraEditors.XtraForm
    {
        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.BaoGia == null)
                BUS.BaoGia = new BusinessLogic.BaoGia();
            if (BUS.CTBaoGia == null)
                BUS.CTBaoGia = new BusinessLogic.CTBaoGia();
        }

        DataTable tbCTBG = new DataTable();
        DataTable tbBG = new DataTable();

        public frmQuanLyBaoGia()
        {
            InitializeComponent();
            KhoiTaoCacBUSCanThiet();
        }

        public void NapGiaoDien()
        {
            dateFrom.EditValue = DateTime.Now.AddMonths(-1);
            dateTo.EditValue = DateTime.Now;
        }

        public void KhoiTaoTableCTBG()
        {

            tbCTBG.Columns.Add("MaSanPham");
            tbCTBG.Columns.Add("GiaBan");
            tbCTBG.Columns.Add("GhiChu");
            tbCTBG.PrimaryKey = new DataColumn[] { tbCTBG.Columns[0] };
        }
        public void KhoiTaoTableBG()
        {
            tbBG.Columns.Add("MaBaoGia");
            tbBG.Columns.Add("NgayLap");
            tbBG.Columns.Add("MaNhanVien");
            tbBG.PrimaryKey = new DataColumn[] { tbBG.Columns[0] };
        }

        private void frmQuanLyBaoGia_Load(object sender, EventArgs e)
        {
            gridBaoGia.DataSource = BUS.BaoGia.Table;
            KhoiTaoTableCTBG();
            KhoiTaoTableBG();
            labStatusBG.Text = BUS.BaoGia.Table.Rows.Count.ToString() + " báo giá";
            NapGiaoDien();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            DateTime from = dateFrom.DateTime;
            DateTime to = dateTo.DateTime;

            tbBG.Rows.Clear();
            foreach (DataRow _r in BUS.BaoGia.Table.Rows)
            {
                DateTime date = DateTime.Parse(_r["NgayLap"].ToString());
                if (DateTime.Compare(date, from) >= 0 && DateTime.Compare(date, to) <= 0)
                {
                    DataRow r = tbBG.NewRow();
                    r["MaBaoGia"] = _r["MaBaoGia"];
                    r["NgayLap"] = _r["NgayLap"];
                    r["MaNhanVien"] = _r["MaNhanVien"];
                    tbBG.Rows.Add(r);
                }
            }
            gridBaoGia.DataSource = tbBG;
            labStatusBG.Text = tbBG.Rows.Count.ToString() + " báo giá";
        }

        private void btnHienTatCa_Click(object sender, EventArgs e)
        {
            gridBaoGia.DataSource = BUS.BaoGia.Table;
        }

        private void gridBaoGia_Click(object sender, EventArgs e)
        {
            tbCTBG.Rows.Clear();
            string maBG = gridViewBaoGia.GetRowCellValue(gridViewBaoGia.FocusedRowHandle, gridViewBaoGia.Columns[0]).ToString();
            foreach (DataRow _r in BUS.CTBaoGia.Table.Rows)
            {

                if (_r["MaBaoGia"].ToString() == maBG)
                {
                    DataRow r = tbCTBG.NewRow();
                    r["MaSanPham"] = _r["MaSanPham"];
                    r["GiaBan"] = _r["GiaBan"];
                    r["GhiChu"] = _r["GhiChu"];
                    tbCTBG.Rows.Add(r);
                }
            }
            gridCTBG.DataSource = tbCTBG;
            labNameCTBG.Text = "CHI TIẾT BÁO GIÁ [" + maBG + "]";
            labStatusCTBG.Text = tbCTBG.Rows.Count.ToString() + " sản phẩm trong báo giá";
            labNgayLap.Text = "Ngày Lập: " + gridViewBaoGia.GetRowCellValue(gridViewBaoGia.FocusedRowHandle, gridViewBaoGia.Columns[1]).ToString();
            labNhanVienLap.Text = "Mã nhân viên lập phiếu: " + gridViewBaoGia.GetRowCellValue(gridViewBaoGia.FocusedRowHandle, gridViewBaoGia.Columns[2]).ToString();
        }

        private void gridViewBaoGia_RowCountChanged(object sender, EventArgs e)
        {
            labStatusBG.Text = BUS.BaoGia.Table.Rows.Count.ToString() + " báo giá";
        }
    }
}