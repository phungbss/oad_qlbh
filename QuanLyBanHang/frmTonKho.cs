﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

namespace QuanLyBanHang
{
    public partial class frmTonKho : DevExpress.XtraEditors.XtraForm
    {
        //BusinessLogic.LoaiSanPham lsp_bus;
        //BusinessLogic.SanPham sp_bus;
        DataTable tbSP = new DataTable();
        DataTable tbKetQuaLocSanPham = new DataTable();
        public frmTonKho()
        {
            InitializeComponent();
            KhoiTaoBangKetQuaLocSanPham();
            KhoiTaoCacBUSCanThiet();
        }
        public void KhoiTaoCacBUSCanThiet()
        {
            if (BUS.LoaiSanPham == null)
                BUS.LoaiSanPham = new BusinessLogic.LoaiSanPham();
            if (BUS.SanPham == null)
                BUS.SanPham = new BusinessLogic.SanPham();
        }
        /// <summary>
        /// Nạp giao diện
        /// </summary>
        public void NapGiaoDien()
        {
            cboSP.Text = string.Empty;
            tbSP.Rows.Clear();
            //load cbo SP
            RepositoryItemComboBox properties_sp = cboSP.Properties;
            properties_sp.Items.Clear();
            foreach(DataRow item in BUS.SanPham.Table.Rows)
            {
                properties_sp.Items.Add(item["TenSanPham"].ToString() + " [" + item["MaSanPham"].ToString() + "]");
            }
        }

        public void KhoiTaoBangKetQuaLocSanPham()
        {
            tbKetQuaLocSanPham.Columns.Add("MaSanPham");
            tbKetQuaLocSanPham.Columns.Add("TenSanPham");
           
            tbKetQuaLocSanPham.Columns.Add("TonKho");
            tbKetQuaLocSanPham.Columns.Add("DonVi");
            tbKetQuaLocSanPham.Columns.Add("MaLoai");
            tbKetQuaLocSanPham.PrimaryKey = new DataColumn[] { tbKetQuaLocSanPham.Columns[0] };
        }


        //hàm này ko cần thiết
        public void KhoiTaoTableSP()
        {
            tbSP.Columns.Add("MaSanPham");
            tbSP.Columns.Add("TenSanPham");
            tbSP.Columns.Add("TonKho");
            tbSP.Columns.Add("DonVi");
            tbSP.Columns.Add("MaLoai");
            tbSP.PrimaryKey = new DataColumn[] { tbSP.Columns[0] };
        }

        private void frmTonKho_Load(object sender, EventArgs e)
        {
            KhoiTaoTableSP();
            gridMaLoai.DataSource = BUS.LoaiSanPham.Table;
            gridSP.DataSource = tbSP;
            labStateLoaiSP.Text = BUS.LoaiSanPham.Table.Rows.Count.ToString() + " loại sản phẩm - Double Click để xem các sản phẩm theo loại";
            NapGiaoDien();
        }

        private void gridMaLoai_Click(object sender, EventArgs e)
        {
           
        }

        private void btnChonSP_Click(object sender, EventArgs e)
        {
            try
            {
                tbSP.Rows.Clear();
                DataRow r = tbSP.NewRow();
                DataRow _r = BUS.SanPham.Table.Rows.Find(cboSP.Text.Substring(cboSP.Text.IndexOf('[') + 1, 8));
                r["MaSanPham"] = _r["MaSanPham"];
                r["TenSanPham"] = _r["TenSanPham"];
                r["DonVi"] = _r["DonVi"];
                r["TonKho"] = _r["TonKho"];
                r["MaLoai"] = _r["MaLoai"];

                tbSP.Rows.Add(r);
                gridViewSP.SelectRow(gridViewSP.FindRow(r));
                gridViewSP.Focus();
                gridSP.DataSource = tbSP;
                labStateSPDaChon.Text = tbSP.Rows.Count.ToString() + " sản phẩm trong danh sách";
            }
            catch (ConstraintException)
            {
                XtraMessageBox.Show("Bạn đã chọn sản phẩm này rồi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridMaLoai_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //cboSP.Enabled = false;
                string LoaiSP = gridViewMaLoai.GetRowCellValue(gridViewMaLoai.FocusedRowHandle, gridViewMaLoai.Columns[0]).ToString();
                tbKetQuaLocSanPham.Rows.Clear();
                BusinessLogic.SanPham temp = new BusinessLogic.SanPham();
                foreach (DataRow _r in temp.Table.Rows)
                {
                    if (_r["MaLoai"].ToString() == LoaiSP)
                    {
                        DataRow r = tbKetQuaLocSanPham.NewRow();
                        r["MaSanPham"] = _r["MaSanPham"];
                        r["TenSanPham"] = _r["TenSanPham"];
                        r["DonVi"] = _r["DonVi"];
                        r["TonKho"] = _r["TonKho"];
                        r["MaLoai"] = _r["MaLoai"];
                        tbKetQuaLocSanPham.Rows.Add(r);
                    }
                }
                labStateSPDaChon.Text = tbKetQuaLocSanPham.Rows.Count.ToString() + " sản phẩm trong danh sách";
                gridSP.DataSource = tbKetQuaLocSanPham;             
            }
            catch
            {
            }
        }
        private void cboSP_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnChonSP.PerformClick();
            }
        }
    }
}