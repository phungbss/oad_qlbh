﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho SanPham
    /// </summary>
    public class SanPham
    {
        /// <summany>
        /// Truy cập đến bảng SANPHAM
        /// </summany>
        private DataAccess.SanPham access = new DataAccess.SanPham();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summany>
        /// Thêm một Sản phẩm vào bảng SANPHAM
        /// </summany>
        /// <param name="sp">Sản phẩm cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.SanPham sp)
        {
            return access.Them(sp);
        }

        /// <summany>
        /// Xóa một Sản phẩm từ bảng SANPHAM
        /// </summany>
        /// <param name="maSanPham">Sản phẩm cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maSanPham)
        {
            return access.Xoa(maSanPham);
        }

        /// <summany>
        /// Cập nhật một Sản phẩm từ bảng SANPHAM
        /// </summany>
        /// <param name="maSanPham">Sản phẩm cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.SanPham sp)
        {
            return access.Sua(sp);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "SP";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
