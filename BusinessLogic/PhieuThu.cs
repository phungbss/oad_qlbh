﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho PhieuThu
    /// </summary>
    public class PhieuThu
    {
        /// <summary>
        /// Truy cập đến bảng PHIEUTHU
        /// </summary>
        private DataAccess.PhieuThu access = new DataAccess.PhieuThu();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một phiếu thu vào bảng PHIEUTHU
        /// </summary>
        /// <param name="pt">phiếu thu cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuThu pt)
        {
            return access.Them(pt);
        }

        /// <summary>
        /// Xóa phiếu thu từ bảng
        /// </summary>
        /// <param name="maPhieu">mã phiêu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu)
        {
            return access.Xoa(maPhieu);
        }

        /// <summary>
        /// Sửa một phiếu thu
        /// </summary>
        /// <param name="pt">phiếu thu cần sửa</param>
        /// <returns></returns>
        public bool Sua(DataTransfer.PhieuThu pt)
        {
            return access.Sua(pt);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "PT";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
