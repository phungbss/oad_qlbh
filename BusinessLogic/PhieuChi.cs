﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho PhieuThu
    /// </summary>
    public class PhieuChi
    {
        /// <summary>
        /// Truy cập đến bảng PHIEUCHI
        /// </summary>
        private DataAccess.PhieuChi access = new DataAccess.PhieuChi();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một phiếu thu vào bảng PHIEUCHI
        /// </summary>
        /// <param name="pc">phiếu thu cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuChi pc)
        {
            return access.Them(pc);
        }

        /// <summary>
        /// Xóa phiếu thu từ bảng
        /// </summary>
        /// <param name="maPhieu">mã phiếu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu)
        {
            return access.Xoa(maPhieu);
        }

        /// <summary>
        /// Sửa một phiếu chi
        /// </summary>
        /// <param name="pc">phiếu thu cần sửa</param>
        /// <returns></returns>
        public bool Sua(DataTransfer.PhieuChi pc)
        {
            return access.Sua(pc);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "PC";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
