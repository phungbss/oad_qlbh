﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho CTPhieuXuat
    /// </summary>
    public class CTPhieuXuat
    {
        /// <summary>
        /// Truy cập đến bảng CTPHIEUXUAT
        /// </summary>
        private DataAccess.CTPhieuXuat access = new DataAccess.CTPhieuXuat();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một chi tiết phiếu xuất vào bảng CTPHIEUXUAT
        /// </summary>
        /// <param name="ctpx">Chi tiết phiếu xuất cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTPhieuXuat ctpx)
        {
            return access.Them(ctpx);
        }
        /// <summary>
        /// Xóa một hóa đơn từ bảng CTPHIEUXUAT
        /// </summary>
        /// <param name="maPhieu">Mã phiếu xuất cần xóa</param>
        /// <param name="maSanPham">Sản phẩm của phiếu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu, string maSanPham)
        {
            return access.Xoa(maPhieu, maSanPham);
        }

        /// <summary>
        /// Cập nhật một chi tiết hóa đơn trong bảng CTPHIEUXUAT
        /// </summary>
        /// <param name="ctpx">Chi tiết phiếu xuất cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.CTPhieuXuat ctpx)
        {
            return access.Sua(ctpx);
        }
    }
}
