﻿using System;
using System.Data;
using System.IO;

namespace BusinessLogic
{
    public class SaoLuuPhucHoi
    {
        DataAccess.SaoLuuPhucHoi access = new DataAccess.SaoLuuPhucHoi();

        public DataTable Table
        {
            get { return access.Table; }
        }

        public bool Them(DataTransfer.SaoLuuPhucHoi saoLuuPhucHoi, bool isBackUpImmmediately = false)
        {
            return access.Them(saoLuuPhucHoi, isBackUpImmmediately);
        }

        public bool Xoa(string maSaoLuu)
        {
            return access.Xoa(maSaoLuu);
        }

        public bool PhucHoi(string duongDan)
        {
            return access.PhucHoi(duongDan);
        }

        public string AutoGenerateID()
        {
            string id = "SL";
            while (true)
            {
                string result = id + access.AutoGenerateNumber();
                if (access.Table.Rows.Find(result) == null)
                    return result;
            }
        }
    }
}
