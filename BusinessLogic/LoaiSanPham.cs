﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho LoaiSanPham
    /// </summary>
    public class LoaiSanPham
    {
        /// <summany>
        /// Truy cập đến bảng LOAISANPHAM
        /// </summany>
        private DataAccess.LoaiSanPham access = new DataAccess.LoaiSanPham();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summany>
        /// Thêm một Loại Sản phẩm vào bảng LOAISANPHAM
        /// </summany>
        /// <param name="lsp">Loại Sản phẩm cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.LoaiSanPham lsp)
        {
            return access.Them(lsp);
        }

        /// <summany>
        /// Xóa một Sản phẩm từ bảng LOAISANPHAM
        /// </summany>
        /// <param name="maLoai">Loại Sản phẩm cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maLoai)
        {
            return access.Xoa(maLoai);
        }

        /// <summany>
        /// Cập nhật một Loại Sản phẩm từ bảng LOAISANPHAM
        /// </summany>
        /// <param name="maLoai">Loại Sản phẩm cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.LoaiSanPham lsp)
        {
            return access.Sua(lsp);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "LS";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
