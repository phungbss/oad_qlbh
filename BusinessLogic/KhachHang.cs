﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho KhachHang
    /// </summary>
    public class KhachHang
    {
        /// <summary>
        /// Truy cập đến bảng KHACHHANG
        /// </summary>
        private DataAccess.KhachHang access = new DataAccess.KhachHang();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một khách hàng vào bảng KHACHHANG
        /// </summary>
        /// <param name="kh">Khách hàng cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.KhachHang kh)
        {
            return access.Them(kh);
        }

        /// <summary>
        /// Xóa một khách hàng từ bảng KHACHHANG
        /// </summary>
        /// <param name="maKhachHang">Khách hàng cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maKhachHang)
        {
            return access.Xoa(maKhachHang);
        }

        /// <summary>
        /// Cập nhật thông tin một khách hàng trong bảng KHACHHANG
        /// </summary>
        /// <param name="kh">Khách hàng cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.KhachHang kh)
        {
            return access.Sua(kh);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "KH";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
