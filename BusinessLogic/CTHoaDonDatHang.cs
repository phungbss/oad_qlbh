﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho CTHoaDonDatHang
    /// </summary>
    public class CTHoaDonDatHang
    {
        /// <summany>
        /// Truy cập đến bảng CTHOADONDATHANG
        /// </summany>
        private DataAccess.CTHoaDonDatHang access = new DataAccess.CTHoaDonDatHang();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summany>
        /// Thêm một hóa đơn vào bảng CTHOADONDATHANG
        /// </summany>
        /// <param name="hd">Hóa đơn cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTHoaDonDatHang Cthd, bool themSanPham = false)
        {
            return access.Them(Cthd, themSanPham);
        }

        /// <summany>
        /// Xóa một hóa đơn từ bảng CTHOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">Hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon, string maSanPham)
        {
            return access.Xoa(maHoaDon,maSanPham);
        }

        /// <summany>
        /// Cập nhật một hóa đơn từ bảng CTHOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">Hóa đơn cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.CTHoaDonDatHang Cthd)
        {
            return access.Sua(Cthd);
        }
    }
}
