﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho HoaDonDatHang
    /// </summary>
    public class HoaDonDatHang
    {
        /// <summany>
        /// Truy cập đến bảng HOADONDATHANG
        /// </summany>
        private DataAccess.HoaDonDatHang access = new DataAccess.HoaDonDatHang();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summany>
        /// Thêm một hóa đơn vào bảng HOADONDATHANG
        /// </summany>
        /// <param name="hd">Hóa đơn cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.HoaDonDatHang hd)
        {
            return access.Them(hd);
        }

        /// <summany>
        /// Xóa một hóa đơn từ bảng HOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">Hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon)
        {
            return access.Xoa(maHoaDon);
        }

        /// <summany>
        /// Cập nhật một hóa đơn từ bảng HOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">Hóa đơn cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.HoaDonDatHang hd)
        {
            return access.Sua(hd);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "DH";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
