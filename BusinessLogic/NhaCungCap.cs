﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho NhaCungCap
    /// </summary>
    public class NhaCungCap
    {
        /// <summary>
        /// Truy cập đến bảng NHACUNGCAP
        /// </summary>
        private DataAccess.NhaCungCap access = new DataAccess.NhaCungCap();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một nhà cung cấp vào bảng NHACUNGCAP
        /// </summary>
        /// <param name="ncc">Nhà cung cấp cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.NhaCungCap ncc)
        {
            return access.Them(ncc);
        }

        /// <summary>
        /// Xóa một nhà cung cấp từ bảng NHACUNGCAP
        /// </summary>
        /// <param name="maNhaCungCap">Nhà cung cấp cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maNhaCungCap)
        {
            return access.Xoa(maNhaCungCap);
        }

        /// <summary>
        /// Cập nhật thông tin một nhà cung cấp trong bảng NHACUNGCAP
        /// </summary>
        /// <param name="ncc">Nhà cung cấp cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.NhaCungCap ncc)
        {
            return access.Sua(ncc);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "NC";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
