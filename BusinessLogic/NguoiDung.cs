﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho NguoiDung
    /// </summary>
    public class NguoiDung
    {
        /// <summary>
        /// Truy cập đến bảng NGUOIDUNG
        /// </summary>
        private DataAccess.NguoiDung access = new DataAccess.NguoiDung();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một người dùng vào bảng NGUOIDUNG
        /// </summary>
        /// <param name="nd">Người dùng cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.NguoiDung nd)
        {
            return access.Them(nd);
        }

        /// <summary>
        /// Xóa một người dùng từ bảng NGUOIDUNG
        /// </summary>
        /// <param name="maNguoiDung">Người dùng cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maNguoiDung)
        {
            return access.Xoa(maNguoiDung);
        }

        /// <summary>
        /// Cập nhật thông tin một người dùng trong bảng NGUOIDUNG
        /// </summary>
        /// <param name="nd">Người dùng cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.NguoiDung nd)
        {
            return access.Sua(nd);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "ND";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }

        public bool DangNhap(string name, string pass)
        {
            return access.DangNhap(name, pass);
        }
    }
}
