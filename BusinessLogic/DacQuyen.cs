﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho DacQuyen
    /// </summary>
    public class DacQuyen
    {
        /// <summary>
        /// Truy cập đến bảng NGUOIDUNG
        /// </summary>
        private DataAccess.DacQuyen access = new DataAccess.DacQuyen();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một đặc quyền vào bảng DACQUYEN
        /// </summary>
        /// <param name="dq">Đặc quyền cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.DacQuyen dq)
        {
            return access.Them(dq);
        }

        /// <summary>
        /// Xóa một đặc quyền từ bảng DACQUYEN
        /// </summary>
        /// <param name="maDacQuyen">Đặc quyền cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maDacQuyen)
        {
            return access.Xoa(maDacQuyen);
        }

        /// <summary>
        /// Cập nhật thông tin đặc quyền trong bảng DACQUYEN
        /// </summary>
        /// <param name="dq">Đặc quyền cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.DacQuyen dq)
        {
            return access.Sua(dq);
        }
    }
}
