﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho CTBaoGia
    /// </summary>
    public class CTBaoGia
    {
        /// <summary>
        /// Truy cập đến bảng CTBAOGIA
        /// </summary>
        private DataAccess.CTBaoGia access = new DataAccess.CTBaoGia();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một chi tiết báo giá
        /// </summary>
        /// <param name="bg"></param>
        /// <returns></returns>
        public bool Them(DataTransfer.CTBaoGia bg)
        {
            return access.Them(bg);
        }
        /// <summary>
        /// Xóa chi tiết báo giáo
        /// </summary>
        /// <param name="maBaoGia">mã báo giá</param>
        /// <param name="maSanPham">mã sản phẩm cần xóa</param>
        /// <returns></returns>
        public bool Xoa(string maBaoGia, string maSanPham)
        {
            return access.Xoa(maBaoGia, maSanPham);
        }

        /// <summary>
        /// Sửa một chi tiết báo giá
        /// </summary>
        /// <param name="ctbg">chi tiết báo giá cần sửa</param>
        /// <returns></returns>
        public bool Sua(DataTransfer.CTBaoGia ctbg)
        {
            return access.Sua(ctbg);
        }
    }
}
