﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho HoaDonBanHang
    /// </summary>
    public class HoaDonBanHang
    {
        /// <summary>
        /// Truy cập đến bảng HOADONBANHANG
        /// </summary>
        private DataAccess.HoaDonBanHang access = new DataAccess.HoaDonBanHang();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một hóa đơn vào bảng HOADONBANHANG
        /// </summary>
        /// <param name="hd">Hóa đơn cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.HoaDonBanHang hd)
        {
            return access.Them(hd);
        }

        /// <summary>
        /// Xóa một hóa đơn từ bảng HOADONBANHANG
        /// </summary>
        /// <param name="maHoaDon">Hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon)
        {
            return access.Xoa(maHoaDon);
        }

        /// <summary>
        /// Cập nhật một hóa đơn trong bảng HOADONBANHANG
        /// </summary>
        /// <param name="hd">Hóa đơn cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.HoaDonBanHang hd)
        {
            return access.Sua(hd);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "BH";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
