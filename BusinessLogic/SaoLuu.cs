﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho SaoLuu
    /// </summary>
    public class SaoLuu
    {
        /// <summany>
        /// Truy cập đến bảng SAOLUUPHUCHOI
        /// </summany>
        private DataAccess.SaoLuu access = new DataAccess.SaoLuu();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summany>
        /// Thêm một bản Sao lưu vào bảng SAOLUUPHUCHOI
        /// </summany>
        /// <param name="sl">Bản Sao lưu cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.SaoLuu sl)
        {
            return access.Them(sl);
        }

        /// <summany>
        /// Xóa một bản Sao lưu từ bảng SAOLUUPHUCHOI
        /// </summany>
        /// <param name="maSaoLuu">Bản Sao lưu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maSaoLuu)
        {
            return access.Xoa(maSaoLuu);
        }

        /// <summany>
        /// Cập nhật một bản Sao lưu từ bảng SAOLUUPHUCHOI
        /// </summany>
        /// <param name="maSaoLuu">Bản Sao lưu cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.SaoLuu sl)
        {
            return access.Sua(sl);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "SL";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }

        public bool BackUp_full(string Duongdan, string TenSl)
        {
            return access.Backups_full(Duongdan, TenSl);
        }

        public bool Restore(string Duongdan)
        {
            return access.Restore(Duongdan);
        }

    }
}
