﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho BaoGia
    /// </summary>
    public class BaoGia
    {
        /// <summary>
        /// Truy cập đến bảng BAOGIA
        /// </summary>
        private DataAccess.BaoGia access = new DataAccess.BaoGia();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một báo giá vào bảng BAOGIA
        /// </summary>
        /// <param name="bg">báo giá cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.BaoGia bg)
        {
            return access.Them(bg);
        }

        /// <summary>
        /// Xóa một báo giá
        /// </summary>
        /// <param name="maBaoGia">mã báo giá cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maBaoGia)
        {
            return access.Xoa(maBaoGia);
        }

        /// <summary>
        /// Sửa một báo giá
        /// </summary>
        /// <param name="bg">báo giá cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.BaoGia bg)
        {
            return access.Sua(bg);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "BG";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
