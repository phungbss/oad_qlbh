﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho PhieuNhap
    /// </summary>
    public class PhieuNhap
    {
        /// <summary>
        /// Truy cập đến bảng PHIEUNHAP
        /// </summary>
        private DataAccess.PhieuNhap access = new DataAccess.PhieuNhap();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một phiếu  nhập vào bảng PHIEUNHAP
        /// </summary>
        /// <param name="pn">phiếu nhập cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuNhap pn)
        {
            return access.Them(pn);
        }

        /// <summary>
        /// Xóa một phiếu nhập từ bảng PHIEUNHAP
        /// </summary>
        /// <param name="maPhieu">phiếu nhập cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu)
        {
            return access.Xoa(maPhieu);
        }

        /// <summary>
        /// Cập nhật một phiếu nhập trong bảng PHIEUNHAP
        /// </summary>
        /// <param name="pn">phiếu  nhập cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.PhieuNhap pn)
        {
            return access.Sua(pn);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "PN";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
