﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho CTPhieuNhap
    /// </summary>
    public class CTPhieuNhap
    {
        /// <summary>
        /// Truy cập đến bảng CTPHIEUNHAP
        /// </summary>
        private DataAccess.CTPhieuNhap access = new DataAccess.CTPhieuNhap();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một chi tiết phiếu nhập vào bảng CTPHIEUNHAP
        /// </summary>
        /// <param name="ctpn">Chi tiết phiếu nhập cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTPhieuNhap ctpn)
        {
            return access.Them(ctpn);
        }
        /// <summary>
        /// Xóa một hóa đơn từ bảng CTPHIEUNHAP
        /// </summary>
        /// <param name="maPhieu">Mã phiếu nhập cần xóa</param>
        /// <param name="maSanPham">Sản phẩm của phiếu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu, string maSanPham)
        {
            return access.Xoa(maPhieu, maSanPham);
        }

        /// <summary>
        /// Cập nhật một chi tiết trong bảng CTPHIEUNHAP
        /// </summary>
        /// <param name="ctpn">Chi tiết phiếu nhập cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.CTPhieuNhap ctpn)
        {
            return access.Sua(ctpn);
        }
    }
}
