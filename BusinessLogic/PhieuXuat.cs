﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lí nghiệp vụ cho PhieuXuat
    /// </summary>
    public class PhieuXuat
    {
        /// <summary>
        /// Truy cập đến bảng PHIEUXUAT
        /// </summary>
        private DataAccess.PhieuXuat access = new DataAccess.PhieuXuat();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một phiếu  nhập vào bảng PHIEUXUAT
        /// </summary>
        /// <param name="px">phiếu xuất cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuXuat px)
        {
            return access.Them(px);
        }

        /// <summary>
        /// Xóa một phiếu xuất từ bảng PHIEUXUAT
        /// </summary>
        /// <param name="maHoaDon">phiếu xuất cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu)
        {
            return access.Xoa(maPhieu);
        }

        /// <summary>
        /// Cập nhật một phiếu xuất trong bảng PHIEUXUAT
        /// </summary>
        /// <param name="px">phiếu xuất cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.PhieuXuat px)
        {
            return access.Sua(px);
        }

        /// <summary>
        /// Tự động sinh mã ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateID()
        {
            //mã định danh
            string id = "PX";

            //mã ngẫu nhiên đã tồn tại chưa
            bool isExist;

            do
            {
                string result = id + access.AutoGenerateNumber();
                isExist = Table.Rows.Find(result) != null;
                if (!isExist)
                    return result;
            }
            while (isExist);
            return null;
        }
    }
}
