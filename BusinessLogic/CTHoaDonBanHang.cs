﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    /// <summary>
    /// Xử lý nghiệp vụ cho CTHoaDonBanHang
    /// </summary>
    public class CTHoaDonBanHang
    {
        /// <summary>
        /// Truy cập đến bảng CTHOADONBANHANG
        /// </summary>
        private DataAccess.CTHoaDonBanHang access = new DataAccess.CTHoaDonBanHang();

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return access.Table; }
            set { access.Table = value; }
        }

        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return access.SelectAll();
        }

        /// <summary>
        /// Thêm một chi tiết hóa đơn vào bảng CTHOADONBANHANG
        /// </summary>
        /// <param name="cthd">Hóa đơn cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTHoaDonBanHang cthd)
        {
            return access.Them(cthd);
        }

        /// <summary>
        /// Xóa một hóa đơn từ bảng CTHOADONBANHANG
        /// </summary>
        /// <param name="maHoaDon">Chi tiết hóa đơn cần xóa</param>
        /// <param name="maSanPham">Sản phẩm của hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon, string maSanPham, bool xoaSanPham = false)
        {
            return access.Xoa(maHoaDon,maSanPham, xoaSanPham);
        }

        /// <summary>
        /// Cập nhật một chi tiết hóa đơn trong bảng HOADONBANHANG
        /// </summary>
        /// <param name="cthd">Chi tiết hóa đơn cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.CTHoaDonBanHang cthd)
        {
            return access.Sua(cthd);
        }


    }
}
