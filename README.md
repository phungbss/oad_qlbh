# QUẢN LÝ CỬA HÀNG TẠP HÓA #

* Môn học: Phương pháp Phát triển phần mềm hướng đối tượng
* Giảng viên: ThS. Phạm Thi Vương

### Nhóm 17
* Nguyễn Thiện Nhân - 14520626
* Nguyễn Lê Gia Phụng - 14520705
* Trần Minh Công - 14520100
* Võ Thị Thanh Thảo - 14520857

### What is this repository for? ###

Đồ án Quản lý cửa  hàng tạp hóa

### Things you need ###

DevExpress, Visual Studio (VS2015 recommended), SQL Server (ver2014 recommended)

Optional: [Sourcetree](https://www.sourcetreeapp.com) 