﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DataAccess
{
    public class SaoLuuPhucHoi: DBConnect
    {
        private DataTable table;

        public DataTable Table
        {
            get { return table; }
        }

        #region Methods
        public SaoLuuPhucHoi()
        {
            table = GetData("Select * from SAOLUUPHUCHOI");
            table.PrimaryKey = new DataColumn[] { table.Columns[DataTransfer.SaoLuuPhucHoi.MASAOLUU]};
        }

        public bool Them(DataTransfer.SaoLuuPhucHoi saoLuuPhucHoi, bool isBackUpImmediate = false)
        {
            try
            {
                if (isBackUpImmediate && !SaoLuu(saoLuuPhucHoi.DuongDan))
                    return false;

                DataRow row = table.NewRow();
                row[DataTransfer.SaoLuuPhucHoi.MASAOLUU] = saoLuuPhucHoi.MaSaoLuu;
                row[DataTransfer.SaoLuuPhucHoi.NGAYTHUCHIEN] = saoLuuPhucHoi.NgayThucHien;
                row[DataTransfer.SaoLuuPhucHoi.DUONGDAN] = saoLuuPhucHoi.DuongDan;
                row[DataTransfer.SaoLuuPhucHoi.NOIDUNG] = saoLuuPhucHoi.NoiDung;
                table.Rows.Add(row);

                SqlDataAdapter adapter = new SqlDataAdapter("Select * from SAOLUUPHUCHOI", Connection);
                SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
                adapter.Update(table);

                return true;
            }
            catch
            {
                CloseConnection();
                return false;
            }
        }

        public bool Xoa(string maSaoLuu)
        {
            try
            {
                DataRow row = table.Rows.Find(maSaoLuu);
                if (row == null)
                    return false;

                FileInfo file = new FileInfo(row[DataTransfer.SaoLuuPhucHoi.DUONGDAN].ToString());
                if(file.Exists)
                    file.Delete();

                row.Delete();
                
                SqlDataAdapter adapter = new SqlDataAdapter("Select * from SAOLUUPHUCHOI", Connection);
                SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
                adapter.Update(table);

                return true;
            }
            catch(Exception ex)
            {
                CloseConnection();
                return false;
            }
        }

        private bool SaoLuu(string duongDan)
        {
            try
            {
                string sqlCommand = string.Format(
                    @"Backup Database QUANLYBANHANG TO DISK = N'{0}' WITH NOFORMAT, INIT, SKIP, STATS = 10",
                    duongDan
                    );

                ExecuteCommand(sqlCommand);

                return true;
            }
            catch
            {
                CloseConnection();
                return false;
            }
        }

        public bool PhucHoi(string duongDan)
        {
            try
            {
                SqlCommand sqlCommand;

                OpenConnection();
                sqlCommand = new SqlCommand("USE [master]", Connection);
                sqlCommand.ExecuteNonQuery();

                sqlCommand = new SqlCommand(
                    string.Format(@"Restore Database QUANLYBANHANG FROM DISK = N'{0}' WITH STATS = 10", duongDan),
                    Connection
                    );
                sqlCommand.ExecuteNonQuery();
                CloseConnection();

                return true;
            }
            catch(Exception ex)
            {
                CloseConnection();
                return false;
            }
        }
        #endregion
    }
}
