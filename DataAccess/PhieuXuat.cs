﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa các phương thức truy xuất cơ sở dữ liệu cho PhieuXuat
    /// </summary>
    public class PhieuXuat : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public PhieuXuat()
        {
            //nạp bảng
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }
        /// <summary>
        /// Lấy tất cả dữ liệu trong bảng PHIEUXUAT
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from PHIEUXUAT");
        }

        /// <summary>
        /// Thêm phiếu xuất
        /// </summary>
        /// <param name="px">Phiếu xuất cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuXuat px)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUXUAT", Connection);

                //tạo dòng mới và thêm dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaPhieu"] = px.MaPhieu;
                r["MaNhanVien"] = px.MaNhanVien;
                r["NgayLap"] = px.NgayLap;
                r["TongTien"] = px.TongTien;
                r["GhiChu"] = px.GhiChu;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một phiếu từ bảng PHIEUXUAT 
        /// </summary>
        /// <param name="maPhieu">phiếu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUXUAT", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maPhieu);

                //xóa khóa ngoại
                DataAccess.CTPhieuXuat px = new CTPhieuXuat();

                DataRow[] px_row = px.Table.Select("MaPhieu like '" + maPhieu + "'");
                foreach (DataRow item in px_row)
                {
                    px.Xoa(item["MaPhieu"].ToString(), item["MaSanPham"].ToString());
                }


                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật một PHIẾU trong bảng PHIEUXUAT
        /// </summary>
        /// <param name="px">Phiếu cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.PhieuXuat px)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUXUAT", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(px.MaPhieu);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaPhieu"] = px.MaPhieu;
                    r["MaNhanVien"] = px.MaNhanVien;
                    r["NgayLap"] = px.NgayLap;
                    r["TongTien"] = px.TongTien;
                    r["GhiChu"] = px.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
