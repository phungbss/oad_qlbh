﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho SanPham
    /// </summary>
    public class SanPham : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public SanPham()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summany>
        /// Lấy tất cả dữ liệu của bảng SANPHAM
        /// </summany>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from SANPHAM");
        }

        /// <summany>
        /// Thêm một Sản phẩm bảng SANPHAM
        /// </summany>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.SanPham sp)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from SANPHAM", Connection);

                //Tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaSanPham"] = sp.MaSanPham;
                r["TenSanPham"] = sp.TenSanPham;
                r["GiaNhap"] = sp.GiaNhap;
                r["GiaBan"] = sp.GiaBan;
                r["MaLoai"] = sp.MaLoai;
                r["TonKho"] = sp.TonKho;
                r["MoTa"] = sp.MoTa;
                r["DonVi"] = sp.DonVi;
                r["VAT"] = sp.VAT;
                r["ChietKhau"] = sp.ChietKhau;
                dt.Rows.Add(r);

                //Cập nhật vào cơ sở dữ liệu
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Xóa một hóa đơn từ bảng SANPHAM
        /// </summany>
        /// <param name="maSanPham">Sản phẩm cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maSanPham)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from SANPHAM", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maSanPham);

                //Kiểm tra khóa ngoại
                DataAccess.CTHoaDonBanHang Ctbh = new DataAccess.CTHoaDonBanHang();
                DataRow[] ctbh_row = Ctbh.Table.Select("MaSanPham like '" + maSanPham + "'");
                foreach (DataRow item in ctbh_row)
                    Ctbh.Xoa(item["MaHoaDon"].ToString(), item["MaSanPham"].ToString());

                DataAccess.CTHoaDonDatHang Ctdh = new DataAccess.CTHoaDonDatHang();
                DataRow[] ctdh_row = Ctdh.Table.Select("MaSanPham like '" + maSanPham + "'");
                foreach (DataRow item in ctdh_row)
                    Ctdh.Xoa(item["MaHoaDon"].ToString(), item["MaSanPham"].ToString());

                DataAccess.CTPhieuNhap Ctpn = new DataAccess.CTPhieuNhap();
                DataRow[] ctpn_row = Ctpn.Table.Select("MaSanPham like '" + maSanPham + "'");
                foreach (DataRow item in ctpn_row)
                    Ctpn.Xoa(item["MaPhieu"].ToString(), item["MaSanPham"].ToString());

                DataAccess.CTPhieuXuat Ctpx = new DataAccess.CTPhieuXuat();
                DataRow[] ctpx_row = Ctpx.Table.Select("MaSanPham like '" + maSanPham + "'");
                foreach (DataRow item in ctpx_row)
                    Ctpx.Xoa(item["MaPhieu"].ToString(), item["MaSanPham"].ToString());               

                DataAccess.CTBaoGia Ctbg = new DataAccess.CTBaoGia();
                DataRow[] ctbg_row = Ctbg.Table.Select("MaSanPham like '" + maSanPham + "'");
                foreach (DataRow item in ctbg_row)
                    Ctbg.Xoa(item["MaBaoGia"].ToString(), item["MaSanPham"].ToString());

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật một sản phẩm trong bảng SANPHAM
        /// </summary>
        /// <param name="sp">Sản phẩm cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.SanPham sp)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from SANPHAM", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(sp.MaSanPham);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaSanPham"] = sp.MaSanPham;
                    r["TenSanPham"] = sp.TenSanPham;
                    r["GiaNhap"] = sp.GiaNhap;
                    r["GiaBan"] = sp.GiaBan;
                    r["MaLoai"] = sp.MaLoai;
                    r["TonKho"] = sp.TonKho;
                    r["MoTa"] = sp.MoTa;
                    r["DonVi"] = sp.DonVi;
                    r["VAT"] = sp.VAT;
                    r["ChietKhau"] = sp.ChietKhau;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
