﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa các phương thức truy xuất cơ sở dữ liệu cho PhieuThu
    /// </summary>
    public class PhieuThu : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public PhieuThu()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }
        /// <summary>
        /// Lấy tất cả dữ liệu của bảng PHIEUTHU
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from PHIEUTHU");
        }
        /// <summary>
        /// Nhập dữ liệu vào bảng PHIEUTHU
        /// </summary>
        /// <param name="pt">phiếu thu cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuThu pt)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUTHU", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaPhieu"] = pt.MaPhieu;
                r["NgayLap"] = pt.NgayLap;
                r["MaNhanVien"] = pt.MaNhanVien;
                r["TenNguoiNop"] = pt.TenNguoiNop;
                r["SoTien"] = pt.SoTien;
                r["LyDo"] = pt.LyDo;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Xóa phiếu thu từ bảng PHIEUTHU
        /// </summary>
        /// <param name="maPhieu">mã phiếu cần xóa</param>
        /// <returns></returns>
        public bool Xoa(string maPhieu)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUTHU", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maPhieu);

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// sửa phiếu thu
        /// </summary>
        /// <param name="pt">phiếu thu cần sửa</param>
        /// <returns></returns>
        public bool Sua(DataTransfer.PhieuThu pt)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUTHU", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(pt.MaPhieu);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaPhieu"] = pt.MaPhieu;
                    r["NgayLap"] = pt.NgayLap;
                    r["MaNhanVien"] = pt.MaNhanVien;
                    r["TenNguoiNop"] = pt.TenNguoiNop;
                    r["SoTien"] = pt.SoTien;
                    r["LyDo"] = pt.LyDo;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
