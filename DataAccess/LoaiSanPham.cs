﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho LoaiSanPham
    /// </summary>
    public class LoaiSanPham : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public LoaiSanPham()
        {
            //Nạp bảng lên
            dt = SelectAll();

            //Đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summany>
        /// Lấy tất cả dữ liệu cho bảng LOAISANPHAM
        /// </summany>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from LOAISANPHAM");
        }

        /// <summany>
        /// Thêm một Loại sản phẩm vào bảng LOAISANPHAM
        /// </summany>
        /// <param name="lsp">Loại sản phẩm cần phải thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.LoaiSanPham lsp)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from LOAISANPHAM", Connection);

                //Tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaLoai"] = lsp.MaLoai;
                r["TenLoai"] = lsp.TenLoai;
                r["GhiChu"] = lsp.GhiChu;
                dt.Rows.Add(r);

                //Cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Xóa một Loại sản phẩm trong bảng LOAISANPHAM
        /// </summany>
        /// <param name="maLoai">Loại sản phẩm cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maLoai)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from LOAISANPHAM", Connection);

                //Tìm dòng
                DataRow r = dt.Rows.Find(maLoai);

                //Xóa dòng
                if (r != null)
                    r.Delete();

                //Cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Sửa một Loại sản phẩm trong bảng LOAISANPHAM
        /// </summany>
        /// <param name="maLoai">Loại sản phẩm cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.LoaiSanPham lsp)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from LOAISANPHAM", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(lsp.MaLoai);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaLoai"] = lsp.MaLoai;
                    r["TenLoai"] = lsp.TenLoai;
                    r["GhiChu"] = lsp.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
