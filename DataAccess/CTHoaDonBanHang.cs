﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho CTHoaDonBanHang
    /// </summary>
    public class CTHoaDonBanHang : DBConnect
    {
        private DataTable dt;
        private DataTable dtSP;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public CTHoaDonBanHang()
        {
            //nạp bảng lên
            dt = SelectAll();
            dtSP = GetData("select * from SANPHAM");

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0], dt.Columns[1] };
            dtSP.PrimaryKey = new DataColumn[] { dtSP.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu của bảng CTHOADONBANHANG
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from CTHOADONBANHANG");
        }

        /// <summary>
        /// Thêm một chi tiết hóa đơn vào bảng CTHOADONBANHANG
        /// </summary>
        /// <param name="cthd">Hóa đơn cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTHoaDonBanHang cthd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTHOADONBANHANG", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaHoaDon"] = cthd.MaHoaDon;
                r["MaSanPham"] = cthd.MaSanPham;
                r["SoLuong"] = cthd.SoLuong;
                
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);

                SqlDataAdapter da1 = new SqlDataAdapter("select * from SANPHAM", Connection);

                //tìm dòng
                DataRow r1 = dtSP.Rows.Find(cthd.MaSanPham);

                //cập nhật số lượng sản phẩm
                if (r1 != null)
                {
                    r1["TonKho"] = Convert.ToInt32(r1["TonKho"]) - cthd.SoLuong;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm1 = new SqlCommandBuilder(da1);
                da1.Update(dtSP);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một hóa đơn từ bảng CTHOADONBANHANG
        /// </summary>
        /// <param name="maHoaDon">Chi tiết hóa đơn cần xóa</param>
        /// <param name="maSanPham">Sản phẩm của hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon, string maSanPham, bool xoaSanPham = false)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTHOADONBANHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { maHoaDon, maSanPham });

                if (xoaSanPham == true)
                {
                    SqlDataAdapter da1 = new SqlDataAdapter("select * from SANPHAM", Connection);

                    //tìm dòng
                    DataRow r1 = dtSP.Rows.Find(Convert.ToString(r["MaSanPham"]));

                    //cập nhật số lượng sản phẩm
                    if (r1 != null)
                    {
                        r1["TonKho"] = Convert.ToInt32(r1["TonKho"]) + Convert.ToInt32(r["SoLuong"]);
                    }

                    //cập nhật vào CSDL
                    SqlCommandBuilder cm1 = new SqlCommandBuilder(da1);
                    da1.Update(dtSP);
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật một chi tiết hóa đơn trong bảng HOADONBANHANG
        /// </summary>
        /// <param name="cthd">Chi tiết hóa đơn cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.CTHoaDonBanHang cthd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTHOADONBANHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { cthd.MaHoaDon, cthd.MaSanPham });

                //cập nhật dòng
                if (r != null)
                {
                    r["MaHoaDon"] = cthd.MaHoaDon;
                    r["MaSanPham"] = cthd.MaSanPham;
                    r["SoLuong"] = cthd.SoLuong;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
