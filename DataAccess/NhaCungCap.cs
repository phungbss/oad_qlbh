﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho NhaCungCap
    /// </summary>
    public class NhaCungCap : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public NhaCungCap()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu của bảng NHACUNGCAP
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from NHACUNGCAP");
        }

        /// <summary>
        /// Thêm một nhà cung cấp vào bảng NHACUNGCAP
        /// </summary>
        /// <param name="ncc">Nhà cung cấp cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.NhaCungCap ncc)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from NHACUNGCAP", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaNhaCungCap"] = ncc.MaNhaCungCap;
                r["TenNhaCungCap"] = ncc.TenNhaCungCap;
                r["DiaChi"] = ncc.DiaChi;
                r["SoDienThoai"] = ncc.SoDienThoai;
                r["Email"] = ncc.Email;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một nhà cung cấp từ bảng NHACUNGCAP
        /// </summary>
        /// <param name="maNhaCungCap">Nhà cung cấp cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maNhaCungCap)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from NHACUNGCAP", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maNhaCungCap);

                //tìm và xóa khóa ngoại
                DataAccess.PhieuNhap pn = new DataAccess.PhieuNhap();
                DataRow[] pn_row = pn.Table.Select("MaNhaCungCap like '" + maNhaCungCap + "'");
                foreach (DataRow item in pn_row)
                {
                    pn.Xoa(item["MaPhieu"].ToString());
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật thông tin một nhà cung cấp trong bảng NHACUNGCAP
        /// </summary>
        /// <param name="ncc">Nhà cung cấp cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.NhaCungCap ncc)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from NHACUNGCAP", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(ncc.MaNhaCungCap);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaNhaCungCap"] = ncc.MaNhaCungCap;
                    r["TenNhaCungCap"] = ncc.TenNhaCungCap;
                    r["DiaChi"] = ncc.DiaChi;
                    r["SoDienThoai"] = ncc.SoDienThoai;
                    r["Email"] = ncc.Email;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
