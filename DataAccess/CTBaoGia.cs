﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho CTBaoGia
    /// </summary>
    public class CTBaoGia : DBConnect 
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public CTBaoGia()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0], dt.Columns[1] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu từ bảng CTBAOGIA
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from CTBAOGIA");
        }

        /// <summary>
        /// Thêm chi tiết báo giá
        /// </summary>
        /// <param name="bg">chi tiết cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTBaoGia bg)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTBAOGIA", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaBaoGia"] = bg.MaBaoGia;
                r["MaSanPham"] = bg.MaSanPham;
                r["GiaBan"] = bg.GiaBan;
                r["GhiChu"] = bg.GhiChu;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một chi tiết báo giá
        /// </summary>
        /// <param name="maBaoGia">mã báo giá</param>
        /// <param name="maSanPham">mã sản phẩm cần xóa</param>
        /// <returns></returns>
        public bool Xoa(string maBaoGia, string maSanPham)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTBAOGIA", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { maBaoGia, maSanPham });

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Sửa chi tiết báo giá
        /// </summary>
        /// <param name="bg">chi tiết báo giá cần sửa</param>
        /// <returns></returns>
        public bool Sua(DataTransfer.CTBaoGia bg)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTBAOGIA", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { bg.MaBaoGia, bg.MaSanPham });

                //cập nhật dòng
                if (r != null)
                {
                    r["MaBaoGia"] = bg.MaBaoGia;
                    r["MaSanPham"] = bg.MaSanPham;
                    r["GiaBan"] = bg.GiaBan;
                    r["GhiChu"] = bg.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
