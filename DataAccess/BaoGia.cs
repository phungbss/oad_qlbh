﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa các phương thức truy xuất cơ sở dữ liệu cho BaoGia
    /// </summary>
    public class BaoGia : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public BaoGia()
        {
            //nạp bảng
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu trong bảng BAOGIA
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from BAOGIA");
        }

        /// <summary>
        /// Thêm báo giá
        /// </summary>
        /// <param name="bg">báo giá cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.BaoGia bg)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from BAOGIA", Connection);

                //tạo dòng mới và thêm dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaBaoGia"] = bg.MaBaoGia;
                r["MaNhanVien"] = bg.MaNhanVien;
                r["NgayLap"] = bg.NgayLap;
                r["GhiChu"] = bg.GhiChu;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Xóa báo giá
        /// </summary>
        /// <param name="maBaoGia">mã báo giá cần xóa</param>
        /// <returns></returns>
        public bool Xoa(string maBaoGia)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from BAOGIA", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maBaoGia);

                //tìm và xóa khóa ngoại
                DataAccess.CTBaoGia bg = new CTBaoGia();
                
                DataRow[] bg_row = bg.Table.Select("MaBaoGia like '" + maBaoGia + "'");
                foreach (DataRow item in bg_row)
                {
                    bg.Xoa(item["MaBaoGia"].ToString(), item["MaSanPham"].ToString());
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sửa một báo giá
        /// </summary>
        /// <param name="bg">Báo giá cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.BaoGia bg)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from BAOGIA", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(bg.MaBaoGia);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaBaoGia"] = bg.MaBaoGia;
                    r["MaNhanVien"] = bg.MaNhanVien;
                    r["NgayLap"] = bg.NgayLap;
                    r["GhiChu"] = bg.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
