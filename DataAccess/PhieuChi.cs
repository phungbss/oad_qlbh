﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa các phương thức truy xuất cơ sở dữ liệu cho PhieuChi
    /// </summary>
    public class PhieuChi : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public PhieuChi()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }
        /// <summary>
        /// Lấy tất cả dữ liệu của bảng PHIEUCHI
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from PHIEUCHI");
        }
        /// <summary>
        /// Nhập dữ liệu vào bảng PHIEUCHI
        /// </summary>
        /// <param name="pc">phiếu thu cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuChi pc)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUCHI", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaPhieu"] = pc.MaPhieu;
                r["NgayLap"] = pc.NgayLap;
                r["MaNhanVien"] = pc.MaNhanVien;
                r["TenNguoiNhan"] = pc.TenNguoiNhan;
                r["SoTien"] = pc.SoTien;
                r["LyDo"] = pc.LyDo;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// xóa phiếu chi từ bảng PHIEUCHI
        /// </summary>
        /// <param name="maPhieu">mã phiếu cần xóa</param>
        /// <returns></returns>
        public bool Xoa(string maPhieu)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUCHI", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maPhieu);

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// sửa phiếu chi
        /// </summary>
        /// <param name="pc">phiếu chi cần sửa</param>
        /// <returns></returns>
        public bool Sua(DataTransfer.PhieuChi pc)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUCHI", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(pc.MaPhieu);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaPhieu"] = pc.MaPhieu;
                    r["NgayLap"] = pc.NgayLap;
                    r["MaNhanVien"] = pc.MaNhanVien;
                    r["TenNguoiNhan"] = pc.TenNguoiNhan;
                    r["SoTien"] = pc.SoTien;
                    r["LyDo"] = pc.LyDo;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
