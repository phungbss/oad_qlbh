﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho SaoLuu
    /// </summary>
    public class SaoLuu : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public SaoLuu()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summany>
        /// Lấy tất cả dữ liệu của bảng SAOLUUPHUCHOI
        /// </summany>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from SAOLUUPHUCHOI");
        }

        /// <summany>
        /// Thêm một bản Sao lưu vào bảng SAOLUUPHUCHOI
        /// </summany>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.SaoLuu sl)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from SAOLUUPHUCHOI", Connection);

                //Tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaSaoLuu"] = sl.MaSaoLuu;
                r["DuongDan"] = sl.DuongDan;
                r["NgayThucHien"] = sl.NgayThucHien;
                r["NoiDung"] = sl.NoiDung;
                dt.Rows.Add(r);

                //Cập nhật vào cơ sở dữ liệu
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Xóa một bản Sao lưu từ bảng SAOLUUPHUCHOI
        /// </summany>
        /// <param name="maSaoLuu">bản Sao lưu cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maSaoLuu)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from SAOLUUPHUCHOI", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maSaoLuu);

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật một bản Sao lưu trong bảng SAOLUUPHUCHOI
        /// </summary>
        /// <param name="sp">bản Sao lưu cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.SaoLuu sl)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from SAOLUUPHUCHOI", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(sl.MaSaoLuu);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaSaoLuu"] = sl.MaSaoLuu;
                    r["DuongDan"] = sl.DuongDan;
                    r["NgayThucHien"] = sl.NgayThucHien;
                    r["NoiDung"] = sl.NoiDung;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Backups_full(string Duongdan, string TenSl)
        {
            try
            {
                string NameDatabases ="[" + Environment.CurrentDirectory + @"\DATABASES\QUANLYBANHANG.MDF" + "]";

                string str_Backup = @"WITH NOFORMAT, INIT,  NAME = N'" + Environment.CurrentDirectory + @"\DATABASES\QUANLYBANHANG.MDF" + "-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";

                string sqlCommand = @"BACKUP DATABASE " + NameDatabases +  "TO DISK = N'" + Duongdan + @"\" + TenSl + ".bak'" + str_Backup;                

                SqlCommand command = new SqlCommand(sqlCommand, Connection);

                OpenConnection();

                command.ExecuteNonQuery();

                CloseConnection();

                return true;
            }
            catch
            {
                CloseConnection();

                return false;
            }
        }
        
        public bool Restore(string Duongdan)
        {
            try
            {
                string NameDatabases = "[" + Environment.CurrentDirectory + @"\DATABASES\QUANLYBANHANG.MDF" + "]";

                string sqlCommand_0 = "USE [master]";

                string sqlCommand_1 = @"RESTORE DATABASE " + NameDatabases + " FROM DISK = N'" + Duongdan + "' WITH  FILE = 1,  NOUNLOAD, REPLACE , STATS = 5";

                SqlCommand command_0 = new SqlCommand(sqlCommand_0, Connection);

                SqlCommand command_1 = new SqlCommand(sqlCommand_1, Connection);

                OpenConnection();

                command_0.ExecuteNonQuery();

                command_1.ExecuteNonQuery();

                CloseConnection();

                return true;
            }
            catch
            {
                CloseConnection();

                return false;
            }
        }   
            
    }
}
