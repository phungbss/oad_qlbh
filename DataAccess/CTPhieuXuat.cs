﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class CTPhieuXuat : DBConnect
    {
        private DataTable dt;
        private DataTable dtSP;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public CTPhieuXuat()
        {
            //nạp bảng lên
            dt = SelectAll();
            dtSP = GetData("select * from SANPHAM");

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0], dt.Columns[1] };
            dtSP.PrimaryKey = new DataColumn[] { dtSP.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu của bảng CTPHIEUXUAT
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from CTPHIEUXUAT");
        }
        /// <summary>
        /// Thêm một chi tiết vào bảng CTPHIEUXUAT
        /// </summary>
        /// <param name="ctpx">Phiếu cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTPhieuXuat ctpx)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTPHIEUXUAT", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaPhieu"] = ctpx.MaPhieu;
                r["MaSanPham"] = ctpx.MaSanPham;
                r["SoLuong"] = ctpx.SoLuong;
                r["GhiChu"] = ctpx.GhiChu;

                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);

                SqlDataAdapter da1 = new SqlDataAdapter("select * from SANPHAM", Connection);

                //tìm dòng
                DataRow r1 = dtSP.Rows.Find(ctpx.MaSanPham);

                //cập nhật số lượng sản phẩm
                if (r1 != null)
                {
                    r1["TonKho"] = Convert.ToInt32(r1["TonKho"]) - ctpx.SoLuong;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm1 = new SqlCommandBuilder(da1);
                da1.Update(dtSP);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Xoá một chi tiết phiếu nhập trong bảng CTPhieuXuat
        /// </summary>
        /// <param name="maPhieu">mã phiếu</param>
        /// <param name="maSanPham">mã sản phẩm cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maPhieu, string maSanPham)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTPHIEUXUAT", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { maPhieu, maSanPham });

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Sua(DataTransfer.CTPhieuXuat ctpx)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTPHIEUXUAT", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { ctpx.MaPhieu, ctpx.MaSanPham });

                //cập nhật dòng
                if (r != null)
                {
                    r["MaHoaDon"] = ctpx.MaPhieu;
                    r["MaSanPham"] = ctpx.MaSanPham;
                    r["SoLuong"] = ctpx.SoLuong;
                    r["GhiChu"] = ctpx.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
