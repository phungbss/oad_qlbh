﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho HoaDonBanHang
    /// </summary>
    public class HoaDonBanHang : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public HoaDonBanHang()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu của bảng HOADONBANHANG
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from HOADONBANHANG");
        }

        /// <summary>
        /// Thêm một hóa đơn vào bảng HOADONBANHANG
        /// </summary>
        /// <param name="hd">Hóa đơn cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.HoaDonBanHang hd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from HOADONBANHANG", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaHoaDon"] = hd.MaHoaDon;
                r["MaKhachHang"] = hd.MaKhachHang;
                r["MaNhanVien"] = hd.MaNhanVien;
                r["NgayLap"] = hd.NgayLap;
                r["TongTien"] = hd.TongTien;
                r["ThanhTien"] = hd.ThanhTien;
                r["DaThu"] = hd.DaThu;
                r["GhiChu"] = hd.GhiChu;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một hóa đơn từ bảng HOADONBANHANG
        /// </summary>
        /// <param name="maHoaDon">Hóa đơn cần xóa</param>
        /// <returns>Thành công hay không?</returns>
        public bool Xoa(string maHoaDon)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from HOADONBANHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maHoaDon);

                //tìm và xóa tất cả khóa ngoại
                DataAccess.CTHoaDonBanHang cthd = new CTHoaDonBanHang();
                DataRow[] cthd_row = cthd.Table.Select("MaHoaDon like '" + maHoaDon + "'");
                foreach (DataRow item in cthd_row)
                {
                    cthd.Xoa(item["MaHoaDon"].ToString(),item["MaSanPham"].ToString());
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật một hóa đơn trong bảng HOADONBANHANG
        /// </summary>
        /// <param name="hd">Hóa đơn cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.HoaDonBanHang hd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from HOADONBANHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(hd.MaHoaDon);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaHoaDon"] = hd.MaHoaDon;
                    r["MaKhachHang"] = hd.MaKhachHang;
                    r["MaNhanVien"] = hd.MaNhanVien;
                    r["NgayLap"] = hd.NgayLap;
                    r["TongTien"] = hd.TongTien;
                    r["ThanhTien"] = hd.ThanhTien;
                    r["DaThu"] = hd.DaThu;
                    r["GhiChu"] = hd.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
