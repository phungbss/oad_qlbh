﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho NguoiDung
    /// </summary>
    public class NguoiDung : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public NguoiDung()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu của bảng NGUOIDUNG
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from NGUOIDUNG");
        }

        /// <summary>
        /// Thêm một người dùng vào bảng NGUOIDUNG
        /// </summary>
        /// <param name="nd">Người dùng cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.NguoiDung nd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from NGUOIDUNG", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaNguoiDung"] = nd.MaNguoiDung;
                r["TenDangNhap"] = nd.TenDangNhap;
                r["MatKhau"] = nd.MatKhau;
                r["HoTen"] = nd.HoTen;
                r["GioiTinh"] = nd.GioiTinh;
                r["MaDacQuyen"] = nd.MaDacQuyen;
                r["NgaySinh"] = nd.NgaySinh;
                r["SoDienThoai"] = nd.SoDienThoai;
                r["Email"] = nd.Email;               
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một người dùng từ bảng NGUOIDUNG
        /// </summary>
        /// <param name="maNguoiDung">Người dùng cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maNguoiDung)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from NGUOIDUNG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maNguoiDung);

                //tìm và xóa tất cả khóa ngoại
                DataAccess.HoaDonBanHang hdbh = new HoaDonBanHang();
                DataRow[] hdbh_row = hdbh.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in hdbh_row)
                {
                    hdbh.Xoa(item["MaHoaDon"].ToString());
                }

                DataAccess.HoaDonDatHang hddh = new HoaDonDatHang();
                DataRow[] hddh_row = hddh.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in hddh_row)
                {
                    hddh.Xoa(item["MaHoaDon"].ToString());
                }

                DataAccess.PhieuNhap pn = new PhieuNhap();
                DataRow[] pn_row = pn.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in pn_row)
                {
                    pn.Xoa(item["MaPhieu"].ToString());
                }

                DataAccess.PhieuXuat px = new PhieuXuat();
                DataRow[] px_row = px.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in px_row)
                {
                    px.Xoa(item["MaPhieu"].ToString());
                }

                DataAccess.PhieuThu pt = new PhieuThu();
                DataRow[] pt_row = pt.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in pt_row)
                {
                    pt.Xoa(item["MaPhieu"].ToString());
                }

                DataAccess.PhieuChi pc = new PhieuChi();
                DataRow[] pc_row = pc.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in pc_row)
                {
                    pc.Xoa(item["MaPhieu"].ToString());
                }

                DataAccess.BaoGia bg = new BaoGia();
                DataRow[] bg_row = bg.Table.Select("MaNhanVien like '" + maNguoiDung + "'");
                foreach (DataRow item in bg_row)
                {
                    bg.Xoa(item["MaBaoGia"].ToString());
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật thông tin một người dùng trong bảng NGUOIDUNG
        /// </summary>
        /// <param name="nd">Người dùng cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.NguoiDung nd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from NGUOIDUNG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(nd.MaNguoiDung);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaNguoiDung"] = nd.MaNguoiDung;
                    r["TenDangNhap"] = nd.TenDangNhap;
                    r["MatKhau"] = nd.MatKhau;
                    r["HoTen"] = nd.HoTen;
                    r["GioiTinh"] = nd.GioiTinh;
                    r["MaDacQuyen"] = nd.MaDacQuyen;
                    r["NgaySinh"] = nd.NgaySinh;
                    r["SoDienThoai"] = nd.SoDienThoai;
                    r["Email"] = nd.Email;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DangNhap(string name, string pass)
        {
            DataRow[] r = dt.Select(string.Format("TenDangNhap = '{0}'", name));
            return r.Length > 0 && r[0]["MatKhau"].ToString() == pass;
        }

    }
}
