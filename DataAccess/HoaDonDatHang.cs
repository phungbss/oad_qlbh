﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho HoaDonDatHang
    /// </summary>
    public class HoaDonDatHang : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public HoaDonDatHang()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summany>
        /// Lấy tất cả dữ liệu của bảng HOADONDATHANG
        /// </summany>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from HOADONDATHANG");
        }

        /// <summany>
        /// Thêm một hóa đơn vào bảng HOADONDATHANG
        /// </summany>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.HoaDonDatHang hd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from HOADONDATHANG", Connection);

                //Tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaHoaDon"] = hd.MaHoaDon;
                r["MaKhachHang"] = hd.MaKhachHang;
                r["MaNhanVien"] = hd.MaNhanVien;
                r["NgayDuKienGiao"] = hd.NgayDuKienGiao;
                r["NgayLap"] = hd.NgayLap;
                r["TongTien"] = hd.TongTien;
                r["ThanhTien"] = hd.ThanhTien;
                r["DaGiao"] = hd.DaGiao;
                r["GhiChu"] = hd.GhiChu;
                dt.Rows.Add(r);

                //Cập nhật vào cơ sở dữ liệu
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Xóa một hóa đơn từ bảng HOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">Hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from HOADONDATHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maHoaDon);

                //Kiểm tra khóa ngoại
                DataAccess.CTHoaDonDatHang Ctdh = new DataAccess.CTHoaDonDatHang();
                DataRow[] ctdh_row = Ctdh.Table.Select("MaHoaDon like '" + maHoaDon + "'");
                foreach (DataRow item in ctdh_row)
                {
                    Ctdh.Xoa(item["MaHoaDon"].ToString(), item["MaSanPham"].ToString());
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật một hóa đơn trong bảng HOADONDATHANG
        /// </summary>
        /// <param name="hd">Hóa đơn cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.HoaDonDatHang hd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from HOADONDATHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(hd.MaHoaDon);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaHoaDon"] = hd.MaHoaDon;
                    r["MaKhachHang"] = hd.MaKhachHang;
                    r["MaNhanVien"] = hd.MaNhanVien;
                    r["NgayDuKienGiao"] = hd.NgayDuKienGiao;
                    r["NgayLap"] = hd.NgayLap;
                    r["TongTien"] = hd.TongTien;
                    r["ThanhTien"] = hd.ThanhTien;
                    r["DaGiao"] = hd.DaGiao;
                    r["GhiChu"] = hd.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
