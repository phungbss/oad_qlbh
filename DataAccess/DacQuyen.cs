﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho DacQuyen
    /// </summary>
    public class DacQuyen : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public DacQuyen()
        {
            //nạp bảng lên
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }

        /// <summary>
        /// Lấy tất cả dữ liệu của bảng DACQUYEN
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from DACQUYEN");
        }

        /// <summary>
        /// Thêm một đặc quyền vào bảng DACQUYEN
        /// </summary>
        /// <param name="dq">Đặc quyền cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.DacQuyen dq)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from DACQUYEN", Connection);

                //tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaDacQuyen"] = dq.MaDacQuyen;
                r["TenDacQuyen"] = dq.TenDacQuyen;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa một đặc quyền từ bảng DACQUYEN
        /// </summary>
        /// <param name="maDacQuyen">Đặc quyền cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maDacQuyen)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from DACQUYEN", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maDacQuyen);

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cập nhật thông tin đặc quyền trong bảng DACQUYEN
        /// </summary>
        /// <param name="dq">Đặc quyền cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.DacQuyen dq)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from DACQUYEN", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(dq.MaDacQuyen);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaDacQuyen"] = dq.MaDacQuyen;
                    r["TenDacQuyen"] = dq.TenDacQuyen;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
