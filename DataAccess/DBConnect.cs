﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Lớp cơ sở chứa chuỗi kết nối và các hàm thao tác cơ bản
    /// </summary>
    public abstract class DBConnect
    {
        protected SqlConnection _connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""|DataDirectory|\Databases\QUANLYBANHANG.mdf"";Integrated Security=True");

        /// <summary>
        /// Chuỗi kết nối cơ sở dữ liệu
        /// </summary>
        public SqlConnection Connection
        {
            get { return _connection; }
        }

        /// <summary>
        /// Mở kết nối cơ sở dữ liệu
        /// </summary>
        protected void OpenConnection()
        {
            Connection.Open();
        }

        /// <summary>
        /// Đóng kết nối cơ sở dữ liệu
        /// </summary>
        protected void CloseConnection()
        {
            Connection.Close();
        }

        /// <summary>
        /// Lấy dữ liệu với câu lệnh select
        /// </summary>
        /// <param name="sqlCommand">Câu lệnh SQL</param>
        /// <returns></returns>
        public DataTable GetData(string sqlCommand)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand, Connection);

            //mở kết nối
            OpenConnection();

            //lấp dữ liệu vào bảng
            adapter.Fill(dt);

            //đóng kết nối
            CloseConnection();
            return dt;
        }

        /// <summary>
        /// Thực thi các câu lệnh SQL khác ngoài lệnh select
        /// </summary>
        /// <param name="sqlCommand">Câu lệnh SQL</param>
        /// <returns>Số dòng bị ảnh hưởng</returns>
        public int ExecuteCommand(string sqlCommand)
        {
            SqlCommand command = new SqlCommand(sqlCommand, Connection);

            OpenConnection();

            int row = command.ExecuteNonQuery();

            CloseConnection();

            return row;
        }

        /// <summary>
        /// Tự động sinh mã 6 chữ số ngẫu nhiên
        /// </summary>
        /// <returns></returns>
        public string AutoGenerateNumber()
        {
            StringBuilder result = new StringBuilder(6);
            Random rand = new Random();

            for (int i = 0; i < result.Capacity; i++)
            {
                result.Append((char)rand.Next('0', '9'));
            }

            return result.ToString();
        }
    }
}
