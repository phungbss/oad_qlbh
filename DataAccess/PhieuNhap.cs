﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccess
{
    /// <summary>
    /// Chứa các phương thức truy xuất cơ sở dữ liệu cho PhieuNhap
    /// </summary>
    public class PhieuNhap : DBConnect
    {
        private DataTable dt;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public PhieuNhap()
        {
            //nạp bảng
            dt = SelectAll();

            //đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0] };
        }
        /// <summary>
        /// Lấy tất cả dữ liệu trong bảng PHIEUNHAP
        /// </summary>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from PHIEUNHAP");
        }
        /// <summary>
        /// Thêm phiếu nhập
        /// </summary>
        /// <param name="pn">Phiếu nhập cần thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.PhieuNhap pn)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUNHAP", Connection);

                //tạo dòng mới và thêm dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaPhieu"] = pn.MaPhieu;
                r["MaNhanVien"] = pn.MaNhanVien;
                r["MaNhaCungCap"] = pn.MaNhaCungCap;
                r["NgayLap"] = pn.NgayLap;
                r["LoaiPhieu"] = pn.LoaiPhieu;
                r["TongTien"] = pn.TongTien;
                r["GhiChu"] = pn.GhiChu;
                dt.Rows.Add(r);

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Xóa một phiếu từ bảng PHIEUNHAP
        /// </summary>
        /// <param name="maPhieu">Phiếu cần xóa</param>
        /// <returns>Thành công hay không?</returns>
        public bool Xoa(string maPhieu)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUNHAP", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(maPhieu);

                //xóa khóa ngoại
                DataAccess.CTPhieuNhap pn = new CTPhieuNhap();

                DataRow[] pn_row = pn.Table.Select("MaPhieu like '" + maPhieu + "'");
                foreach (DataRow item in pn_row)
                {
                    pn.Xoa(item["MaPhieu"].ToString(), item["MaSanPham"].ToString());
                }

                //xóa dòng
                if (r != null)
                    r.Delete();

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Cập nhật một PHIẾU trong bảng PHIEUNHAP
        /// </summary>
        /// <param name="pn">Phiếu cần cập nhật</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.PhieuNhap pn)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from PHIEUNHAP", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(pn.MaPhieu);

                //cập nhật dòng
                if (r != null)
                {
                    r["MaPhieu"] = pn.MaPhieu;
                    r["MaNhanVien"] = pn.MaNhanVien;
                    r["MaNhaCungCap"] = pn.MaNhaCungCap;
                    r["NgayLap"] = pn.NgayLap;
                    r["LoaiPhieu"] = pn.LoaiPhieu;
                    r["TongTien"] = pn.TongTien;
                    r["GhiChu"] = pn.GhiChu;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
