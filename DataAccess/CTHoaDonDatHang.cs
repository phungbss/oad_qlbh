﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Chứa phương thức truy xuất cơ sở dữ liệu cho CTHoaDonDatHang
    /// </summary>
    public class CTHoaDonDatHang : DBConnect
    {
        private DataTable dt;
        private DataTable dtSP;

        /// <summary>
        /// Bảng dữ liệu
        /// </summary>
        public DataTable Table
        {
            get { return dt; }
            set { dt = value; }
        }

        public CTHoaDonDatHang()
        {
            //Nạp bảng lên
            dt = SelectAll();
            dtSP = GetData("select * from SANPHAM");

            //Đặt khóa chính
            dt.PrimaryKey = new DataColumn[] { dt.Columns[0], dt.Columns[1] };
            dtSP.PrimaryKey = new DataColumn[] { dtSP.Columns[0] };
        }

        /// <summany>
        /// Lấy tất cả dữ liệu cho bảng CTHOADONDATHANG
        /// </summany>
        /// <returns></returns>
        public DataTable SelectAll()
        {
            return GetData("select * from CTHOADONDATHANG");
        }

        /// <summany>
        /// Thêm một Chi tiết hóa đơn vào bảng CTHOADONDATHANG
        /// </summany>
        /// <param name="Cthd">CT hóa đơn cần phải thêm</param>
        /// <returns>Thành công hay không</returns>
        public bool Them(DataTransfer.CTHoaDonDatHang Cthd, bool themSanPham = false)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTHOADONDATHANG", Connection);

                //Tạo dòng mới và chèn dữ liệu vào
                DataRow r = dt.NewRow();
                r["MaHoaDon"] = Cthd.MaHoaDon;
                r["MaSanPham"] = Cthd.MaSanPham;
                r["SoLuong"] = Cthd.SoLuong;
                dt.Rows.Add(r);

                //Cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);

                if (themSanPham == true)
                {
                    SqlDataAdapter da1 = new SqlDataAdapter("select * from SANPHAM", Connection);

                    //tìm dòng
                    DataRow r1 = dtSP.Rows.Find(Cthd.MaSanPham);

                    //cập nhật số lượng sản phẩm
                    if (r1 != null)
                    {
                        r1["TonKho"] = Convert.ToInt32(r1["TonKho"]) - Cthd.SoLuong;
                    }

                    //cập nhật vào CSDL
                    SqlCommandBuilder cm1 = new SqlCommandBuilder(da1);
                    da1.Update(dtSP);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Xóa một Chi tiết hóa đơn trong bảng CTHOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">CT Hóa đơn cần xóa</param>
        /// <returns>Thành công hay không</returns>
        public bool Xoa(string maHoaDon, string maSanPham)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTHOADONDATHANG", Connection);

                //Tìm dòng
                DataRow r = dt.Rows.Find(new object[] { maHoaDon, maSanPham });

                //Xóa dòng
                if (r != null)
                    r.Delete();

                //Cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summany>
        /// Sửa một Chi tiết hóa đơn trong bảng CTHOADONDATHANG
        /// </summany>
        /// <param name="maHoaDon">CT Hóa đơn cần sửa</param>
        /// <returns>Thành công hay không</returns>
        public bool Sua(DataTransfer.CTHoaDonDatHang Cthd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select * from CTHOADONDATHANG", Connection);

                //tìm dòng
                DataRow r = dt.Rows.Find(new object[] { Cthd.MaHoaDon, Cthd.MaSanPham });

                //cập nhật dòng
                if (r != null)
                {
                    r["MaHoaDon"] = Cthd.MaHoaDon;
                    r["MaSanPham"] = Cthd.MaSanPham;
                    r["SoLuong"] = Cthd.SoLuong;
                }

                //cập nhật vào CSDL
                SqlCommandBuilder cm = new SqlCommandBuilder(da);
                da.Update(dt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
